{% extends "test.cu" %}

{%- macro subterm_ctr(term, id, parent) %}
	{%- if term.is_term %}unsigned int idx{{id}} = start_index++;
    unsigned int parent{{id}} = {%-if parent %} idx{{parent}} {%else%} t.index {%endif%};
	{%- for a in term.args %}
	{{subterm_ctr(a, id ~ '_' ~ loop.index0, id)}}
	{%- endfor %}
    Term t{{id}} = Term({{term.hs.name}}, idx{{id}},{% for i in range(maxarity) %}{%- if loop.index0 < term.args|length %} t{{id ~ '_' ~ loop.index0}}.index{%- else %} 0{%-endif%}, {%- endfor %} parent{{id}});
	term[idx{{id}}] = t{{id}};
    h_refs[idx{{id}}]++;
        {%- if rewritable_terms[term.hs] %}
    h_nf[idx{{id}}] = false;
    h_frontier[idx{{id}}] = true;
        {%- elif term.args %}
    h_nf[idx{{id}}] = {%- for a in term.args %} h_nf[idx{{id ~ '_' ~ loop.index0}}] {{ " && " if not loop.last }}{%- endfor %};
    h_frontier[idx{{id}}] = !h_nf[idx{{id}}];
        {%- else %}
	h_nf[idx{{id}}] = true;
    h_frontier[idx{{id}}] = false;
    h_frontier[parent{{id}}] = true;
	    {%- endif %}

	{%- endif %}
{%- endmacro %}

{%- macro print_term(term, input) %}
    {%- for a in term.args %}
	{{subterm_ctr(a, loop.index0, "")}}
	{%- endfor %}

	t.head_symbol = {{term.hs.name}};
    h_refs[t.index] = 1;

    {%- for i in range(maxarity) %}
    {%- if i >= term.args|length %}
    t.arg{{i}} = 0;
	{%- else %}
    t.arg{{i}} = t{{i}}.index;
	{%- endif %}
    {%- endfor %}

    {%- if term.args %}
	h_nf[t.index] = false;
    {%- else %}
    h_nf[t.index] = true;
	{%-endif%}
	h_frontier[t.index] = true;
{%- endmacro %}

{%- block input %}
unsigned int Expand_Input(Term &t, unsigned int start_index, size_t num_terms) {
  //gpuErrchk(cudaMemPrefetchAsync(term, num_terms*term_size, cudaCpuDeviceId, stream3));
  //gpuErrchk(cudaMemsetAsync(frontier, false, num_terms * frontier_size, stream1));
  //gpuErrchk(cudaMemsetAsync(nf, false, num_terms * nf_size, stream2));
  bool *h_frontier;
  gpuErrchk(cudaMallocHost((void**)&h_frontier, num_terms * frontier_size));
  bool *h_nf;
  gpuErrchk(cudaMallocHost((void**)&h_nf, num_terms * nf_size));
  h_nf[0] = true;
  int *h_refs;
  gpuErrchk(cudaMallocHost((void**)&h_refs, num_terms * ref_size));
  memset(h_refs, 0, num_terms);
  
  {{print_term(input_term)}}

  //  gpuErrchk(cudaMemPrefetchAsync((void *)term, num_terms*term_size, device, stream4));
  gpuErrchk(cudaMemcpyAsync((void*)frontier, h_frontier, start_index*frontier_size, cudaMemcpyHostToDevice, stream1));
  gpuErrchk(cudaMemcpyAsync((void*)nf, h_nf, start_index*nf_size, cudaMemcpyHostToDevice, stream2));
  gpuErrchk(cudaMemcpyAsync((void*)refs, h_refs, start_index*ref_size, cudaMemcpyHostToDevice, stream3));

  // gpuErrchk(cudaStreamSynchronize(stream1));
  // gpuErrchk(cudaStreamSynchronize(stream2));
  // gpuErrchk(cudaStreamSynchronize(stream3));

  // gpuErrchk(cudaFreeHost(h_frontier));
  // gpuErrchk(cudaFreeHost(h_nf));
  // gpuErrchk(cudaFreeHost(h_refs));
  
  return start_index;
}
{%- endblock %}
