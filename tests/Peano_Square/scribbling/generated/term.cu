#include "term.cuh"
#include <cooperative_groups.h>
#include <stdio.h>

__device__ int atomicAggInc(unsigned int *ctr) {
  using namespace cooperative_groups;
  auto g = coalesced_threads();
  int warp_res;
  if(g.thread_rank() == 0)
    warp_res = atomicAdd(ctr, g.size());
  return g.shfl(warp_res, 0) + g.thread_rank();
}

__inline__ void __device__ Rewrite_Mult(Term &t, const unsigned int &free_index, unsigned int *next_free, bool *nf, bool *frontier, int *refs) {
  if(is_Succ(term[t.arg0])) {
    
    auto ref0 = atomicSub(&refs[t.arg0], 1);
	atomicAdd(&refs[t.arg1], 1);
    unsigned int acc0 = t.arg1;
	unsigned int idx1 = free_index;
    unsigned int parent1 = t.index ;
	unsigned int acc1_0 = term[t.arg0].arg0;
	if(ref0 > 1) {
	  atomicAdd(&refs[acc1_0], 1);
	}
	unsigned int acc1_1 = t.arg1;
    Term t1 = Term(Mult, idx1, acc1_0, acc1_1, parent1);
	term[idx1] = t1;
    refs[idx1]++;
    nf[idx1] = false;
    frontier[idx1] = true;
    printf("1Term[%u] is now Mult, arg0:=%u, arg1:=%u,  parent:=%u\n",
			   idx1, t1.arg0, t1.arg1,  t1.parent);
	t.head_symbol = Plus;
    t.arg0 = acc0;
    t.arg1 = idx1;
    nf[t.index] = false;
    frontier[t.index] = true;
    printf("\tTerm[%u] is now Plus, arg0:=%u, arg1:=%u,  parent:=%u\n",
			 t.index, t.arg0, t.arg1,  t.parent);
  } else if(is_Zero(term[t.arg0])) {
    
    atomicSub(&refs[t.arg0], 1);
	atomicSub(&refs[t.arg1], 1);
    
	t.head_symbol = Zero;
    t.arg0 = 0;
    t.arg1 = 0;
	nf[t.index] = true;
	frontier[t.parent] = true;
    frontier[t.index] = false;
    printf("\tTerm[%u] is now Zero, arg0:=%u, arg1:=%u,  parent:=%u\n",
			 t.index, t.arg0, t.arg1,  t.parent);
  } else {
  	
    printf("Could not rewrite term %u with type Mult any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
	frontier[t.index] = false;
	frontier[t.parent] = true;
  } 
}

__inline__ void __device__ Rewrite_Plus(Term &t, const unsigned int &free_index, unsigned int *next_free, bool *nf, bool *frontier, int *refs) {
  if(is_Succ(term[t.arg0])) {
    
    auto ref0 = atomicSub(&refs[t.arg0], 1);
	
    
	unsigned int idx0 = free_index;
    unsigned int parent0 = t.index ;
	unsigned int acc0_0 = term[t.arg0].arg0;
	if(ref0 > 1) {
	  atomicAdd(&refs[acc0_0], 1);
	}
	unsigned int acc0_1 = t.arg1;
    Term t0 = Term(Plus, idx0, acc0_0, acc0_1, parent0);
	term[idx0] = t0;
    refs[idx0]++;
    nf[idx0] = false;
    frontier[idx0] = true;
    printf("0Term[%u] is now Plus, arg0:=%u, arg1:=%u,  parent:=%u\n",
			   idx0, t0.arg0, t0.arg1,  t0.parent);
	t.head_symbol = Succ;
    t.arg0 = idx0;
    t.arg1 = 0;
    nf[t.index] = false;
    frontier[t.index] = true;
    printf("\tTerm[%u] is now Succ, arg0:=%u, arg1:=%u,  parent:=%u\n",
			 t.index, t.arg0, t.arg1,  t.parent);
  } else if(is_Zero(term[t.arg0])) {
    
    atomicSub(&refs[t.arg0], 1);
	
    
  Term &acc = term[t.arg1];
  t.head_symbol = acc.head_symbol;
  
  t.arg0 = acc.arg0;
  atomicSub(&refs[t.arg1], 1);
  t.arg1 = acc.arg1;
  nf[t.index] = true;
  frontier[t.index] = false;
  frontier[t.parent] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Plus any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
	frontier[t.index] = false;
	frontier[t.parent] = true;
  } 
}

__inline__ void __device__ Rewrite_Square(Term &t, const unsigned int &free_index, unsigned int *next_free, bool *nf, bool *frontier, int *refs) {
  
    
	atomicAdd(&refs[t.arg0], 1);
    unsigned int acc0 = t.arg0;unsigned int acc1 = t.arg0;
	t.head_symbol = Mult;
    t.arg0 = acc0;
    t.arg1 = acc1;
    nf[t.index] = false;
    frontier[t.index] = true;
    printf("\tTerm[%u] is now Mult, arg0:=%u, arg1:=%u,  parent:=%u\n",
			 t.index, t.arg0, t.arg1,  t.parent);
}



__global__ void frontier2wl(unsigned int *nelig, unsigned int n_terms, bool *frontier, unsigned int *eligible_terms) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= n_terms)
	return;
  if(frontier[id])
	eligible_terms[atomicAggInc(nelig)] = id;
}

__global__ void countrefs(unsigned int n_terms, int *refs) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= n_terms)
	return;
  Term &my_term = term[id];
  refs[my_term.arg0] = 1;
  refs[my_term.arg1] = 1;
}

__global__ void refs2freepos(unsigned int *nfree, unsigned int n_terms, int *refs, unsigned int *free_indices) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= n_terms || id < 2)
	return;
  if(refs[id] < 1) {
  	free_indices[atomicAggInc(nfree)] = id;
    refs[id] = 0;
  }
}

__global__ void pad_freeindices(const unsigned int offset, const unsigned int required, unsigned int *next_free, unsigned int *free_indices) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= required)
	return;
  free_indices[id + offset] = atomicAggInc(next_free);
}

__global__ void Rewrite(bool *frontier, bool *nf, unsigned int *eligible_terms, unsigned int *free_indices, bool *done, unsigned int wl_size, unsigned int *next_free, int *refs) {
    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id >= wl_size)
	  return;
	unsigned int my_term_idx = eligible_terms[id];

    if(!nf[my_term_idx]) {
        Term &my_term = term[my_term_idx];
		auto n = *next_free;
		printf("Term[%u] with HS %s being rewritten\n\t Assigned ID: %u\n Nextfree: %u\n", my_term_idx, HS2String(my_term.head_symbol), free_indices[id], n);
        switch(my_term.head_symbol) {
		case Mult:
		Rewrite_Mult(my_term, free_indices[id], next_free, nf, frontier, refs);
		  *done = false;
		break;
		case Plus:
		Rewrite_Plus(my_term, free_indices[id], next_free, nf, frontier, refs);
		  *done = false;
		break;
		case Square:
		Rewrite_Square(my_term, free_indices[id], next_free, nf, frontier, refs);
		  *done = false;
		break;
		default:
		   nf[my_term_idx] = true;
		   frontier[my_term_idx] = false;
		   frontier[my_term.parent] = true;
		   *done = false;
		  break;
        }
    }
    else frontier[my_term_idx] = false;
}