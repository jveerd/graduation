def pp_formals(formals):
    names = map(lambda f : f.name, formals)
    return ','.join(names)

def pp_ctr(ctr):
    if(len(ctr.formals)):
        print('\t' + ctr.name + '(' + pp_formals(ctr.formals) + ')')
    else:
        print('\t' + ctr.name)

def pp_sort(sort):
    print('sort ' + sort.name)
    print('\tconstructors: ')
    for ctr in sort.constructors:
        pp_ctr(ctr)


def pp_var(var):
    print('\t' + var.name + ' : ' + var.type.name)


def pp_term(term):
    if(term.var): return term.var.name
    else:
        if(len(term.args)):
            return term.hs.name + '(' + ','.join(map(pp_term, term.args)) + ')'
        else:
            return term.hs.name
   
def pp_eqn(eqn):
    print('\t' + pp_term(eqn.lhs) + ' = ' + pp_term(eqn.rhs))

def str_eqn(eqn):
    return '\t' + pp_term(eqn.lhs) + ' = ' + pp_term(eqn.rhs)

def pretty_print(ast):
    print('sorts:')
    for sort in ast.sortDefs:
        pp_sort(sort)
    
    print('variables:')
    for var in ast.varDefs:
        pp_var(var)

    print('eqns:')
    for eqn in ast.eqnDefs:
        pp_eqn(eqn)
