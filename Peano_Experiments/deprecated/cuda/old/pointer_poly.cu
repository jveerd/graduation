#include "pointer_poly.cuh"

__global__ void g_Rewrite(Peano_Term **in) {
    //printf("Thread %d, working on pointer %p\n", threadIdx.x, in[threadIdx.x]);
    Peano_Term* temp = in[threadIdx.x]->Rewrite();
    //printf("Thread %d, rewrite result: %p\n", threadIdx.x, temp);
    in[threadIdx.x] = temp;
}

__device__ Peano_Term* Peano_Zero::Rewrite() {
    return this;
}

__device__ Peano_Term* Peano_Succ::Rewrite() {
    if(this->n->is_normalform()) {
        printf("SUCC: Successor of normal form, making myself normal form\n");
        this->make_normal();
    }
    else {
        printf("SUCC: Successor of non-normal form, rewriting n\n");
        this->n = this->n->Rewrite();
        //g_Rewrite<<1,1>>(&(this->n), 1);
    }
    return this;
}

__device__ Peano_Term* Peano_Add::Rewrite() {
    if(!this->lhs->is_normalform() && !this->rhs->is_normalform()) {
        printf("ADD: neither lhs or rhs are normal form, applying parallel rewrite\n");
        Peano_Term **children = (Peano_Term **) malloc(2*sizeof(Peano_Term *));
        children[0] = this->lhs;
        children[1] = this->rhs;
        g_Rewrite<<<1,2>>>(children);
        cudaDeviceSynchronize();
        this->lhs = children[0];
        this->rhs = children[1];
        return this;
    } else if(!this->lhs->is_normalform()) { //leftmost innermost
        printf("ADD: Addition of non-normal lhs, rewriting lhs\n");
        this->lhs = this->lhs->Rewrite();
        return this;
    } else if (!this->rhs->is_normalform()) {
        printf("ADD: Addition of non-normal rhs, rewriting rhs\n");
        this->rhs = this->rhs->Rewrite();
        return this;
    } else { //both lhs and rhs are in normal form, apply addition rewrite rules
        if(this->lhs->is_zero()) {
            printf("ADD: lhs is 0, returning rhs\n");
            return this->rhs;
        } else { //a->lhs is a successor
            printf("ADD: lhs is a successor, returning S(n + rhs)\n");
            Peano_Succ *s = reinterpret_cast<Peano_Succ*>(this->lhs);
            this->lhs = s->n;
            return new Peano_Succ(this);
        }
    }
}

__device__ Peano_Term* Peano_Mult::Rewrite() {
    if(!this->lhs->is_normalform() && !this->rhs->is_normalform()) {
        printf("MULT: neither lhs or rhs are normal form, applying parallel rewrite\n");
        Peano_Term **children = (Peano_Term **) malloc(2*sizeof(Peano_Term *));
        children[0] = this->lhs;
        children[1] = this->rhs;
        g_Rewrite<<<1,2>>>(children);
        cudaDeviceSynchronize();
        this->lhs = children[0];
        this->rhs = children[1];
        return this;
    } else if(!this->lhs->is_normalform()) { //leftmost innermost
        printf("MULT: Multiplication of non-normal lhs, rewriting lhs\n");
        this->lhs = this->lhs->Rewrite();
        return this;
    } else if (!this->rhs->is_normalform()) {
        printf("MULT: Multiplication of non-normal rhs, rewriting rhs\n");
        this->rhs = this->rhs->Rewrite();
        return this;
    } else { //both lhs and rhs are in normal form, apply multiplication rewrite rules
        if(this->lhs->is_zero()) {
            printf("MULT: Multiplication with 0, returning 0\n");
            return this->lhs;
        } else {
            Peano_Succ *s = reinterpret_cast<Peano_Succ*>(this->lhs);
            printf("MULT: lhs is a successor, returning (n * rhs) + rhs\n");
            this->lhs = s->n;
            return new Peano_Add(this, this->rhs); //Careful? we now have two pointers to rhs in our tree. What happens during rewriting?
        }
    }
}