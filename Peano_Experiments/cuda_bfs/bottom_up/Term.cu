#include "Term.cuh"

__global__ void rewrite_Root() {
    term[root_idx].Rewrite();
}

__global__ void p_Rewrite(unsigned int *in) {
    term[in[threadIdx.x]].Rewrite();
}

__global__ void Rewrite(unsigned int offset) {
    unsigned int my_id = offset + blockIdx.x * blockDim.x + threadIdx.x;
    term[my_id].Rewrite();
}

void __device__ Rewrite_Succ(Term& succ) {
    if(!in_normalform[succ.lhs])
        term[succ.lhs].Rewrite();

    in_normalform[succ.t] = true;
}

void __device__ Rewrite_Add(Term &add) {
    if(!in_normalform[add.lhs]) { //leftmost innermost
        //debug("ADD: Addition of non-normal lhs, rewriting lhs");
        term[add.lhs].Rewrite();
    }
    if(!in_normalform[add.rhs]) {
        //debug("ADD: Addition of non-normal rhs, rewriting rhs");
        term[add.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply addition rewrite rules
    if(term[add.lhs].type == Zero) {
        //debug("ADD: lhs is 0, result := rhs");
        in_normalform[add.t] = true;
        add.type = term[add.rhs].type;
        add.lhs = term[add.rhs].lhs;
        add.rhs = term[add.rhs].rhs;
        term[add.t] = add;
    } else { //term[add.lhs].type is Succ
        //debug("ADD: rewriting S(n) + rhs to S(n + rhs)");

        Term &new_succ = add;
        Term &new_add = term[add.lhs];

        new_succ.type = Succ;
        new_succ.lhs = new_add.t;

        new_add.type = Add;
        new_add.rhs = new_succ.rhs;

        new_add.Rewrite();

        in_normalform[new_add.t] = true;
        in_normalform[new_succ.t] = true;
        term[new_add.t] = new_add;
    }
}

void __device__ Rewrite_Mult(Term &mult) {
    if(!in_normalform[mult.lhs]) { //leftmost innermost
        term[mult.lhs].Rewrite();
    }
    if(!in_normalform[mult.rhs]) {
        term[mult.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply multiplication rewrite rules
    if(term[mult.lhs].type == Zero) {
        term[mult.t].type = Zero;
        in_normalform[mult.t] = true;
    } else { //term[mult.lhs].type is Succ
        Term &new_add = mult;
        Term &new_mult = term[mult.lhs];

        new_add.type = Add;
        new_add.lhs = new_mult.t;

        new_mult.type = Mult;
        new_mult.rhs = new_add.rhs;

        new_mult.Rewrite();
        new_add.Rewrite();
    }
}

void __device__ Term::Rewrite() {
    switch(this->type) {
        case Zero:
            break;
        case Succ:
            Rewrite_Succ(*this);
            break;
        case Add:
            Rewrite_Add(*this);
            break;
        case Mult:
            Rewrite_Mult(*this);
            break;
    }
}