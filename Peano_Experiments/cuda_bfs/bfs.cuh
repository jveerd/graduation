#include "term.cuh"

__global__ void bfs_kernel(Term *term, bool *frontier, bool *nf, bool *done);