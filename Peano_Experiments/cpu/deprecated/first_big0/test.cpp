#include <iostream>
#include <chrono>
#include "Peano_Term.hpp"

#define DEPTH 3

unsigned int next_free;

int make_big0(int depth) {
    auto mem_size = (1<<(depth+1)) * sizeof(Peano_Term) + (1<<(depth+1)) * sizeof(bool);
//    std::cout << "Memory usage for " << ((1<<(depth+1)) - 1) << " terms: " << mem_size << " bytes." << std::endl;

    term = new Peano_Term[1<<(depth+1)];
    in_normalform = new bool[1<<(depth+1)];

    term[0] = Peano_Term(Zero, 0, 0, 0);

    for(unsigned int i = 1; i <= (1<<depth); i++) {
        term[i] = Peano_Term(Add, i, 0, 0);
    }

    unsigned int start_index = (1<<depth);

    for(int d = depth-1; d >= 0; d--) {
//        std::cout << "Populating level " << d << " of term.\n";

        unsigned int end_index = start_index + (1<<d);
        unsigned int prev_lvl = start_index - (1<<(d+1));
        for(unsigned int i = start_index + 1; i <= end_index; i++) {
            unsigned int j = i - start_index;
            term[i] = Peano_Term(Add, i, prev_lvl + j*2-1, prev_lvl + j*2);
        }
//        std::cout << "term[" << end_index <<"] written: " /*<< term[end_index]*/ << std::endl;
        start_index = end_index;
    }

    next_free = start_index + 1;
    return start_index;
}

int make_2x2x2x2() {
    term = new Peano_Term[250];
    in_normalform = new bool[250];

    term[0] = Peano_Term(Zero, 0, 0, 0); //zero

    term[1] = Peano_Term(Succ, 1, 0, 0); //lone1
    term[2] = Peano_Term(Succ, 2, 0, 0); //lone2
    term[3] = Peano_Term(Succ, 3, 0, 0); //rone1
    term[4] = Peano_Term(Succ, 4, 0, 0); //rone2

    term[5] = Peano_Term(Succ, 5, 1, 0); //ltwo1
    term[6] = Peano_Term(Succ, 6, 2, 0); //ltwo2
    term[7] = Peano_Term(Succ, 7, 3, 0); //rtwo1
    term[8] = Peano_Term(Succ, 8, 4, 0); //rtwo2

    term[9] = Peano_Term(Mult, 9, 5, 6); //mul1 = ltwo1 * ltwo2
    term[10] = Peano_Term(Mult, 10, 7, 8); //mul2 = rtwo1 * rtwo2

    term[11] = Peano_Term(Mult, 11, 9, 10); //mul = mul1 * mul2

    next_free = 12;
//    std::cout << "Initial term: " << term[11].toString() << std::endl;

    return 11;
}

std::ostream& operator<<(std::ostream& os, const Peano_Term& pt) {
    os << pt.toString();
    return os;
}

int main(int argc, const char* argv[])
{
    int depth;
    if(argc > 1) {
        depth = atoi(argv[1]);
    } else {
        std::cout << "No depth provided, continuing with DEPTH=" << DEPTH << std::endl;
        depth = DEPTH;
    }

    int root = make_big0(depth);
    using milli = std::chrono::milliseconds;
    auto start = std::chrono::high_resolution_clock::now();
    term[root].Rewrite();
    auto finish = std::chrono::high_resolution_clock::now();

    std::cout << "Done rewriting after "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

    std::cout << "Rewritten to: " << term[root].toString() << std::endl;
    return 0;
}