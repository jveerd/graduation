#include "Term.hpp"
#include <iostream>

bool verbose = false;
Term *term;
bool *in_normalform;

void debug(std::string msg) {
    if(verbose) std::cerr << msg << std::endl;
}

std::string Term::toString() const {
    switch(this->type) {
        case Zero:
            return "0";
            break;
        case Succ:
            return "S(" + term[this->lhs].toString() + ")";
            break;
        case Add:
            return "(" + term[this->lhs].toString() + " + " + term[this->rhs].toString() + ")";
            break;
        case Mult:
            return "(" + term[this->lhs].toString() + " x " + term[this->rhs].toString() + ")";
            break;
    }
}

void Rewrite_Succ(Term& succ) {
    if(!in_normalform[succ.lhs])
        term[succ.lhs].Rewrite();

    in_normalform[succ.t] = true;
}

void Rewrite_Add(Term &add) {
    if(!in_normalform[add.lhs]) { //leftmost innermost
        debug("ADD: Addition of non-normal lhs, rewriting lhs");
        term[add.lhs].Rewrite();
    }
    if(!in_normalform[add.rhs]) {
        debug("ADD: Addition of non-normal rhs, rewriting rhs " + term[add.rhs].toString());
        term[add.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply addition rewrite rules
    if(term[add.lhs].type == Zero) {
        debug("ADD: lhs is 0, result := rhs");
        in_normalform[add.t] = true;
        add.type = term[add.rhs].type;
        add.lhs = term[add.rhs].lhs;
        add.rhs = term[add.rhs].rhs;
        term[add.t] = add;
    } else { //term[add.lhs].type is Succ
        debug("ADD: rewriting S(n) + rhs to S(n + rhs)");

        Term &new_succ = add;
        Term new_add = Term(Add, next_free, term[add.lhs].lhs, add.rhs);
        term[next_free++] = new_add;

        new_succ.type = Succ;
        new_succ.lhs = new_add.t;

        new_add.Rewrite();

        in_normalform[new_add.t] = true;
        in_normalform[new_succ.t] = true;
        term[new_add.t] = new_add;
    }
}

void Rewrite_Mult(Term &mult) {
    if(!in_normalform[mult.lhs]) { //leftmost innermost
        debug("MULT: Multiplication of non-normal lhs, rewriting lhs " + term[mult.lhs].toString());
        term[mult.lhs].Rewrite();
        debug("\tlhs_MULT: rewritten to " + term[mult.lhs].toString());
    }
    if(!in_normalform[mult.rhs]) {
        debug("MULT: Multiplication of non-normal rhs, rewriting rhs " + term[mult.rhs].toString());
        term[mult.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply multiplication rewrite rules
    if(term[mult.lhs].type == Zero) {
        debug("MULT: Multiplication with 0, result := 0");
        term[mult.t].type = Zero;
        in_normalform[mult.t] = true;
    } else { //term[mult.lhs].type is Succ
        debug("MULT: rewriting S(n) * rhs to (n * rhs) + rhs");
        Term &new_add = mult;
        Term &new_mult = term[mult.lhs];

        new_add.type = Add;
        new_add.lhs = new_mult.t;

        new_mult.type = Mult;
        new_mult.rhs = new_add.rhs;

        new_mult.Rewrite();
        term[new_mult.t] = new_mult;

        new_add.Rewrite();
        term[new_add.t] = new_add;
    }
}

void Term::Rewrite() {
    switch(this->type) {
        case Zero:
            break;
        case Succ:
            Rewrite_Succ(*this);
            break;
        case Add:
            Rewrite_Add(*this);
            break;
        case Mult:
            Rewrite_Mult(*this);
            break;
    }
}

//Constructor requires that terms are built from the leaves in order to deduce whether they should be normal form
Term::Term(Type type, unsigned int t, unsigned int lhs, unsigned int rhs)
    : type(type), t(t), lhs(lhs), rhs(rhs) {
    bool nf = (type == Zero) || ((type == Succ) && in_normalform[lhs]);
    in_normalform[t] = nf;
}