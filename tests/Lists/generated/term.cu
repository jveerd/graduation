#include "term.cuh"



inline void __device__ Rewrite_Append(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Append(t) && is_Cons(term[t.arg0])) {
    
	Term &acc0 = term[term[t.arg0].arg0];
	
	Term &acc1_0 = term[term[t.arg0].arg1];
	Term &acc1_1 = term[t.arg1];
	unsigned int idx1 = atomicAdd(&next_free, 1);
    Term t1 = Term(Append, idx1, acc1_0.t, acc1_1.t, t.t);
	term[idx1] = t1;
	nf[idx1] = false;
	frontier[idx1] = true;
	t.type = Cons;
    t.arg0 = acc0.t;
    t.arg1 = t1.t;
    frontier[t.t] = true;
  } else if(is_Append(t) && is_Nil(term[t.arg1])) {
    
  Term &acc = term[t.arg0];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else if(is_Append(t) && is_Nil(term[t.arg0])) {
    
  Term &acc = term[t.arg1];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Append any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Head(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Head(t) && is_Cons(term[t.arg0])) {
    
  Term &acc = term[term[t.arg0].arg0];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else if(is_Head(t) && is_Nil(term[t.arg0])) {
    
	t.type = NaN;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Head any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}




__global__ void Rewrite(Term *term, bool *done, bool *frontier, bool *nf) {
    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    bool do_work = frontier[id] && !nf[id];
    __syncthreads();
    if(do_work) {
        Term &my_term = term[id];

        switch(my_term.type) {
		case Append:
		  Rewrite_Append(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Head:
		  Rewrite_Head(my_term, term, frontier, nf);
		  *done = false;
		break;
		default:
		   nf[id] = true;
		   frontier[id] = false;
		   frontier[my_term.parent] = true;
		   *done = false;
		  break;
        }
    }
    else if(nf[id]) frontier[id] = false;
}