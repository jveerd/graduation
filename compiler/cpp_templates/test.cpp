#include <iostream>
#include "term.h"
#include <chrono>

Term *term;
Indices *indices;
bool *nf;

{%- block input %} {%- endblock %}

int main(int argc, const char* argv[]) {
  term = new Term[1<<23];
  int refs[1<<23] = {0};
  nf = new bool[1<<23]{false};
  std::stack<unsigned int> free_indices = std::stack<unsigned int>();
  indices = new Indices(term, refs, 2, free_indices);
  
  Term zero(NOT_INITIALIZED, 0, {%- for i in range(maxarity) %} 0{{", " if not loop.last}}{%- endfor %});
  term[0] = zero;
  nf[0] = true;
  Term root(Input, 1, {%- for i in range(maxarity) %} 0{{", " if not loop.last}} {%- endfor %});
  term[1] = root;
  refs[1] = 1;
  Expand_Input(root);

  //  std::cout << "Input: " + root.toString(term) << std::endl;
  
  auto beginTime = std::chrono::high_resolution_clock::now();

  auto handled = Rewrite(root, term, nf, *indices);

  //std::cout << "Output: " + root.toString(term) << std::endl;
  auto endTime = std::chrono::high_resolution_clock::now();

  auto ms = std::chrono::duration<double, std::milli>(endTime - beginTime);
  double seconds = ms.count() / (double)1000;
  double tps = (double)handled / seconds;

  std::cout.precision(0);
  std::cout << std::fixed /*<< "Rewriting took "*/ << ms.count() << " ";//ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific /*<< handled << "\n R/S: "*/ << tps << " ";//std::endl;


  std::cout << /*"Highest pointer to occupied memory: " <<*/ indices->next_free << std::endl;
    
  return 0;
}
