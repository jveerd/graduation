{% extends "term.cpp" %}

{%- macro arg_kind(arg) %}
	{%- if arg.is_acc %}acc
	{%- elif arg.is_term %}idx
	{%- else %} *****ARG_KIND: NOT AN ACCESSOR OR TERM: {{arg.toString()}}***** {%-endif%}
{%- endmacro %}

{%- macro unfold_acc_(acc) %}
	{%- if acc.term.is_acc %}term[{{unfold_acc_(acc.term)}}.arg{{acc.i}}]
	{%- else %}term[t.arg{{acc.i}}] {%- endif %}
{%- endmacro%}

{%- macro unfold_acc(acc) %}
	{%- if acc.term.is_acc %}{{unfold_acc_(acc.term)}}.arg{{acc.i}}
	{%- else %}t.arg{{acc.i}}{%- endif %}
{%- endmacro %}

{%- macro collect(accs) %}
    {%- for acc in accs %}
    indices.decRef({{unfold_acc(acc)}});{%- endfor %}
{%- endmacro %}

{%- macro addrefs(varcounts) %}
    {%- for acc, amt in varcounts.items() %}
    {%- if amt > 0 %}indices.incRef({{unfold_acc(acc)}}, {{amt}});{%-endif%}
	{%- endfor %}
{%- endmacro %}

{%- macro make_ctr(term, id) %}
	{%- if term.is_acc %}unsigned int acc{{id}} = {{unfold_acc(term)}};
	{%- elif term.is_term %}unsigned int idx{{id}} = indices.getRef();
	{%- for a in term.args %}
	{{make_ctr(a, id ~ '_' ~ loop.index0)}}
	{%- endfor %}
    Term t{{id}} = Term({{term.hs.name}}, idx{{id}},{% for i in range(maxarity) %}{%- if loop.index0 < term.args|length %} {{arg_kind(term.args[loop.index0])}}{{id ~ '_' ~ loop.index0}}{%- else %} 0{%-endif%}{{", " if not loop.last}}{%- endfor %});
	term[idx{{id}}] = t{{id}};
    {#printf("{{id}}Term[%u] is now {{term.hs.name}}, {%for i in range(maxarity)%}arg{{i}}:=%u, {%endfor%}\n",
			 idx{{id}}, {%for i in range(maxarity)%}t{{id}}.arg{{i}}{{", " if not loop.last}} {%endfor%});#}

    {%- if rewritable_terms[term.hs] %}
    nf[idx{{id}}] = false;
    {%- elif not term.args %}
	nf[idx{{id}}] = true;
    {%- else %}
	nf[idx{{id}}] = false;
    {%-endif%}
    {%- endif %}
{%- endmacro %}

{%- macro print_term(term) %}
  {%- if term.is_toplevel %}
    {%- for a in term.args %}
    {{make_ctr(a, loop.index0)}}
	{%- endfor %}
	t.head_symbol = {{term.hs.name}};
    {%- for a in term.args %}
    indices.decRef(t.arg{{loop.index0}});
    t.arg{{loop.index0}} = {{arg_kind(a)}}{{loop.index0}};
    {%- endfor %}
    {%- for i in range(maxarity) %}
    {%- if i >= term.args|length %}
    indices.decRef(t.arg{{i}});
    t.arg{{i}} = 0;
    {%- endif %}
    {%- endfor %}
    {%- if not term.args %}
	nf[t.index] = true;
	{%- else %}
    nf[t.index] = false;
	{%-endif%}
    term[t.index] = t;
    {#printf("\tTerm[%u] is now {{term.hs.name}}, {%for i in range(maxarity)%}arg{{i}}:=%u, {%endfor%}\n",
			 t.index, {%for i in range(maxarity)%}t.arg{{i}}{{", " if not loop.last}} {%endfor%});#}
  {%- elif term.is_new_term %}
    printf("Could not rewrite term %u with type {{term.hs.name}} any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
  {%- endif %}
{%- endmacro %}

{%- macro print_acc(acc) %}
  {%- if acc.is_toplevel %}
  Term &acc = term[{{unfold_acc(acc)}}];
  t.head_symbol = acc.head_symbol;
  {% for i in range(maxarity) %}
  indices.decRef(t.arg{{i}});
  t.arg{{i}} = acc.arg{{i}};
  {%- endfor %}
  nf[t.index] = true;
  {%- endif %}
{%- endmacro %}

{%- macro print_rec(rec) %}
{%- if rec.term.is_acc %}is_{{rec.hs.name}}({{unfold_acc_(rec.term)}})
{%- elif rec.term.is_term %}is_{{rec.hs.name}}(t)
{%- endif %}
{%- endmacro %}

{%- macro print_if(i) %}
{%- if i.Preds %}if({%- for p in i.Preds%}{{print_rec(p)}}{{ " && " if not loop.last }}{%- endfor %}) {
    {{addrefs(i.VarCounts)}}
	{#{%- if i.Then.is_term %}{{collect(i.Collect)}}{%-endif%}#}
    {{print_statement(i.Then)}}
  } else {%- if i.Else.is_if %} {{print_statement(i.Else)}}
  {%- else %} {
  	{{print_statement(i.Else)}}
  } {% endif %}
{%- else %}
	{{addrefs(i.VarCounts)}}
    {#{%- if i.Then.is_term %}{{collect(i.Collect)}}{%-endif%}#}
    {{print_statement(i.Then)}}
{%- endif %}
{%- endmacro %}

{%- macro print_statement(stmt, varcounts) %}
{%- if stmt.is_acc %}{{print_acc(stmt)}}
{%- elif stmt.is_rec %}{{print_rec(stmt)}}
{%- elif stmt.is_term %}{{print_term(stmt)}}
{%- elif stmt.is_if %}{{print_if(stmt)}}
{%- endif %}
{%- endmacro %}

{% block rules %}
{% for function, rule in system.items()%}
__inline__ void Rewrite_{{function.name}}(Term &t, Term *term, bool *nf, Indices &indices) {
  {{print_statement(rule)}}
}
{% endfor %}
{% endblock %}
