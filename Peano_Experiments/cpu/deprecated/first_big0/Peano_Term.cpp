#include "Peano_Term.hpp"
#include <iostream>

bool verbose;
Peano_Term *term;
bool *in_normalform;

void debug(std::string msg) {
    if(verbose) std::cerr << msg << std::endl;
}

std::string Peano_Term::toString() const {
    switch(this->type) {
        case Zero:
            return "0";
            break;
        case Succ:
            return "S(" + term[this->lhs].toString() + ")";
            break;
        case Add:
            return "(" + term[this->lhs].toString() + " + " + term[this->rhs].toString() + ")";
            break;
        case Mult:
            return "(" + term[this->lhs].toString() + " x " + term[this->rhs].toString() + ")";
            break;
    }
}

void Rewrite_Succ(Peano_Term& succ) {
    if(!in_normalform[succ.lhs])
        term[succ.lhs].Rewrite();

    in_normalform[succ.t] = true;
}

void Rewrite_Add(Peano_Term &add) {
    if(!in_normalform[add.lhs]) { //leftmost innermost
        debug("ADD: Addition of non-normal lhs, rewriting lhs");
        term[add.lhs].Rewrite();
    }
    if(!in_normalform[add.rhs]) {
        debug("ADD: Addition of non-normal rhs, rewriting rhs");
        term[add.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply addition rewrite rules
    if(term[add.lhs].type == Zero) {
        debug("ADD: lhs is 0, result := rhs");
        in_normalform[add.t] = true;
        add.type = term[add.rhs].type;
        add.lhs = term[add.rhs].lhs;
        add.rhs = term[add.rhs].rhs;
        term[add.t] = add;
    } else { //term[add.lhs].type is Succ
        debug("ADD: rewriting S(n) + rhs to S(n + rhs)");

        Peano_Term &new_succ = add;
        Peano_Term new_add = Peano_Term(Add, next_free, term[add.lhs].lhs, add.rhs);
        term[next_free++] = new_add;

        new_succ.type = Succ;
        new_succ.lhs = new_add.t;

        new_add.Rewrite();

        in_normalform[new_add.t] = true;
        in_normalform[new_succ.t] = true;
        term[new_add.t] = new_add;
        //term[new_succ.t] = new_succ;
    }
}

void Rewrite_Mult(Peano_Term &mult) {
    if(!in_normalform[mult.lhs]) { //leftmost innermost
        debug("MULT: Multiplication of non-normal lhs, rewriting lhs " + term[mult.lhs].toString());
        term[mult.lhs].Rewrite();
    }
    if(!in_normalform[mult.rhs]) {
        debug("MULT: Multiplication of non-normal rhs, rewriting rhs" + term[mult.rhs].toString());
        term[mult.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply multiplication rewrite rules
    if(term[mult.lhs].type == Zero) {
        debug("MULT: Multiplication with 0, result := 0");
        term[mult.t].type = Zero;
        in_normalform[mult.t] = true;
    } else { //term[mult.lhs].type is Succ
        debug("MULT: rewriting S(n) * rhs to (n * rhs) + rhs");
        Peano_Term &new_add = mult;
        Peano_Term &new_mult = term[mult.lhs];
        Peano_Term &rhs = term[mult.rhs];

        new_add.type = Add;
        new_mult.type = Mult;

        new_add.lhs = new_mult.t;
        term[next_free] = Peano_Term(rhs.type, next_free, rhs.lhs, rhs.rhs); //TODO: requires deep clone to avoid sharing
        new_mult.rhs = next_free++;
        new_mult.Rewrite();
        new_add.Rewrite();
    }
}

void Peano_Term::Rewrite() {
    switch(this->type) {
        case Zero:
            break;
        case Succ:
            Rewrite_Succ(*this);
            break;
        case Add:
            Rewrite_Add(*this);
            break;
        case Mult:
            Rewrite_Mult(*this);
            break;
    }
}

//Constructor requires that terms are built from the leaves in order to deduce whether they should be normal form
Peano_Term::Peano_Term(Type type, unsigned int t, unsigned int lhs, unsigned int rhs)
    : type(type), t(t), lhs(lhs), rhs(rhs) {
    bool nf = (type == Zero) || ((type == Succ) && in_normalform[lhs]);
//    debug("Term " + std::to_string(t) + " is " + (nf ? "" : "NOT " ) + "in normal form");
    in_normalform[t] = nf;
}