#ifndef PEANO_EXPERIMENTS_PEANO_TERM_CUH
#define PEANO_EXPERIMENTS_PEANO_TERM_CUH

#include <string>

enum Type { Zero, Succ, Add, Mult, NOT_INITIALIZED };

struct term {
    Type type;
    unsigned int t = 0;
    unsigned int lhs = 0;
    unsigned int rhs = 0;
    term() { type = NOT_INITIALIZED; };
    term(Type type, unsigned int t, unsigned int lhs, unsigned int rhs)
        : type(type), t(t), lhs(lhs), rhs(rhs) {}
};

__global__ void Rewrite(unsigned int offset);

//global variables...
extern __device__ Peano_Term *term;
extern __device__ unsigned int next_free;
extern __device__ unsigned int root_idx;

#endif //PEANO_EXPERIMENTS_PEANO_TERM_CUH
