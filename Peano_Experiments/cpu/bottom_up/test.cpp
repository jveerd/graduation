#include <iostream>
#include <chrono>
#include "term.hpp"

#define DEPTH 3

unsigned int next_free;

int make_big_term(int depth, Type bottom, Type rest) {
    term = new Term[1<<(depth+1)];
    in_normalform = new bool[1<<(depth+1)];

    term[0] = Term(Zero, 0, 0, 0);

    for(unsigned int i = 1; i <= (1<<depth); i++) {
        term[i] = Term(bottom, i, 0, 0);
    }

    unsigned int start_index = (1<<depth);

    for(int d = depth-1; d >= 0; d--) {
        unsigned int end_index = start_index + (1<<d);
        unsigned int prev_lvl = start_index - (1<<(d+1));
        for(unsigned int i = start_index + 1; i <= end_index; i++) {
            unsigned int j = i - start_index;
            term[i] = Term(rest, i, prev_lvl + j*2-1, prev_lvl + j*2);
        }
        start_index = end_index;
    }

    next_free = start_index + 1;
    return start_index;
}

int make_big0(int depth) {
    return make_big_term(depth, Add, Add);
}


int make_big1(int depth) {
    return make_big_term(depth, Succ, Mult);
}

std::ostream& operator<<(std::ostream& os, const Term& pt) {
    os << pt.toString();
    return os;
}

int main(int argc, const char* argv[])
{
    int depth = DEPTH;
    int big = 1;
    if(argc > 2) {
        big = atoi(argv[2]);
        depth = atoi(argv[1]);
    } else if(argc > 1) {
        depth = atoi(argv[1]);
        std::cout << "No term specified, continuing with big1 with depth = " << depth << std::endl;
    } else {
        std::cout << "No depth provided, continuing with big1 with depth = " << DEPTH << std::endl;
    }

    using milli = std::chrono::milliseconds;
    auto start = std::chrono::high_resolution_clock::now();
    unsigned int root = (big == 0) ? make_big0(depth) : make_big1(depth);

    auto finish = std::chrono::high_resolution_clock::now();

    std::cout << "Initializing term on CPU took "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

    start = std::chrono::high_resolution_clock::now();

    unsigned int offset = 1;
    for(int d = depth; d >= 0; d--) {

        unsigned int kernels = (1<<d);
        for(int i = 0; i < kernels; i++) {
            unsigned int idx = offset + i;
            term[idx].Rewrite();
        }
        offset += kernels;
    }

    finish = std::chrono::high_resolution_clock::now();

    std::cout << "Done rewriting after "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

    std::cout << "Rewritten to: " << term[root].toString() << std::endl;
    return 0;
}