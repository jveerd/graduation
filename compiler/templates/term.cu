#include "term.cuh"
#include <cooperative_groups.h>
#include <stdio.h>

__device__ int atomicAggInc(unsigned int *ctr) {
  using namespace cooperative_groups;
  auto g = coalesced_threads();
  int warp_res;
  if(g.thread_rank() == 0)
    warp_res = atomicAdd(ctr, g.size());
  return g.shfl(warp_res, 0) + g.thread_rank();
}

{%- block rules %}{%- endblock%}

__global__ void frontier2wl(unsigned int *nelig, const unsigned int n_terms, const bool * const frontier, unsigned int *eligible_terms) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= n_terms)
	return;
  if(frontier[id])
	eligible_terms[atomicAggInc(nelig)] = id;
}

__global__ void refs2freepos(unsigned int *nfree, const unsigned int n_terms, const int * const refs, unsigned int *free_indices) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id < 2) return;
  if(id >= n_terms)	return;
  if(refs[id] <= 0) {
	free_indices[atomicAggInc(nfree)] = id;
  }
}

__global__ void propagate_refs(const unsigned int n_terms, int *refs) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= n_terms || id < 2)
	return;
  if(refs[id] == 0) {
	Term &my_term = term[id];
    refs[id] = -1;
	{%- for i in range(maxarity) %}
	if(my_term.arg{{i}} != 0) {
      atomicSub(&refs[my_term.arg{{i}}], 1);
	}{%-endfor%}
  }
}

__global__ void pad_freeindices(const unsigned int offset, const unsigned int required, unsigned int *next_free, unsigned int *free_indices) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= required)
	return;
  free_indices[id + offset] = atomicAggInc(next_free);
}

__global__ void Rewrite(bool *frontier, bool *nf, const unsigned int * const eligible_terms, const unsigned int *free_indices, bool *done, const unsigned int wl_size, unsigned int *next_free, int *refs) {
    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id >= wl_size)
	  return;
	unsigned int my_term_idx = eligible_terms[id];

    if(!nf[my_term_idx]) {
        Term &my_term = term[my_term_idx];
		// auto n = *next_free;
		// printf("Term[%u] with HS %s being rewritten\n\t Assigned ID: %u\n Nextfree: %u\n", my_term_idx, HS2String(my_term.head_symbol), free_indices[id], n);
        switch(my_term.head_symbol) {
		{% for f, rule in system.items()%}case {{ f.name }}:
		Rewrite_{{f.name}}(my_term, free_indices[id], next_free, nf, frontier, refs);
		  *done = false;
		break;
		{% endfor %}default:
		   nf[my_term_idx] = true;
		   frontier[my_term_idx] = false;
		   frontier[my_term.parent] = true;
		   *done = false;
		  break;
        }
    }
    else frontier[my_term_idx] = false;
}