#include <iostream>
#include "term.cuh"
#include "../bfs.cuh"
#include <chrono>

const unsigned int term_size = sizeof(Term);
const unsigned int frontier_size = sizeof(bool);
const unsigned int nf_size = sizeof(bool);
const unsigned int eligitem_size = sizeof(unsigned int);
const unsigned int ref_size = sizeof(int);
const unsigned int freeidx_size = sizeof(unsigned int);

int device;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess)
    {
	  fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	  if (abort) exit(code);
    }
}

cudaStream_t stream1, stream2, stream3, stream4;

__managed__ Term *term;
bool *frontier;
bool *nf;
unsigned int *eligible_terms;
unsigned int *free_indices;
int *refs;

std::string term2string(Term &t) {
  switch(t.head_symbol) {
	case Zero: {
	  //std::cout << "Zero " << t.index <<  std::endl;
  return std::string() + "Zero("  + ")";
	}
  case Succ: {
	  //std::cout << "Succ " << t.index <<  " arg0 " << t.arg0 << std::endl;
  return std::string() + "Succ("  + term2string(term[t.arg0])  + ")";
	}
  case Plus: {
	  //std::cout << "Plus " << t.index <<  " arg0 " << t.arg0 << " arg1 " << t.arg1 << std::endl;
  return std::string() + "Plus("  + term2string(term[t.arg0])  + ", " + term2string(term[t.arg1])  + ")";
	}
  case Mult: {
	  //std::cout << "Mult " << t.index <<  " arg0 " << t.arg0 << " arg1 " << t.arg1 << std::endl;
  return std::string() + "Mult("  + term2string(term[t.arg0])  + ", " + term2string(term[t.arg1])  + ")";
	}
  case Square: {
	  //std::cout << "Square " << t.index <<  " arg0 " << t.arg0 << std::endl;
  return std::string() + "Square("  + term2string(term[t.arg0])  + ")";
	}
  

  default:
	return "Type: " + std::string(HS2String(t.head_symbol));
  }
}
unsigned int Expand_Input(Term &t, unsigned int start_index, size_t num_terms) {
  gpuErrchk(cudaMemsetAsync(frontier, false, num_terms * frontier_size, stream1));
  gpuErrchk(cudaMemsetAsync(nf, false, num_terms * nf_size, stream2));
  bool *h_frontier;
  gpuErrchk(cudaMallocHost((void**)&h_frontier, num_terms * frontier_size));
  bool *h_nf;
  gpuErrchk(cudaMallocHost((void**)&h_nf, num_terms * nf_size));
  h_nf[0] = true;
  int *h_refs;
  gpuErrchk(cudaMallocHost((void**)&h_refs, num_terms * ref_size));
  memset(h_refs, 0, num_terms);

  
  
	unsigned int idx0 = start_index++;
    unsigned int parent0 = t.index ;
	unsigned int idx0_0 = start_index++;
    unsigned int parent0_0 = t.index ;
	unsigned int idx0_0_0 = start_index++;
    unsigned int parent0_0_0 = idx0_0 ;
    Term t0_0_0 = Term(Zero, idx0_0_0, 0, 0, parent0_0_0);
	term[idx0_0_0] = t0_0_0;
    h_refs[idx0_0_0]++;
	h_nf[idx0_0_0] = true;
    h_frontier[idx0_0_0] = false;
    h_frontier[parent0_0_0] = true;
    Term t0_0 = Term(Succ, idx0_0, t0_0_0.index, 0, parent0_0);
	term[idx0_0] = t0_0;
    h_refs[idx0_0]++;
	h_nf[idx0_0] = false;
    h_frontier[idx0_0] = true;
    Term t0 = Term(Succ, idx0, t0_0.index, 0, parent0);
	term[idx0] = t0;
    h_refs[idx0]++;
	h_nf[idx0] = false;
    h_frontier[idx0] = true;

	t.head_symbol = Square;
    h_refs[t.index] = 1;
    t.arg0 = t0.index;
    t.arg1 = 0;
	h_nf[t.index] = false;
	h_frontier[t.index] = true;


  std::cout << "Input: " << term2string(t) << std::endl;

  gpuErrchk(cudaMemPrefetchAsync(term, num_terms*term_size, device, stream1));
  gpuErrchk(cudaMemcpyAsync((void*)frontier, h_frontier, start_index*frontier_size, cudaMemcpyHostToDevice, stream2));
  gpuErrchk(cudaMemcpyAsync((void*)nf, h_nf, start_index*nf_size, cudaMemcpyHostToDevice, stream3));
  gpuErrchk(cudaMemcpyAsync((void*)refs, h_refs, start_index*ref_size, cudaMemcpyHostToDevice, stream4));

  gpuErrchk(cudaStreamSynchronize(stream2));
  gpuErrchk(cudaStreamSynchronize(stream3));
  gpuErrchk(cudaStreamSynchronize(stream4));

  gpuErrchk(cudaFreeHost(h_frontier));
  gpuErrchk(cudaFreeHost(h_nf));
  gpuErrchk(cudaFreeHost(h_refs));
  
  return start_index;
}

__global__ void printrefs(int *refs, int n_terms) {
  unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= n_terms)
	return;
  printf("refs[%u] = %u\n", id, refs[id]);
}

void dump_memory(unsigned int n) {
  for(int i = 1; i < n; i++) {
	std::cout << "Term[" << i << "] = " << term2string(term[i]) << "\n\t(arg0 = " << term[i].arg0 << ", arg1 = " << term[i].arg1 << ")\n";
  }
}

int main(int argc, const char* argv[]) {
  size_t free, total;
  gpuErrchk(cudaMemGetInfo(&free, &total));

  cudaStreamCreate(&stream1);
  cudaStreamCreate(&stream2);
  cudaStreamCreate(&stream3);
  cudaStreamCreate(&stream4);

  gpuErrchk(cudaGetDevice(&device));
  cudaDeviceProp devProp;
  cudaGetDeviceProperties(&devProp,device);

  unsigned int rewrite_tpb = 32;
  unsigned int other_tpb = 128;

  unsigned int total_size = term_size + frontier_size + nf_size + eligitem_size + ref_size + freeidx_size;
  unsigned int num_terms = free / total_size - 10;

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms * term_size));
  gpuErrchk(cudaMalloc((void**)&frontier, num_terms * frontier_size));
  gpuErrchk(cudaMalloc((void**)&nf, num_terms * nf_size));
  gpuErrchk(cudaMalloc((void**)&eligible_terms, num_terms * eligitem_size));
  gpuErrchk(cudaMalloc((void**)&refs, num_terms * ref_size));
  gpuErrchk(cudaMalloc((void**)&free_indices, num_terms * freeidx_size));
  
  term[0] = Term(NOT_INITIALIZED, 0, 0, 0,  0);
  unsigned int root = 1;
  term[root] = Term(Input, root, 0, 0,  0);

  unsigned int end_index = Expand_Input(term[root], 2, num_terms);
  
  unsigned int *next_free;
  unsigned int n_terms;
  cudaMallocManaged((void**)&next_free, sizeof(unsigned int));
  *next_free = n_terms = end_index;

  unsigned int numBlocks = (unsigned int)ceil((double)n_terms / (double) other_tpb);
  unsigned int numThreads = other_tpb;

  // Find first frontier of rewritable terms
  bool *done;
  cudaMallocManaged((void**)&done, sizeof(bool));
  *done = false;

  auto beginTime = std::chrono::high_resolution_clock::now();
  
  unsigned int terms_handled = 0;
  while(!(*done)) {
	terms_handled += numBlocks * numThreads;
	*done = true;
	gpuErrchk(cudaMemPrefetchAsync(done, sizeof(bool), device, stream2));
	bfs_kernel<<<numBlocks, numThreads, 0, stream1>>>(frontier, nf, done, n_terms);
	gpuErrchk(cudaMemPrefetchAsync(done, sizeof(bool), cudaCpuDeviceId, stream1));
	gpuErrchk(cudaStreamSynchronize(stream1));
	gpuErrchk(cudaPeekAtLastError());
  }

  unsigned int *nfree;
  
  cudaMallocManaged((void**)&nfree, sizeof(unsigned int));
  
  *nfree = 0;
  refs2freepos<<<numBlocks, numThreads, 0, stream1>>>(nfree, n_terms, refs, free_indices);
  gpuErrchk(cudaPeekAtLastError());
  //gpuErrchk(cudaStreamSynchronize(stream1));
  
  unsigned int *wl_size;
  cudaMallocManaged((void**)&wl_size, sizeof(unsigned int));
  *wl_size = 0;
  frontier2wl<<<numBlocks, numThreads, 0, stream2>>>(wl_size, n_terms, frontier, eligible_terms);  
  gpuErrchk(cudaPeekAtLastError());

  gpuErrchk(cudaStreamSynchronize(stream1));
  gpuErrchk(cudaStreamSynchronize(stream2));
  
  *done = false;
  auto rewrites = 0;
  auto _wl_size = *wl_size;
  auto _nfree = *nfree;

  int leftover = _nfree - _wl_size;
  
  if(leftover < 0) {
	int required = _wl_size - _nfree;
	int offset = _wl_size - required;
	unsigned int threads = 32;
	unsigned int blocks = (unsigned int) ceil( (double)required / (double)threads );
	pad_freeindices<<<blocks, threads>>>(offset, required, next_free, free_indices);
	gpuErrchk(cudaDeviceSynchronize());
	gpuErrchk(cudaPeekAtLastError());
  }

  while(_wl_size && !(*done)) {
	printrefs<<<numBlocks, numThreads>>>(refs, n_terms);
	gpuErrchk(cudaDeviceSynchronize());
	gpuErrchk(cudaPeekAtLastError());

	dump_memory(n_terms);
	terms_handled += _wl_size;
	rewrites++;

	*done = true;

	numBlocks = (unsigned int)ceil((double)*next_free / (double) rewrite_tpb);
	numThreads = rewrite_tpb;
	gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), device, stream1));

	Rewrite<<<numBlocks, numThreads, 0, stream2>>>(frontier, nf, eligible_terms, free_indices, done, _wl_size, next_free, refs);
	gpuErrchk(cudaStreamSynchronize(stream2));
	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), cudaCpuDeviceId, stream2));
	gpuErrchk(cudaStreamSynchronize(stream2));
	gpuErrchk(cudaPeekAtLastError());
	n_terms = *next_free;

	if(!(*done)) {

	  numBlocks = (unsigned int)ceil((double)n_terms / (double) other_tpb);
	  numThreads = other_tpb;

	  while(!(*done)) {
		terms_handled += n_terms;
		*done = true;
		gpuErrchk(cudaMemPrefetchAsync(done, sizeof(bool), device, stream2));
		bfs_kernel<<<numBlocks, numThreads, 0, stream1>>>(frontier, nf, done, n_terms);
		gpuErrchk(cudaMemPrefetchAsync(done, sizeof(bool), cudaCpuDeviceId, stream1));
		gpuErrchk(cudaStreamSynchronize(stream1));
		gpuErrchk(cudaPeekAtLastError());
	  }
	  
	  *done = false;

	  *wl_size = 0;
	  frontier2wl<<<numBlocks, numThreads, 0, stream1>>>(wl_size, n_terms, frontier, eligible_terms);
	  gpuErrchk(cudaPeekAtLastError());

	  *nfree = 0;
	  refs2freepos<<<numBlocks, numThreads, 0, stream2>>>(nfree, n_terms, refs, free_indices);
	  gpuErrchk(cudaPeekAtLastError());
	  
	  gpuErrchk(cudaStreamSynchronize(stream1));
	  gpuErrchk(cudaStreamSynchronize(stream2));
	  
	  _wl_size = *wl_size;
	  _nfree = *nfree;
	  
	  leftover = _nfree - _wl_size;
	  
	  if(leftover < 0) {
		gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), device, stream1));
		int required = _wl_size - _nfree;
		int offset = _wl_size - required;
		unsigned int threads = 32;
		unsigned int blocks = (unsigned int) ceil( (double)required / (double)threads );
		std::cout << "numfree: " << _nfree << " total terms: " << n_terms << std::endl;
		std::cout << "threads 32 blocks " << blocks << " (required " << required << ")\n" << std::endl;
		pad_freeindices<<<blocks, threads>>>(offset, required, next_free, free_indices);
		gpuErrchk(cudaDeviceSynchronize());
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), cudaCpuDeviceId, stream2));
		n_terms = *next_free;
	  }
	}
	std::cout << "Term --" << rewrites << "-->: " + term2string(term[root]) << std::endl;
	std::cout << "_wl_size " << _wl_size << std::endl;
  }
  std::cout << "Output: " + term2string(term[root]) << std::endl;
  auto endTime = std::chrono::high_resolution_clock::now();
#ifdef VERBOSE
  long long milliseconds = std::chrono::duration_cast<
	std::chrono::milliseconds>(endTime - beginTime).count();
  double seconds = (double)milliseconds / (double)1000;
  double tps = (double)terms_handled / seconds;

  std::cout.precision(0);
  std::cout << std::fixed << "BFSing and rewriting took " << milliseconds << " ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific << terms_handled << "\n T/S: " << tps << std::endl;
#endif

  std::cout << "Highest pointer to occupied memory: " << *next_free << std::endl;
  gpuErrchk(cudaDeviceSynchronize());
    
  return 0;
}