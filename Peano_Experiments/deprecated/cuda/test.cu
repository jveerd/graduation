#include <iostream>
#include <chrono>
#include "Peano_Term.cuh"

#define DEPTH 20

//bool verbose;
__device__ Peano_Term *term;
__device__ unsigned int next_free;
__device__ unsigned int root_idx;

//__global__ void make_big0(term *term_ptr, int depth) {
//    term = term_ptr;
//
//    term[0] = term(Zero, 0, 0, 0);
////    printf("Created zero.\n");
//
//    for(unsigned int i = 1; i <= (1<<depth); i++) {
//        term[i] = term(Add, i, 0, 0);
//    }
////    printf("Populated level %d of array\n", depth);
//
//    unsigned int start_index = (1<<depth);
//
//    for(int d = depth-1; d >= 0; d--) {
//        unsigned int end_index = start_index + (1<<d);
//        unsigned int prev_lvl = start_index - (1<<(d+1));
//        for(unsigned int i = start_index + 1; i <= end_index; i++) {
//            unsigned int j = i - start_index;
//            term[i] = term(Add, i, prev_lvl + j*2-1, prev_lvl + j*2);
//        }
////        printf("Populated level %d of array\n", d);
//        start_index = end_index;
//    }
//    next_free = start_index + 1;
//    root_idx = start_index;
//}

__global__ void init(term *term_ptr, unsigned int d_next_free, unsigned int d_root_idx) {
    term = term_ptr;
    next_free = d_next_free;
    root_idx = d_root_idx;
}

void make_big0(term *d_ptr, int depth) {

    term *h_term;
    size_t terms = 1<<(depth+1);
    size_t bytes = terms * sizeof(term);
    printf("Term (of %zu subterms) is going to take %zu bytes.\n", terms, bytes);
//    std::cout << "Term (of " << terms << " subterms) is going to take " << std::to_string(bytes) << " bytes.\n";

    cudaError_t err = cudaMallocHost((void**) &h_term, bytes);
    if(err != cudaSuccess) {
        std::cout << "Could not allocate pinned memory on host: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    h_term[0] = term(Zero, 0, 0, 0);

    for(unsigned int i = 1; i <= (1<<depth); i++) {
        h_term[i] = term(Add, i, 0, 0);
    }

    unsigned int start_index = (1<<depth);

    for(int d = depth-1; d >= 0; d--) {
//        std::cout << "Populating level " << d << " of term.\n";

        unsigned int end_index = start_index + (1<<d);
        unsigned int prev_lvl = start_index - (1<<(d+1));
        for(unsigned int i = start_index + 1; i <= end_index; i++) {
            unsigned int j = i - start_index;
            h_term[i] = term(Add, i, prev_lvl + j*2-1, prev_lvl + j*2);
        }
//        std::cout << "term[" << end_index <<"] written: " /*<< term[end_index]*/ << std::endl;
        start_index = end_index;
    }
    printf("Starting transfer to device\n");
    err = cudaMemcpy((void*) d_ptr, (void*) h_term, bytes, cudaMemcpyHostToDevice);
//    cudaDeviceSynchronize();
    if(err != cudaSuccess) {
        std::cout << "Could not transfer term array to device: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    printf("Going to free pinned host memory now...\n");
    err = cudaFreeHost(h_term);
    if(err != cudaSuccess) {
        std::cout << "Could not free pinned memory on host: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    init<<<1,1>>>(d_ptr, start_index + 1, start_index);
}

//__global__ void make_2x2x2x2(term *term_ptr) {
//    term = term_ptr;
//
//    term[0] = term(Zero, 0, 0, 0); //zero
//
//    term[1] = term(Succ, 1, 0, 0); //lone1
//    term[2] = term(Succ, 2, 0, 0); //lone2
//    term[3] = term(Succ, 3, 0, 0); //rone1
//    term[4] = term(Succ, 4, 0, 0); //rone2
//
//    term[5] = term(Succ, 5, 1, 0); //ltwo1
//    term[6] = term(Succ, 6, 2, 0); //ltwo2
//    term[7] = term(Succ, 7, 3, 0); //rtwo1
//    term[8] = term(Succ, 8, 4, 0); //rtwo2
//
//    term[9] = term(Mult, 9, 5, 6); //mul1 = ltwo1 * ltwo2
//    term[10] = term(Mult, 10, 7, 8); //mul2 = rtwo1 * rtwo2
//
//    term[11] = term(Mult, 11, 9, 10); //mul = mul1 * mul2
//    next_free = 12;
//    root_idx = 11;
//}

__global__ void Report_Result() {
    int succs = 0;
    term &temp = term[root_idx];
    if(temp.type != Zero) printf("root is not zero...\n");
    while(temp.type != Zero) {
        succs++;
        temp = term[temp.lhs];
    }
    printf("Result: %d\n", succs);
}


int main(int argc, const char* argv[])
{
    printf("sizeof term = %zu, sizeof size_t = %zu, SIZE_MAX = %zu\n", sizeof(term), sizeof(size_t), SIZE_MAX);
    using milli = std::chrono::milliseconds;
    int depth;
    if(argc > 1) {
        depth = atoi(argv[1]);
    } else {
        std::cout << "No depth provided, continuing with DEPTH=" << DEPTH << std::endl;
        depth = DEPTH;
    }

    cudaDeviceReset();

//    cudaError_t err = cudaDeviceSetLimit(cudaLimitDevRuntimeSyncDepth, (depth+2));
//    if(err != cudaSuccess) {
//        std::cout << "Could not set runtimesyncdepth limit: " << cudaGetErrorString(err) << std::endl;
//        exit(1);
//    }
//
//    err = cudaDeviceSetLimit(cudaLimitDevRuntimePendingLaunchCount,
//                             2048);
//    if(err != cudaSuccess) {
//        std::cout << "Could not set pendinglaunchcount limit: " << cudaGetErrorString(err) << std::endl;
//        exit(1);
//    }

//    size_t free;
//    size_t total;
//    cudaMemGetInfo(&free, &total);
//    std::cout << "Currently, GPU has " << free << " bytes free out of a total of " << total << " bytes of memory.\n";

    size_t req = 1<<(depth+1);
    term *d_ptr;
    cudaError_t err = cudaMalloc((void**)&d_ptr, req*sizeof(term));
    if(err != cudaSuccess) {
        std::cout << "Could not cudaMalloc for term array: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

//    bool *d_nf_ptr;
//    err = cudaMalloc((void**)&d_nf_ptr, req*sizeof(bool));
//    if(err != cudaSuccess) {
//        std::cout << "Could not cudaMalloc for nf array : " << cudaGetErrorString(err) << std::endl;
//        exit(1);
//    }

//    cudaMemGetInfo(&free, &total);
//    std::cout << "After allocations, GPU has " << free << " bytes free out of a total of " << total << " bytes of memory.\n";

//    make_2x2x2x2<<<1,1>>>(d_ptr, d_nf_ptr);

    auto start = std::chrono::high_resolution_clock::now();

    make_big0(d_ptr, depth);

    auto finish = std::chrono::high_resolution_clock::now();

    std::cout << "Initializing term and storing in GPU took "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

//    cudaDeviceSynchronize();
//    err = cudaGetLastError();
//    if(err != cudaSuccess) {
//        std::cout << "An error occurred while initializing the term: " << cudaGetErrorString(err) << std::endl;
//        exit(1);
//    }

    start = std::chrono::high_resolution_clock::now();

    unsigned int offset = 1;
    for(int d = depth; d >= 0; d--) {
//        auto level_start = std::chrono::high_resolution_clock::now();

        unsigned int kernels = (1<<d);
        Rewrite<<<kernels, 1>>>(offset);
        cudaDeviceSynchronize();
        err = cudaGetLastError();
        if(err != cudaSuccess) {
            std::cout << "An error occurred on level " << d << " of the term: " << cudaGetErrorString(err) << std::endl;
            exit(1);
        }
        offset += kernels;

//        auto level_finish = std::chrono::high_resolution_clock::now();
//
//        std::cout << "Level " << d << " of " << kernels << " kernels computed in "
//                  << std::chrono::duration_cast<milli>(level_finish - level_start).count()
//                  << " milliseconds\n";
    }


//    rewrite_Root<<<1,1>>>();
//    cudaDeviceSynchronize();
//    err = cudaGetLastError();
//    if(err != cudaSuccess) {
//        std::cout << "An error occurred while rewriting the term: " << cudaGetErrorString(err) << std::endl;
//        exit(1);
//    }

    finish = std::chrono::high_resolution_clock::now();

    std::cout << "Done rewriting after "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

    Report_Result<<<1,1>>>();
    cudaDeviceSynchronize();
    err = cudaGetLastError();
    if(err != cudaSuccess) {
        std::cout << "An error occurred while printing the result: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    cudaFree(d_ptr);
//    cudaFree(d_nf_ptr);
    cudaDeviceReset();
    return 0;
}
