cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
set (CMAKE_CXX_STANDARD 11)

add_executable(test_cpu
	../generated/term.cpp
	../generated/test.cpp)
