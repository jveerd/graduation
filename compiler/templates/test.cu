#include <iostream>
#include "term.cuh"
#include "../bfs.cuh"
#include <chrono>

const unsigned int term_size = sizeof(Term);
const unsigned int frontier_size = sizeof(bool);
const unsigned int nf_size = sizeof(bool);
const unsigned int eligitem_size = sizeof(unsigned int);
const unsigned int ref_size = sizeof(int);
const unsigned int freeidx_size = sizeof(unsigned int);

int device;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess)
    {
	  fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	  if (abort) exit(code);
    }
}

cudaStream_t stream1, stream2, stream3, stream4;

__managed__ Term *term;
bool *frontier;
bool *nf;
unsigned int *eligible_terms;
unsigned int *free_indices;
int *refs;

std::string term2string(Term &t) {
  switch(t.head_symbol) {
	{% for f in functions%}case {{f.name}}: {
  return std::string() + "{{f.name}}(" {% for i in range(f.formals|length) %} + term2string(term[t.arg{{i}}]) {{ ' + ", "' if not loop.last}}{%- endfor %} + ")";
	}
  {%endfor%}

  default:
	return "Type: " + std::string(HS2String(t.head_symbol));
  }
}

{%- block input %} {%- endblock %}

{# __global__ void printrefs(int *refs, int n_terms) {
  unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
  if(id >= n_terms)
	return;
  printf("refs[%u] = %u\n", id, refs[id]);
  } #}

{#void dump_memory(unsigned int n) {
  for(int i = 1; i < n; i++) {
	std::cout << "Term[" << i << "] = " << term2string(term[i]) << "\n\t(arg0 = " << term[i].arg0 << ", arg1 = " << term[i].arg1 << ", arg2 = " << term[i].arg2 << ")\n";
  }
  }#}

int main(int argc, const char* argv[]) {
  size_t free, total;
  gpuErrchk(cudaMemGetInfo(&free, &total));

  cudaStreamCreate(&stream1);
  cudaStreamCreate(&stream2);
  cudaStreamCreate(&stream3);
  cudaStreamCreate(&stream4);

  gpuErrchk(cudaGetDevice(&device));
  cudaDeviceProp devProp;
  cudaGetDeviceProperties(&devProp,device);

  unsigned int rewrite_tpb = 128;
  unsigned int other_tpb = 1024;

  unsigned int total_size = term_size + frontier_size + nf_size + eligitem_size + ref_size + freeidx_size;
  unsigned int num_terms = free / total_size - 10;

  std::cout << "Max num_terms: " << num_terms << std::endl;

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms * term_size));
  gpuErrchk(cudaMalloc((void**)&frontier, num_terms * frontier_size));
  gpuErrchk(cudaMalloc((void**)&nf, num_terms * nf_size));
  gpuErrchk(cudaMalloc((void**)&eligible_terms, num_terms * eligitem_size));
  gpuErrchk(cudaMalloc((void**)&refs, num_terms * ref_size));
  gpuErrchk(cudaMalloc((void**)&free_indices, num_terms * freeidx_size));
  
  term[0] = Term(NOT_INITIALIZED, 0, {%- for i in range(maxarity) %} 0, {%- endfor %}  0);
  unsigned int root = 1;
  term[root] = Term(Input, root, {%- for i in range(maxarity) %} 0, {%- endfor %}  0);

  unsigned int end_index = Expand_Input(term[root], 2, num_terms);
  
  unsigned int *next_free;
  unsigned int n_terms;
  cudaMallocManaged((void**)&next_free, sizeof(unsigned int));
  *next_free = n_terms = end_index;

  unsigned int numBlocks = (unsigned int)ceil((double)n_terms / (double) other_tpb);
  unsigned int numThreads = other_tpb;

  // Find first frontier of rewritable terms
  bool *done;
  cudaMallocManaged((void**)&done, sizeof(bool));
  *done = false;

  auto beginTime = std::chrono::high_resolution_clock::now();
  
  unsigned int terms_handled = 0;
  while(!(*done)) {
	terms_handled += n_terms;
	*done = true;
	bfs_kernel<<<numBlocks, numThreads, 0, stream1>>>(frontier, nf, done, n_terms);
	gpuErrchk(cudaStreamSynchronize(stream1));
	gpuErrchk(cudaPeekAtLastError());
  }

  unsigned int *nfree;
  
  cudaMallocManaged((void**)&nfree, sizeof(unsigned int));
  
  *nfree = 0;
  refs2freepos<<<numBlocks, numThreads, 0, stream1>>>(nfree, n_terms, refs, free_indices);
  gpuErrchk(cudaPeekAtLastError());
  
  unsigned int *wl_size;
  cudaMallocManaged((void**)&wl_size, sizeof(unsigned int));
  *wl_size = 0;
  frontier2wl<<<numBlocks, numThreads, 0, stream2>>>(wl_size, n_terms, frontier, eligible_terms);  
  gpuErrchk(cudaPeekAtLastError());

  gpuErrchk(cudaStreamSynchronize(stream1));
  gpuErrchk(cudaStreamSynchronize(stream2));

  terms_handled += 2*n_terms;
  
  *done = false;
  auto rewrites = 0;
  long long rewritten = 0;
  auto _wl_size = *wl_size;
  auto _nfree = *nfree;

  int leftover = _nfree - _wl_size;
  
  if(leftover < 0) {
	int required = _wl_size - _nfree;
	int offset = _wl_size - required;
	unsigned int threads = 32;
	unsigned int blocks = (unsigned int) ceil( (double)required / (double)threads );
	pad_freeindices<<<blocks, threads>>>(offset, required, next_free, free_indices);
	gpuErrchk(cudaDeviceSynchronize());
	gpuErrchk(cudaPeekAtLastError());
  }

  while(_wl_size && !(*done)) {
	terms_handled += _wl_size;
	rewrites++;
	rewritten += _wl_size;

	*done = true;

	numBlocks = (unsigned int)ceil((double)*next_free / (double) rewrite_tpb);
	numThreads = rewrite_tpb;
	
	Rewrite<<<numBlocks, numThreads, 0, stream2>>>(frontier, nf, eligible_terms, free_indices, done, _wl_size, next_free, refs);
	gpuErrchk(cudaStreamSynchronize(stream2));
	gpuErrchk(cudaPeekAtLastError());
	n_terms = *next_free;

	if(!(*done)) {

	  numBlocks = (unsigned int)ceil((double)n_terms / (double) other_tpb);
	  numThreads = other_tpb;

	  *nfree = 0;
	  refs2freepos<<<numBlocks, numThreads, 0, stream2>>>(nfree, n_terms, refs, free_indices);
	  gpuErrchk(cudaPeekAtLastError());
	  terms_handled += n_terms;

	  while(!(*done)) {
		terms_handled += 2*n_terms;
		*done = true;
		bfs_kernel<<<numBlocks, numThreads, 0, stream1>>>(frontier, nf, done, n_terms);
		propagate_refs<<<numBlocks, numThreads, 0, stream3>>>(n_terms, refs);
		gpuErrchk(cudaStreamSynchronize(stream1));
		gpuErrchk(cudaPeekAtLastError());
	  }
	  
	  *done = false;
	  
	  *wl_size = 0;
	  frontier2wl<<<numBlocks, numThreads, 0, stream1>>>(wl_size, n_terms, frontier, eligible_terms);
	  gpuErrchk(cudaPeekAtLastError());
	  
	  gpuErrchk(cudaStreamSynchronize(stream2));
	  gpuErrchk(cudaStreamSynchronize(stream1));
	  terms_handled += n_terms;
	  
	  _wl_size = *wl_size;
	  _nfree = *nfree;
	  
	  leftover = _nfree - _wl_size;
	  
	  if(leftover < 0) {
		int required = _wl_size - _nfree;
		int offset = _wl_size - required;
		unsigned int threads = 32;
		unsigned int blocks = (unsigned int) ceil( (double)required / (double)threads );
		pad_freeindices<<<blocks, threads>>>(offset, required, next_free, free_indices);
		gpuErrchk(cudaDeviceSynchronize());
		gpuErrchk(cudaPeekAtLastError());
		n_terms = *next_free;
	  }
	}
	//std::cerr << "Term --" << rewrites << "-->: " + term2string(term[root]) << std::endl;

	  auto intermediate_time = std::chrono::high_resolution_clock::now();
	  auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(intermediate_time - beginTime).count();
	  std::cerr << std::fixed << ms  << " " << _wl_size << "\n";	
  }
  auto endTime = std::chrono::high_resolution_clock::now();
  //std::cout << "Output: " + term2string(term[root]) << std::endl;
#ifdef VERBOSE
  long long milliseconds = std::chrono::duration_cast<
	std::chrono::milliseconds>(endTime - beginTime).count();
  double seconds = (double)milliseconds / (double)1000;
  double tps = (double)terms_handled / seconds;
  double rps = (double)rewritten / seconds;

  std::cout.precision(0);
  std::cout << std::fixed /*<< "Finished in "*/ << milliseconds << " ";//ms.";
  //std::cout << " Number of rewrites: " << rewrites << ". Number of terms rewritten: " << rewritten;
  std::cout /*<< ".\n Average terms per rewrite: "*/ << rewritten / rewrites << " ";//. Total terms handled: ";
  
  std::cout.precision(4);
  std::cout << std::scientific // << terms_handled << "\n T/S: " 
			<< tps;// << std::endl;
  std::cout << " " << /*"R/S: " <<*/ rps;// << std::endl;
  std::cout << /*"Highest pointer to occupied memory: " <<*/ " " << *next_free << std::endl;
#endif

  gpuErrchk(cudaDeviceSynchronize());
    
  return 0;
}
