#include "../bfs.cuh"

__global__ void bfs_kernel(Term *term, bool *frontier, bool *nf, bool *done) {
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    bool f = frontier[id];
    __syncthreads();

    if (f)
    {
        Term &my_term = term[id];
        if(nf[my_term.arg0] && nf[my_term.arg1] && nf[my_term.arg2]) return;
        else if(!nf[my_term.arg0]) {
			 if(!nf[my_term.arg1]) {
			   if(!nf[m
			 }
            frontier[id] = false;
            frontier[my_term.arg0] = true;
            *done = false;
        }
        else if(!nf[my_term.arg1]) {
            frontier[id] = false;
            frontier[my_term.arg1] = true;
            *done = false;
        }
        else if(!nf[my_term.arg2]) {
            frontier[id] = false;
            frontier[my_term.arg2] = true;
            *done = false;
        }
        else {
            frontier[id] = false;
			frontier[my_term.arg0] = true;
			frontier[my_term.arg1] = true;
			frontier[my_term.arg2] = true;
            *done = false;
        }
    }
}