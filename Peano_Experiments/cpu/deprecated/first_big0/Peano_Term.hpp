#ifndef PEANO_EXPERIMENTS_PEANO_TERM_HPP
#define PEANO_EXPERIMENTS_PEANO_TERM_HPP

#include <string>

enum Type { Zero, Succ, Add, Mult, NOT_INITIALIZED };

struct Peano_Term {
    Type type;
    unsigned int t = 0;
    unsigned int lhs = 0;
    unsigned int rhs = 0;
    Peano_Term() { type = NOT_INITIALIZED; };
    Peano_Term(Type type, unsigned int t, unsigned int lhs, unsigned int rhs);
    ~Peano_Term() {}
    void Rewrite();
    std::string toString() const;
};

//global variables...
extern bool verbose;
extern Peano_Term *term;
extern bool *in_normalform;
extern unsigned int next_free;

#endif //PEANO_EXPERIMENTS_PEANO_TERM_HPP
