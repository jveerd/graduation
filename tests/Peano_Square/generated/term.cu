#include "term.cuh"



inline void __device__ Rewrite_Mult(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Mult(t) && is_Succ(term[t.arg0])) {
    
	Term &acc0 = term[t.arg1];
	
	Term &acc1_0 = term[term[t.arg0].arg0];
	Term &acc1_1 = term[t.arg1];
	unsigned int idx1 = atomicAdd(&next_free, 1);
    Term t1 = Term(Mult, idx1, acc1_0.t, acc1_1.t, t.t);
	term[idx1] = t1;
	nf[idx1] = false;
	frontier[idx1] = true;
	t.type = Plus;
    t.arg0 = acc0.t;
    t.arg1 = t1.t;
    frontier[t.t] = true;
  } else if(is_Mult(t) && is_Zero(term[t.arg0])) {
    
	t.type = Zero;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Mult any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Square(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Square(t)) {
    
	Term &acc0 = term[t.arg0];
	Term &acc1 = term[t.arg0];
	t.type = Mult;
    t.arg0 = acc0.t;
    t.arg1 = acc1.t;
    frontier[t.t] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Square any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Plus(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Plus(t) && is_Succ(term[t.arg0])) {
    
	
	Term &acc0_0 = term[term[t.arg0].arg0];
	Term &acc0_1 = term[t.arg1];
	unsigned int idx0 = atomicAdd(&next_free, 1);
    Term t0 = Term(Plus, idx0, acc0_0.t, acc0_1.t, t.t);
	term[idx0] = t0;
	nf[idx0] = false;
	frontier[idx0] = true;
	t.type = Succ;
    t.arg0 = t0.t;
    frontier[t.t] = true;
  } else if(is_Plus(t) && is_Zero(term[t.arg0])) {
    
  Term &acc = term[t.arg1];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Plus any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}




__global__ void Rewrite(Term *term, bool *done, bool *frontier, bool *nf) {
    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    bool do_work = frontier[id] && !nf[id];
    __syncthreads();
    if(do_work) {
        Term &my_term = term[id];

        switch(my_term.type) {
		case Mult:
		  Rewrite_Mult(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Square:
		  Rewrite_Square(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Plus:
		  Rewrite_Plus(my_term, term, frontier, nf);
		  *done = false;
		break;
		default:
		   nf[id] = true;
		   frontier[id] = false;
		   frontier[my_term.parent] = true;
		   *done = false;
		  break;
        }
    }
    else if(nf[id]) frontier[id] = false;
}