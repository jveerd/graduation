//#include "clionhelper.cuh"
#ifndef POINTER_POLY_H
#define POINTER_POLY_H
#include <cuda.h>
#include <cuda_runtime.h>
#include <iostream>

class Peano_Term {
private:
    bool normal = false;
public:
    //__device__ virtual ~Peano_Term() {}
    __device__ bool is_normalform() { return normal; };
    __device__ void make_normal() { this->normal = true; };
    __device__ virtual bool is_zero() { return false; };
    __device__ virtual Peano_Term* Rewrite() = 0;
};

class Peano_Zero : public Peano_Term {
public:
    __device__ Peano_Zero() { this->make_normal(); }
    /*__device__ ~Peano_Zero() {
        //debug("ZERO: Deleting a Peano_Zero");
    }*/
    __device__ Peano_Term* Rewrite();
    __device__ bool is_zero() { return true; };
};

class Peano_Succ : public Peano_Term {
public:
    __device__ Peano_Succ(Peano_Term* t) : n(t) {
        if(t->is_normalform()) make_normal();
    }
    Peano_Term *n;
    /*__device__ ~Peano_Succ() {
//        debug("SUCC: Deleting a Peano_Succ");
        delete n;
        n = NULL;
    }*/
    __device__ Peano_Term* Rewrite();
};

class Peano_Add : public Peano_Term {
public:
    __device__ Peano_Add(Peano_Term* lhs, Peano_Term* rhs) : lhs(lhs), rhs(rhs) {	}
    Peano_Term *lhs;
    Peano_Term *rhs;
    /*__device__ ~Peano_Add() {
        //debug("ADD: Deleting a Peano_Add");
        delete lhs;
        lhs = NULL;
        delete rhs;
        rhs = NULL;
    }*/
    __device__ Peano_Term* Rewrite();
};

class Peano_Mult : public Peano_Term {
public:
    __device__ Peano_Mult(Peano_Term* lhs, Peano_Term* rhs) : lhs(lhs), rhs(rhs) { }
    Peano_Term *lhs;
    Peano_Term *rhs;
    /*__device__ ~Peano_Mult() {
        //debug("MULT: Deleting a Peano_Mult");
        delete lhs;
        lhs = NULL;
        delete rhs;
        rhs = NULL;
    }*/
    __device__ Peano_Term* Rewrite();
};
#endif //POINTER_POLY_H