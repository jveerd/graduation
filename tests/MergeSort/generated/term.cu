#include "term.cuh"



inline void __device__ Rewrite_Merge(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Merge(t) && is_SortedCons(term[t.arg0]) && is_SortedCons(term[t.arg1])) {
    
	
	Term &acc0_0 = term[term[t.arg0].arg0];
	Term &acc0_1 = term[term[t.arg1].arg0];
	unsigned int idx0 = atomicAdd(&next_free, 1);
    Term t0 = Term(Lt, idx0, acc0_0.t, acc0_1.t, 0, t.t);
	term[idx0] = t0;
	nf[idx0] = false;
	frontier[idx0] = true;
	
	Term &acc1_0 = term[term[t.arg0].arg0];
	
	Term &acc1_1_0 = term[term[t.arg0].arg1];
	
	Term &acc1_1_1_0 = term[term[t.arg1].arg0];
	Term &acc1_1_1_1 = term[term[t.arg1].arg1];
	unsigned int idx1_1_1 = atomicAdd(&next_free, 1);
    Term t1_1_1 = Term(SortedCons, idx1_1_1, acc1_1_1_0.t, acc1_1_1_1.t, 0, t.t);
	term[idx1_1_1] = t1_1_1;
	nf[idx1_1_1] = false;
	frontier[idx1_1_1] = true;
	unsigned int idx1_1 = atomicAdd(&next_free, 1);
    Term t1_1 = Term(Merge, idx1_1, acc1_1_0.t, t1_1_1.t, 0, t.t);
	term[idx1_1] = t1_1;
	nf[idx1_1] = false;
	frontier[idx1_1] = true;
	unsigned int idx1 = atomicAdd(&next_free, 1);
    Term t1 = Term(SortedCons, idx1, acc1_0.t, t1_1.t, 0, t.t);
	term[idx1] = t1;
	nf[idx1] = false;
	frontier[idx1] = true;
	
	Term &acc2_0 = term[term[t.arg1].arg0];
	
	
	Term &acc2_1_0_0 = term[term[t.arg0].arg0];
	Term &acc2_1_0_1 = term[term[t.arg0].arg1];
	unsigned int idx2_1_0 = atomicAdd(&next_free, 1);
    Term t2_1_0 = Term(SortedCons, idx2_1_0, acc2_1_0_0.t, acc2_1_0_1.t, 0, t.t);
	term[idx2_1_0] = t2_1_0;
	nf[idx2_1_0] = false;
	frontier[idx2_1_0] = true;
	Term &acc2_1_1 = term[term[t.arg1].arg1];
	unsigned int idx2_1 = atomicAdd(&next_free, 1);
    Term t2_1 = Term(Merge, idx2_1, t2_1_0.t, acc2_1_1.t, 0, t.t);
	term[idx2_1] = t2_1;
	nf[idx2_1] = false;
	frontier[idx2_1] = true;
	unsigned int idx2 = atomicAdd(&next_free, 1);
    Term t2 = Term(SortedCons, idx2, acc2_0.t, t2_1.t, 0, t.t);
	term[idx2] = t2;
	nf[idx2] = false;
	frontier[idx2] = true;
	t.type = MergeIf;
    t.arg0 = t0.t;
    t.arg1 = t1.t;
    t.arg2 = t2.t;
    frontier[t.t] = true;
  } else if(is_Merge(t) && is_SortedNil(term[t.arg1])) {
    
  Term &acc = term[t.arg0];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else if(is_Merge(t) && is_SortedNil(term[t.arg0])) {
    
  Term &acc = term[t.arg1];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Merge any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Lt(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Lt(t) && is_S(term[t.arg0]) && is_S(term[t.arg1])) {
    
	Term &acc0 = term[term[t.arg0].arg0];
	Term &acc1 = term[term[t.arg1].arg0];
	t.type = Lt;
    t.arg0 = acc0.t;
    t.arg1 = acc1.t;
    frontier[t.t] = true;
  } else if(is_Lt(t) && is_S(term[t.arg0]) && is_Zero(term[t.arg1])) {
    
	t.type = False;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else if(is_Lt(t) && is_Zero(term[t.arg0]) && is_S(term[t.arg1])) {
    
	t.type = True;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else if(is_Lt(t) && is_Zero(term[t.arg0]) && is_Zero(term[t.arg1])) {
    
	t.type = False;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Lt any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Gt(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Gt(t)) {
    
	
	
	Term &acc0_0_0 = term[t.arg0];
	Term &acc0_0_1 = term[t.arg1];
	unsigned int idx0_0 = atomicAdd(&next_free, 1);
    Term t0_0 = Term(Lt, idx0_0, acc0_0_0.t, acc0_0_1.t, 0, t.t);
	term[idx0_0] = t0_0;
	nf[idx0_0] = false;
	frontier[idx0_0] = true;
	unsigned int idx0 = atomicAdd(&next_free, 1);
    Term t0 = Term(Not, idx0, t0_0.t, 0, 0, t.t);
	term[idx0] = t0;
	nf[idx0] = false;
	frontier[idx0] = true;
	
	
	Term &acc1_0_0 = term[t.arg0];
	Term &acc1_0_1 = term[t.arg1];
	unsigned int idx1_0 = atomicAdd(&next_free, 1);
    Term t1_0 = Term(Eq, idx1_0, acc1_0_0.t, acc1_0_1.t, 0, t.t);
	term[idx1_0] = t1_0;
	nf[idx1_0] = false;
	frontier[idx1_0] = true;
	unsigned int idx1 = atomicAdd(&next_free, 1);
    Term t1 = Term(Not, idx1, t1_0.t, 0, 0, t.t);
	term[idx1] = t1;
	nf[idx1] = false;
	frontier[idx1] = true;
	t.type = And;
    t.arg0 = t0.t;
    t.arg1 = t1.t;
    frontier[t.t] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Gt any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Leq(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Leq(t)) {
    
	
	Term &acc0_0 = term[t.arg0];
	Term &acc0_1 = term[t.arg1];
	unsigned int idx0 = atomicAdd(&next_free, 1);
    Term t0 = Term(Eq, idx0, acc0_0.t, acc0_1.t, 0, t.t);
	term[idx0] = t0;
	nf[idx0] = false;
	frontier[idx0] = true;
	
	Term &acc1_0 = term[t.arg0];
	Term &acc1_1 = term[t.arg1];
	unsigned int idx1 = atomicAdd(&next_free, 1);
    Term t1 = Term(Lt, idx1, acc1_0.t, acc1_1.t, 0, t.t);
	term[idx1] = t1;
	nf[idx1] = false;
	frontier[idx1] = true;
	t.type = Or;
    t.arg0 = t0.t;
    t.arg1 = t1.t;
    frontier[t.t] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Leq any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Not(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Not(t) && is_False(term[t.arg0])) {
    
	t.type = True;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else if(is_Not(t) && is_True(term[t.arg0])) {
    
	t.type = False;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Not any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Sort(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Sort(t) && is_Cons(term[t.arg0])) {
    
	
	Term &acc0_0 = term[term[t.arg0].arg0];
	
	unsigned int idx0_1 = atomicAdd(&next_free, 1);
    Term t0_1 = Term(SortedNil, idx0_1, 0, 0, 0, t.t);
	term[idx0_1] = t0_1;
	nf[idx0_1] = false;
	frontier[idx0_1] = true;
	unsigned int idx0 = atomicAdd(&next_free, 1);
    Term t0 = Term(SortedCons, idx0, acc0_0.t, t0_1.t, 0, t.t);
	term[idx0] = t0;
	nf[idx0] = false;
	frontier[idx0] = true;
	
	Term &acc1_0 = term[term[t.arg0].arg1];
	unsigned int idx1 = atomicAdd(&next_free, 1);
    Term t1 = Term(Sort, idx1, acc1_0.t, 0, 0, t.t);
	term[idx1] = t1;
	nf[idx1] = false;
	frontier[idx1] = true;
	t.type = Merge;
    t.arg0 = t0.t;
    t.arg1 = t1.t;
    frontier[t.t] = true;
  } else if(is_Sort(t) && is_Nil(term[t.arg0])) {
    
	t.type = SortedNil;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Sort any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Or(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Or(t) && is_False(term[t.arg0])) {
    
  Term &acc = term[t.arg1];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else if(is_Or(t) && is_True(term[t.arg0])) {
    
	t.type = True;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Or any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_MergeIf(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_MergeIf(t) && is_False(term[t.arg0])) {
    
  Term &acc = term[t.arg2];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else if(is_MergeIf(t) && is_True(term[t.arg0])) {
    
  Term &acc = term[t.arg1];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else {
  	
    printf("Could not rewrite term %u with type MergeIf any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Geq(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Geq(t)) {
    
	
	Term &acc0_0 = term[t.arg0];
	Term &acc0_1 = term[t.arg1];
	unsigned int idx0 = atomicAdd(&next_free, 1);
    Term t0 = Term(Lt, idx0, acc0_0.t, acc0_1.t, 0, t.t);
	term[idx0] = t0;
	nf[idx0] = false;
	frontier[idx0] = true;
	t.type = Not;
    t.arg0 = t0.t;
    frontier[t.t] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Geq any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_And(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_And(t) && is_False(term[t.arg0])) {
    
	t.type = False;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else if(is_And(t) && is_True(term[t.arg0])) {
    
  Term &acc = term[t.arg1];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else {
  	
    printf("Could not rewrite term %u with type And any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Head(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Head(t) && is_Cons(term[t.arg0])) {
    
  Term &acc = term[term[t.arg0].arg0];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else if(is_Head(t) && is_Nil(term[t.arg0])) {
    
	t.type = NaN;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Head any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Append(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Append(t) && is_Cons(term[t.arg0])) {
    
	Term &acc0 = term[term[t.arg0].arg0];
	
	Term &acc1_0 = term[term[t.arg0].arg1];
	Term &acc1_1 = term[t.arg1];
	unsigned int idx1 = atomicAdd(&next_free, 1);
    Term t1 = Term(Append, idx1, acc1_0.t, acc1_1.t, 0, t.t);
	term[idx1] = t1;
	nf[idx1] = false;
	frontier[idx1] = true;
	t.type = Cons;
    t.arg0 = acc0.t;
    t.arg1 = t1.t;
    frontier[t.t] = true;
  } else if(is_Append(t) && is_Nil(term[t.arg1])) {
    
  Term &acc = term[t.arg0];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else if(is_Append(t) && is_Nil(term[t.arg0])) {
    
  Term &acc = term[t.arg1];
  t.type = acc.type;
  
  t.arg0 = acc.arg0;
  t.arg1 = acc.arg1;
  t.arg2 = acc.arg2;
  nf[t.t] = true;
  frontier[t.t] = false;
  frontier[t.parent] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Append any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}

inline void __device__ Rewrite_Eq(Term &t, Term *term, bool *frontier, bool *nf) {
  if(is_Eq(t) && is_Zero(term[t.arg0]) && is_Zero(term[t.arg1])) {
    
	t.type = True;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else if(is_Eq(t) && is_S(term[t.arg0]) && is_S(term[t.arg1])) {
    
	Term &acc0 = term[term[t.arg0].arg0];
	Term &acc1 = term[term[t.arg1].arg0];
	t.type = Eq;
    t.arg0 = acc0.t;
    t.arg1 = acc1.t;
    frontier[t.t] = true;
  } else if(is_Eq(t) && is_S(term[t.arg0]) && is_Zero(term[t.arg1])) {
    
	t.type = False;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else if(is_Eq(t) && is_Zero(term[t.arg0]) && is_S(term[t.arg1])) {
    
	t.type = False;
	nf[t.t] = true;
	frontier[t.parent] = true;
    frontier[t.t] = false;
  } else {
  	
    printf("Could not rewrite term %u with type Eq any further, it is now in NF.\n", t.t);
	nf[t.t] = true;
	frontier[t.t] = false;
	frontier[t.parent] = true;
  } 
}




__global__ void Rewrite(Term *term, bool *done, bool *frontier, bool *nf) {
    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    bool do_work = frontier[id] && !nf[id];
    __syncthreads();
    if(do_work) {
        Term &my_term = term[id];

        switch(my_term.type) {
		case Merge:
		  Rewrite_Merge(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Lt:
		  Rewrite_Lt(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Gt:
		  Rewrite_Gt(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Leq:
		  Rewrite_Leq(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Not:
		  Rewrite_Not(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Sort:
		  Rewrite_Sort(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Or:
		  Rewrite_Or(my_term, term, frontier, nf);
		  *done = false;
		break;
		case MergeIf:
		  Rewrite_MergeIf(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Geq:
		  Rewrite_Geq(my_term, term, frontier, nf);
		  *done = false;
		break;
		case And:
		  Rewrite_And(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Head:
		  Rewrite_Head(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Append:
		  Rewrite_Append(my_term, term, frontier, nf);
		  *done = false;
		break;
		case Eq:
		  Rewrite_Eq(my_term, term, frontier, nf);
		  *done = false;
		break;
		default:
		   nf[id] = true;
		   frontier[id] = false;
		   frontier[my_term.parent] = true;
		   *done = false;
		  break;
        }
    }
    else if(nf[id]) frontier[id] = false;
}