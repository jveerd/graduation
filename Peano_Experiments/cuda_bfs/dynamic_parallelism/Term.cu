#include "Term.cuh"

__global__ void rewrite_Root() {
    term[root_idx].Rewrite();
}

__global__ void p_Rewrite(unsigned int *in) {
    term[in[threadIdx.x]].Rewrite();
}

void __device__ Rewrite_Succ(Term& succ) {
    if(!in_normalform[succ.lhs])
        term[succ.lhs].Rewrite();

    in_normalform[succ.t] = true;
}

void __device__ Rewrite_Add(Term &add) {
    if(!in_normalform[add.lhs] && !in_normalform[add.rhs]) {
        //debug("ADD: neither lhs or rhs are normal form, applying parallel rewrite\n");
        unsigned int *children = (unsigned int *) malloc(2*sizeof(unsigned int));
        children[0] = add.lhs;
        children[1] = add.rhs;
        p_Rewrite<<<1,2>>>(children);
        cudaError_t e = cudaDeviceSynchronize();
        if(e != cudaSuccess) {
            printf("Children of add %u encountered an error: '%s'\n", add.t, cudaGetErrorString(e));
        }
        free(children);
    }
    if(!in_normalform[add.lhs]) { //leftmost innermost
        //debug("ADD: Addition of non-normal lhs, rewriting lhs");
        term[add.lhs].Rewrite();
    }
    if(!in_normalform[add.rhs]) {
        //debug("ADD: Addition of non-normal rhs, rewriting rhs");
        term[add.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply addition rewrite rules
    if(term[add.lhs].type == Zero) {
        //debug("ADD: lhs is 0, result := rhs");
        in_normalform[add.t] = true;
        add.type = term[add.rhs].type;
        add.lhs = term[add.rhs].lhs;
        add.rhs = term[add.rhs].rhs;
        term[add.t] = add;
    } else { //term[add.lhs].type is Succ
        //debug("ADD: rewriting S(n) + rhs to S(n + rhs)");

        Term &new_succ = add;
        unsigned int temp = atomicAdd(&next_free, 1);
        Term new_add = Term(Add, temp, term[add.lhs].lhs, add.rhs);
        term[temp] = new_add;

        new_succ.type = Succ;
        new_succ.lhs = new_add.t;

        new_add.Rewrite();

        in_normalform[new_add.t] = true;
        in_normalform[new_succ.t] = true;
        term[new_add.t] = new_add;
    }
}

void __device__ Rewrite_Mult(Term &mult) {
    if(!in_normalform[mult.lhs] && !in_normalform[mult.rhs]) {
//        printf("MULT: neither lhs or rhs are normal form, applying parallel rewrite\n");
        unsigned int *children = (unsigned int *) malloc(2*sizeof(unsigned int));
        children[0] = mult.lhs;
        children[1] = mult.rhs;
        p_Rewrite<<<1,2>>>(children);
        cudaError_t e = cudaDeviceSynchronize();
        if(e != cudaSuccess) {
            printf("Children of mult %u encountered an error: '%s'\n", mult.t, cudaGetErrorString(e));
        }
        free(children);
    }
    if(!in_normalform[mult.lhs]) { //leftmost innermost
        //debug("MULT: Multiplication of non-normal lhs, rewriting lhs " + term[mult.lhs].toString());
        term[mult.lhs].Rewrite();
    }
    if(!in_normalform[mult.rhs]) {
        //debug("MULT: Multiplication of non-normal rhs, rewriting rhs" + term[mult.rhs].toString());
        term[mult.rhs].Rewrite();
    }

    //both lhs and rhs are in normal form, apply multiplication rewrite rules
    if(term[mult.lhs].type == Zero) {
        //debug("MULT: Multiplication with 0, result := 0");
        term[mult.t].type = Zero;
        in_normalform[mult.t] = true;
    } else { //term[mult.lhs].type is Succ
        //debug("MULT: rewriting S(n) * rhs to (n * rhs) + rhs");
        Term &new_add = mult;
        Term &new_mult = term[mult.lhs];
        Term &rhs = term[mult.rhs];

        new_add.type = Add;
        new_mult.type = Mult;

        new_add.lhs = new_mult.t;
        unsigned int temp = atomicAdd(&next_free, 1);
        term[temp] = Term(rhs.type, temp, rhs.lhs, rhs.rhs); //TODO: requires deep clone to avoid sharing
        new_mult.rhs = temp;
        new_mult.Rewrite();
        new_add.Rewrite();
    }
}

void __device__ Term::Rewrite() {
    switch(this->type) {
        case Zero:
            break;
        case Succ:
            Rewrite_Succ(*this);
            break;
        case Add:
            Rewrite_Add(*this);
            break;
        case Mult:
            Rewrite_Mult(*this);
            break;
    }
}

//Constructor requires that terms are built from the leaves in order to deduce whether they should be normal form
__device__ Term::Term(Type type, unsigned int t, unsigned int lhs, unsigned int rhs)
        : type(type), t(t), lhs(lhs), rhs(rhs) {
    bool nf = (type == Zero) || ((type == Succ) && in_normalform[lhs]);
    in_normalform[t] = nf;
}