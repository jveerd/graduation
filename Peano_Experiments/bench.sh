#!/usr/bin/env bash
benchmark_cpu()
{
    for term_size in 0 1
    do
        for depth in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
        do
            echo "running CPU version with big_$term_size term and depth $depth"
            echo "***CPU***TERM=$term_size DEPTH=$depth" >> benchmark.txt
            ./cpu/build/peano_cpu $depth $term_size | tee -a benchmark.txt
        done
    done

    for term_size in 0 1
    do
        for depth in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
        do
            echo "running CPU bottom up version with big_$term_size term and depth $depth"
            echo "***CPU_BU***TERM=$term_size DEPTH=$depth" >> benchmark.txt
            ./cpu/bottom_up/build/peano_cpu $depth $term_size | tee -a benchmark.txt
        done
    done
}

benchmark_gpu()
{
    for term_size in 0 1
    do
        for depth in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
        do
	        echo "running GPU dynamic parallelism version with big_$term_size term and depth $depth"
	        echo "***GPU_DP***TERM=$term_size DEPTH=$depth" >> benchmark.txt
	        ./cuda_bfs/dynamic_parallelism/build/peano_gpu $depth $term_size | tee -a benchmark.txt
        done
    done

    for term_size in 0 1
    do
        for depth in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
        do
	        echo "running GPU bottom up version with big_$term_size term and depth $depth"
	        echo "***GPU_BU***TERM=$term_size DEPTH=$depth" >> benchmark.txt
	        ./cuda_bfs/bottom_up/build/peano_gpu $depth $term_size | tee -a benchmark.txt
        done
    done

    for term_size in 0 1
    do
        for depth in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
        do
	        echo "running GPU version with big_$term_size term and depth $depth"
	        echo "***GPU***TERM=$term_size DEPTH=$depth" >> benchmark.txt
	        ./cuda_bfs/build/peano_gpu $depth $term_size | tee -a benchmark.txt
        done
    done
}

if [ -f benchmark.txt ]; then
    echo "benchmark.txt exists, exiting"
else
    touch benchmark.txt
    echo "starting cpu benchmark"
    benchmark_cpu
    echo "done with cpu benchmark; starting gpu benchmark"
    benchmark_gpu
    echo "done with gpu benchmark"
fi
