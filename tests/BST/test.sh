#!/usr/bin/env bash
if [ -d build ]; then rm -rf build; fi

keep=false
cpp=false

d=`date +"%Y-%m-%d_%T"`
res=results_$d

function do_test {
	if [ -d "$1" ]; then
		if [ ! -d "$1/generated" ]; then mkdir "$1/generated"; fi
		if [ ! -d "$1/build" ]; then mkdir "$1/build"; fi
	    touch $res/$1.frontier
		if [ "$cpp" = false ]; then python ../../compiler/parse.py --out $(pwd)/"$1/generated" --file $(pwd)/"$1/$1.trs";
		else
			python ../../compiler/parse.py --cpp --out $(pwd)/"$1/generated" --file $(pwd)/"$1/$1.trs"
		fi	
		mkdir build
		cd build
		if [ "$cpp" = false ]; then cmake $(pwd)/../"$1"/. -DVERBOSE=ON >/dev/null
		else
			cmake $(pwd)/../"$1"/cpu/. >/dev/null
		fi
		make >/dev/null
		if [ "$cpp" = false ]; then ./test_gpu >> ../$res/results.txt 2>../$res/$1.frontier
		else
			./test_cpu >> ../$res/results.txt 2>../$res/$1.cpu
		fi

		if [ $? -ne 0 ]; then
			echo "$1 : [FAIL]"
			if [ "$keep" = true ]; then cd ../ && mv build "$1";
			else
				cd ../ && rm -rf build
				rm -rf "$1/generated"
			fi
		else
			echo "$1 : [PASS]"
			cd ../
			if [ "$keep" = false ]; then
				rm -rf build
				rm -rf "$1/generated"
				rm -rf "$1/build"
			else
				if [ "$cpp" = false ]; then
					mv build/test_gpu "$1"/build
				else
					mv build/test_cpu "$1"/build
				fi
				rm -rf build
			fi
		fi
	fi
}

while getopts kc opt
do
  case $opt in
    k)
		keep=true
		;;
	c)
		cpp=true
		;;
    \?)
		echo "Invalid option: -$OPTARG" >&2
		exit
      ;;
  esac
done

shift $(($OPTIND - 1 ))

if [ "$1" != "" ]; then
	mkdir $res
	touch $res/results.txt
	do_test ${1%/}
else
	mkdir $res
	touch $res/results.txt
	for i in 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25; do
		f=BST_$i
		echo $f >> $res/results.txt
		do_test $f
		echo '=========' >> $res/results.txt
	done
fi
