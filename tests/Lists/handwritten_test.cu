#include <iostream>
#include "generated/term.cuh"
#include "bfs.cuh"

#define DEPTH 18

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess)
    {
	  fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	  if (abort) exit(code);
    }
}

//bool verbose;
Term *term;
__device__ unsigned int next_free;
__device__ unsigned int root_idx;

__global__ void init(unsigned int d_next_free, unsigned int d_root_idx) {
  next_free = d_next_free;
  root_idx = d_root_idx;
}

unsigned int make_Head_Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Nil, 0, 0, 0, 0);
  nf_ptr[0] = true;

  term_ptr[1] = Term(Head, 1, 0, 0, 2);
  
  init<<<1,1>>>(2, 1);
  return 1;
}

unsigned int make_Head_Cons1Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 0);
  nf_ptr[0] = true;

  term_ptr[1] = Term(S, 1, 0, 0, 3);
  nf_ptr[1] = true;
  
  term_ptr[2] = Term(Nil, 2, 0, 0, 3);
  nf_ptr[2] = true;

  term_ptr[3] = Term(Cons, 3, 1, 2, 4);
  nf_ptr[3] = true;

  term_ptr[4] = Term(Head, 4, 3, 0, 5);
  nf_ptr[4] = false;
  
  init<<<1,1>>>(5, 4);
  return 4;
}

unsigned int make_Append_Nil_Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Nil, 0, 0, 0, 1);
  nf_ptr[0] = true;

  term_ptr[1] = Term(Append, 1, 0, 0, 2);
  nf_ptr[1] = false;

  init<<<1,1>>>(2,1);
  return 1;
}

unsigned int make_Append_Cons0Nil_Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 2);
  nf_ptr[0] = true;
  
  term_ptr[1] = Term(Nil, 1, 0, 0, 2);
  nf_ptr[1] = true;

  term_ptr[2] = Term(Cons, 2, 0, 1, 3);
  nf_ptr[2] = true;

  term_ptr[3] = Term(Append, 3, 2, 1, 4);
  nf_ptr[3] = false;

  init<<<1,1>>>(4,3);
  return 3;
}

unsigned int make_Append_Nil_Cons0Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 2);
  nf_ptr[0] = true;
  
  term_ptr[1] = Term(Nil, 1, 0, 0, 2);
  nf_ptr[1] = true;

  term_ptr[2] = Term(Cons, 2, 0, 1, 3);
  nf_ptr[2] = true;

  term_ptr[3] = Term(Append, 3, 1, 2, 4);
  nf_ptr[3] = false;

  init<<<1,1>>>(4,3);
  return 3;
}

unsigned int make_Append_Cons0Nil_Cons0Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 2);
  nf_ptr[0] = true;
  
  term_ptr[1] = Term(Nil, 1, 0, 0, 2);
  nf_ptr[1] = true;

  term_ptr[2] = Term(Cons, 2, 0, 1, 3);
  nf_ptr[2] = true;

  term_ptr[3] = Term(Append, 3, 2, 2, 4);
  nf_ptr[3] = false;

  init<<<1,1>>>(4,3);
  return 3;
}

__global__ void Report_Result_NaN(Term *term, bool *correct) {
  Term temp = term[root_idx];
#ifdef VERBOSE
  if(temp.type != NaN) printf("Root is not NaN: %d\n", temp.type);
#endif
  *correct = temp.type == NaN;
}

__global__ void Report_Result_1(Term *term, bool *correct) {
  Term temp = term[root_idx];

  *correct = temp.type == S && term[temp.arg0].type == Zero;
}

__global__ void Report_Result_Nil(Term *term, bool *correct) {
  Term temp = term[root_idx];

  *correct = temp.type == Nil;
}

__global__ void Report_Cons_0_Nil(Term *term, bool *correct) {
  Term temp = term[root_idx];

  *correct = temp.type == Cons && term[temp.arg0].type == Zero && term[temp.arg1].type == Nil;
}

__global__ void Report_Cons_0_Cons_0_Nil(Term *term, bool *correct) {
  Term temp = term[root_idx];

  *correct = temp.type == Cons && term[temp.arg0].type == Zero && term[temp.arg1].type == Cons &&
	term[term[temp.arg1].arg0].type == Zero && term[term[temp.arg1].arg1].type == Nil;
}

__global__ void update(unsigned int *current_terms) {
  *current_terms = next_free + 1;
}

int run_test(unsigned int num_terms, Term* term, bool *nf_ptr, unsigned int (*ctr)(Term* t, bool* n), void (*check) (Term *p, bool *c)) {

  int device;
  gpuErrchk(cudaGetDevice(&device));
  cudaDeviceProp devProp;
  cudaGetDeviceProperties(&devProp,device);
  // std::cout << "Start of new test run\n";
  unsigned int root = ctr(term, nf_ptr);

  cudaMemPrefetchAsync(term, num_terms*sizeof(Term), device, NULL);
  
  unsigned int *current_terms;
  cudaMallocManaged((void**)&current_terms, sizeof(unsigned int));
  *current_terms = root;



  // Get the kernel configuration
  int numBlocks = (int)ceil((double)min(num_terms, *current_terms)/(double)devProp.maxThreadsPerBlock);
  int numThreads = min(min(devProp.maxThreadsPerBlock, (int)num_terms), (int)*current_terms)+1;

  // Find first frontier of rewritable terms
  bool *done;
  cudaMallocManaged((void**)&done, sizeof(bool));
  *done = false;
  bool *frontier;
  gpuErrchk(cudaMallocManaged((void**) &frontier, num_terms * sizeof(bool)));

  for(int i = 0; i < num_terms; i++) {
	frontier[i] = false;
  }
  frontier[root] = true;

  int iter = 0;
  float ms_time = 0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  float terms_handled;
  while(!(*done)) {
	terms_handled += numBlocks * numThreads;
	iter++;
	*done = true;

	cudaEventRecord(start);

	bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;
  }

  *done = false;
  auto rewrites = 0;
  while(!(*done)) {
	terms_handled += numBlocks * numThreads;
	iter++;
	rewrites++;
	*done = true;

	cudaEventRecord(start);

	Rewrite<<<numBlocks, numThreads>>>(term, done, frontier, nf_ptr);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	// std::cout << "End of rewrite step " << rewrites << std::endl;
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;

	update<<<1,1>>>(current_terms);
	gpuErrchk(cudaDeviceSynchronize());
	gpuErrchk(cudaPeekAtLastError());
	numBlocks = (int)ceil((double)min(num_terms, *current_terms)/(double)devProp.maxThreadsPerBlock);
	numThreads = min(min(devProp.maxThreadsPerBlock, (int)num_terms), (int)*current_terms);

	if(!(*done)) {
	  while(!(*done)) {
		terms_handled += numBlocks * numThreads;
		iter++;
		*done = true;

		cudaEventRecord(start);

		bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
		gpuErrchk(cudaPeekAtLastError());

		cudaEventRecord(stop);
		cudaEventSynchronize(stop);
		float temp = 0;
		cudaEventElapsedTime(&temp, start, stop);
		ms_time += temp;
	  }
	  *done = false;
	}
  }

#ifdef VERBOSE
  if(terms_handled < 0) {
	std::cout << "negative amount of terms handled... \n\t#blocks = " << numBlocks << "\n\t#threads = " << numThreads << "\n\titerations = " << iter << std::endl;
  }
  float tps = terms_handled / (ms_time / 1000);

  std::cout.precision(0);
  std::cout << std::fixed << "BFSing and rewriting took " << ms_time << " ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific << terms_handled << "\n T/S: " << tps << std::endl;
#endif

  bool *correct;
  cudaMallocManaged((void**)&correct, sizeof(bool));
  *correct = true;

  check<<<1,1>>>(term, correct);

  gpuErrchk(cudaFree(frontier));
  gpuErrchk(cudaFree(done));

  return !*correct;
}

int main(int argc, const char* argv[])
{
  size_t free, total;

  gpuErrchk(cudaMemGetInfo(&free, &total));
  std::cout << "Free memory: " << free << " bytes." << std::endl;
  std::cout << "Total memory: " << total << " bytes." << std::endl;

  size_t term_size = sizeof(Term);
  size_t frontier_size = sizeof(bool);
  size_t nf_size = sizeof(bool);

  size_t total_size = term_size + frontier_size + nf_size;

  size_t num_terms = free / total_size;
  std::cout << "Max number of terms: " << num_terms << std::endl;

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));

  bool *nf_ptr;
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  
  auto result = run_test(num_terms, term, nf_ptr, &make_Head_Nil, &Report_Result_NaN);
  if(result) {
	std::cout << "Result was not NaN...\n";
	return result;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  result = run_test(num_terms, term, nf_ptr, &make_Head_Cons1Nil, &Report_Result_1);
  if(result) {
  	std::cout << "Result was not 1...\n";
  	return result;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  result = run_test(num_terms, term, nf_ptr, &make_Append_Nil_Nil, &Report_Result_Nil);
  if(result) {
  	std::cout << "Result was not Nil...\n";
  	return result;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  result = run_test(num_terms, term, nf_ptr, &make_Append_Cons0Nil_Nil, &Report_Cons_0_Nil);
  if(result) {
  	std::cout << "Result of Append(Cons(0, Nil), Nil) was not Cons(0, Nil)...\n";
  	return result;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  result = run_test(num_terms, term, nf_ptr, &make_Append_Nil_Cons0Nil, &Report_Cons_0_Nil);
  if(result) {
  	std::cout << "Result of Append(Nil, Cons(0, Nil)) was not Cons(0, Nil)...\n";
  	return result;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  result = run_test(num_terms, term, nf_ptr, &make_Append_Cons0Nil_Cons0Nil, &Report_Cons_0_Cons_0_Nil);
  if(result) {
  	std::cout << "Result of Append(Cons(0, Nil), Cons(0, Nil)) was not Cons(0, Cons(0, Nil))...\n";
  	return result;
  }
    
  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));
  gpuErrchk(cudaDeviceReset());
  return 0;
}