#include <iostream>
#include <chrono>
#include "Term.cuh"

#define DEPTH 20

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

//bool verbose;
__device__ Term *term;
__device__ bool *in_normalform;
__device__ unsigned int next_free;
__device__ unsigned int root_idx;

__global__ void init(Term *term_ptr, unsigned int d_next_free, unsigned int d_root_idx, bool *d_nf) {
    term = term_ptr;
    in_normalform = d_nf;
    next_free = d_next_free;
    root_idx = d_root_idx;
}

unsigned int make_big_term(Term *d_ptr, bool *d_nf, int depth, Type bottom, Type rest) {
    Term *h_term;
    bool *h_nf;
    size_t terms = 1<<(depth+1);
    size_t bytes = terms * sizeof(Term);

    gpuErrchk(cudaMallocHost((void**) &h_term, bytes));

    gpuErrchk(cudaMallocHost((void**) &h_nf, terms*sizeof(bool)));

    h_term[0] = Term(Zero, 0, 0, 0);
    h_nf[0] = true;

    for(unsigned int i = 1; i <= (1<<depth); i++) {
        h_term[i] = Term(bottom, i, 0, 0);
        h_nf[i] = (bottom == Succ) || (bottom == Zero);
    }

    unsigned int start_index = (1<<depth);

    for(int d = depth-1; d >= 0; d--) {
        unsigned int end_index = start_index + (1<<d);
        unsigned int prev_lvl = start_index - (1<<(d+1));
        for(unsigned int i = start_index + 1; i <= end_index; i++) {
            unsigned int j = i - start_index;
            h_term[i] = Term(rest, i, prev_lvl + j*2-1, prev_lvl + j*2);
            h_nf[i] = false;
        }
        start_index = end_index;
    }
    gpuErrchk(cudaMemcpy((void*) d_ptr, (void*) h_term, bytes, cudaMemcpyHostToDevice));

    gpuErrchk(cudaMemcpy((void*) d_nf, (void*) h_nf, terms*sizeof(bool), cudaMemcpyHostToDevice));

    gpuErrchk(cudaFreeHost(h_term));
    gpuErrchk(cudaFreeHost(h_nf));

    init<<<1,1>>>(d_ptr, start_index + 1, start_index, d_nf);
    return start_index;
}

unsigned int make_big0(Term *d_ptr, bool *d_nf, int depth) {
    return make_big_term(d_ptr, d_nf, depth, Add, Add);
}

unsigned int make_big1(Term *d_ptr, bool *d_nf, int depth) {
    return make_big_term(d_ptr, d_nf, depth, Succ, Mult);
}

__global__ void Report_Result_0() {
    int succs = 0;
    Term temp = term[root_idx];
    if(temp.type != Zero) printf("root is not zero...%d\n", temp.type);
    while(temp.type != Zero) {
        succs++;
        temp = term[temp.lhs];
    }
    printf("Result: %d\n", succs);
}

__global__ void Report_Result_1() {
    int succs = 0;
    Term temp = term[root_idx];
    while(temp.type != Zero) {
        succs++;
        temp = term[temp.lhs];
    }
    printf("Result: %d\n", succs);
}

int main(int argc, const char* argv[])
{
    using milli = std::chrono::milliseconds;
    int depth = DEPTH;
    int big = 1;
    if(argc > 2) {
        big = atoi(argv[2]);
        depth = atoi(argv[1]);
    } else if(argc > 1) {
        depth = atoi(argv[1]);
        std::cout << "No term specified, continuing with big1 with depth = " << depth << std::endl;
    } else {
        std::cout << "No depth provided, continuing with big1 with depth = " << DEPTH << std::endl;
    }

    cudaDeviceReset();

    size_t req = 1<<(depth+2);
    Term *d_ptr;
    cudaError_t err = cudaMalloc((void**)&d_ptr, req*sizeof(Term));
    if(err != cudaSuccess) {
        std::cout << "Could not cudaMalloc for term array: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    bool *d_nf_ptr;
    err = cudaMalloc((void**)&d_nf_ptr, req*sizeof(bool));
    if(err != cudaSuccess) {
        std::cout << "Could not cudaMalloc for nf array : " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    (big == 0) ? make_big0(d_ptr, d_nf_ptr, depth) : make_big1(d_ptr, d_nf_ptr, depth);

    cudaDeviceSynchronize();
    err = cudaGetLastError();
    if(err != cudaSuccess) {
        std::cout << "An error occurred while initializing the term: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    auto start = std::chrono::high_resolution_clock::now();

    unsigned int offset = 1;
    for(int d = depth; d >= 0; d--) {
        unsigned int kernels = (1<<d);
        int numBlocks = (int)ceil((double)kernels/(double)1024);
        int numThreads = min(1024, (int)kernels);
        Rewrite<<<numBlocks, numThreads>>>(offset);
        cudaDeviceSynchronize();
        err = cudaGetLastError();
        if(err != cudaSuccess) {
            std::cout << "An error occurred on level " << d << " of the term: " << cudaGetErrorString(err) << std::endl;
            exit(1);
        }
        offset += kernels;
    }

    auto finish = std::chrono::high_resolution_clock::now();

    std::cout << "Done rewriting after "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

    if(big != 0) {
        Report_Result_1<<<1,1>>>();
    } else {
        Report_Result_0<<<1,1>>>();
    }

    cudaDeviceSynchronize();
    err = cudaGetLastError();
    if(err != cudaSuccess) {
        std::cout << "An error occurred while printing the result: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    cudaFree(d_ptr);
    cudaFree(d_nf_ptr);
    cudaDeviceReset();
    return 0;
}
