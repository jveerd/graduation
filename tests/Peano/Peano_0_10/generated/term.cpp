#include "term.h"

__inline__ void Rewrite_Plus(Term &t, std::vector<Term> term, std::vector<bool> nf, Indices &indices) {
  if(is_S(term[t.arg0])) {
    indices.incRef(t.arg1, 1);indices.incRef(term[t.arg0].arg0, 1);
	
    
    unsigned int idx0 = indices.getRef();
	unsigned int acc0_0 = term[t.arg0].arg0;
	unsigned int acc0_1 = t.arg1;
    Term t0 = Term(Plus, idx0, acc0_0,  acc0_1);
	term[idx0] = t0;
    
    nf[idx0] = false;
	t.head_symbol = S;
    indices.decRef(t.arg0);
    t.arg0 = idx0;
    indices.decRef(t.arg1);
    t.arg1 = 0;
    nf[t.index] = false;
    term[t.index] = t;
    
  } else if(is_Zero(term[t.arg0])) {
    indices.incRef(t.arg1, 1);
	
    
  Term &acc = term[t.arg1];
  t.head_symbol = acc.head_symbol;
  
  indices.decRef(t.arg0);
  t.arg0 = acc.arg0;
  indices.decRef(t.arg1);
  t.arg1 = acc.arg1;
  nf[t.index] = true;
  } else {
  	
    printf("Could not rewrite term %u with type Plus any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
  } 
}

__inline__ void Rewrite_InputMult(Term &t, std::vector<Term> term, std::vector<bool> nf, Indices &indices) {
  if(is_S(term[t.arg0])) {
    indices.incRef(t.arg1, 2);indices.incRef(term[t.arg0].arg0, 2);
	
    
    unsigned int idx0 = indices.getRef();
	unsigned int acc0_0 = term[t.arg0].arg0;
	unsigned int acc0_1 = t.arg1;
    Term t0 = Term(InputMult, idx0, acc0_0,  acc0_1);
	term[idx0] = t0;
    
    nf[idx0] = false;
    unsigned int idx1 = indices.getRef();
	unsigned int acc1_0 = term[t.arg0].arg0;
	unsigned int acc1_1 = t.arg1;
    Term t1 = Term(InputMult, idx1, acc1_0,  acc1_1);
	term[idx1] = t1;
    
    nf[idx1] = false;
	t.head_symbol = Mult;
    indices.decRef(t.arg0);
    t.arg0 = idx0;
    indices.decRef(t.arg1);
    t.arg1 = idx1;
    nf[t.index] = false;
    term[t.index] = t;
    
  } else if(is_Zero(term[t.arg0])) {
    indices.incRef(t.arg1, 2);
	
    
    unsigned int acc0 = t.arg1;
    unsigned int acc1 = t.arg1;
	t.head_symbol = Mult;
    indices.decRef(t.arg0);
    t.arg0 = acc0;
    indices.decRef(t.arg1);
    t.arg1 = acc1;
    nf[t.index] = false;
    term[t.index] = t;
    
  } else {
  	
    printf("Could not rewrite term %u with type InputMult any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
  } 
}

__inline__ void Rewrite_Mult(Term &t, std::vector<Term> term, std::vector<bool> nf, Indices &indices) {
  if(is_S(term[t.arg0])) {
    indices.incRef(t.arg1, 2);indices.incRef(term[t.arg0].arg0, 1);
	
    
    unsigned int acc0 = t.arg1;
    unsigned int idx1 = indices.getRef();
	unsigned int acc1_0 = term[t.arg0].arg0;
	unsigned int acc1_1 = t.arg1;
    Term t1 = Term(Mult, idx1, acc1_0,  acc1_1);
	term[idx1] = t1;
    
    nf[idx1] = false;
	t.head_symbol = Plus;
    indices.decRef(t.arg0);
    t.arg0 = acc0;
    indices.decRef(t.arg1);
    t.arg1 = idx1;
    nf[t.index] = false;
    term[t.index] = t;
    
  } else if(is_Zero(term[t.arg0])) {
    
	
    
	t.head_symbol = Zero;
    indices.decRef(t.arg0);
    t.arg0 = 0;
    indices.decRef(t.arg1);
    t.arg1 = 0;
	nf[t.index] = true;
    term[t.index] = t;
    
  } else {
  	
    printf("Could not rewrite term %u with type Mult any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
  } 
}

__inline__ void Rewrite_InputPlus(Term &t, std::vector<Term> term, std::vector<bool> nf, Indices &indices) {
  if(is_S(term[t.arg0])) {
    indices.incRef(t.arg1, 2);indices.incRef(term[t.arg0].arg0, 2);
	
    
    unsigned int idx0 = indices.getRef();
	unsigned int acc0_0 = term[t.arg0].arg0;
	unsigned int acc0_1 = t.arg1;
    Term t0 = Term(InputPlus, idx0, acc0_0,  acc0_1);
	term[idx0] = t0;
    
    nf[idx0] = false;
    unsigned int idx1 = indices.getRef();
	unsigned int acc1_0 = term[t.arg0].arg0;
	unsigned int acc1_1 = t.arg1;
    Term t1 = Term(InputPlus, idx1, acc1_0,  acc1_1);
	term[idx1] = t1;
    
    nf[idx1] = false;
	t.head_symbol = Plus;
    indices.decRef(t.arg0);
    t.arg0 = idx0;
    indices.decRef(t.arg1);
    t.arg1 = idx1;
    nf[t.index] = false;
    term[t.index] = t;
    
  } else if(is_Zero(term[t.arg0])) {
    indices.incRef(t.arg1, 2);
	
    
    unsigned int acc0 = t.arg1;
    unsigned int acc1 = t.arg1;
	t.head_symbol = Plus;
    indices.decRef(t.arg0);
    t.arg0 = acc0;
    indices.decRef(t.arg1);
    t.arg1 = acc1;
    nf[t.index] = false;
    term[t.index] = t;
    
  } else {
  	
    printf("Could not rewrite term %u with type InputPlus any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
  } 
}


unsigned int Rewrite(Term &t, std::vector<Term> term, std::vector<bool> nf, Indices &indices) {
  std::cout << "Size: " << term.size() << " " << nf.size() << " " << indices.refs.size() << std::endl;
  auto rewrites = 0;
  if(t.arg0 > 0 && !nf[t.arg0]) {
	rewrites += Rewrite(term[t.arg0], term, nf, indices);
  }
  if(t.arg1 > 0 && !nf[t.arg1]) {
	rewrites += Rewrite(term[t.arg1], term, nf, indices);
  }
  rewrites++;
  switch(t.head_symbol) {
	case Plus:
	  Rewrite_Plus(t, term, nf, indices);
	  break;
	case InputMult:
	  Rewrite_InputMult(t, term, nf, indices);
	  break;
	case Mult:
	  Rewrite_Mult(t, term, nf, indices);
	  break;
	case InputPlus:
	  Rewrite_InputPlus(t, term, nf, indices);
	  break;
	default:
	  nf[t.index] = true;
	break;
  }
  if(!nf[t.index]) {
	rewrites += Rewrite(t, term, nf, indices);
  }
  return rewrites;
}