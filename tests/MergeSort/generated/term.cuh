#ifndef PEANO_EXPERIMENTS_TERM_CUH
#define PEANO_EXPERIMENTS_TERM_CUH

#include <string>

enum Type { Zero, S, NaN, Head, True, False, Not, Or, And, Eq, Lt, Gt, Leq, Geq, Nil, Cons, Append, Sort, SortedNil, SortedCons, Merge, MergeIf, NOT_INITIALIZED };

__host__ __device__ inline const char* Type2String(Type t) {
  switch(t) {
	case Zero: return "Zero";
	case S: return "S";
	case NaN: return "NaN";
	case Head: return "Head";
	case True: return "True";
	case False: return "False";
	case Not: return "Not";
	case Or: return "Or";
	case And: return "And";
	case Eq: return "Eq";
	case Lt: return "Lt";
	case Gt: return "Gt";
	case Leq: return "Leq";
	case Geq: return "Geq";
	case Nil: return "Nil";
	case Cons: return "Cons";
	case Append: return "Append";
	case Sort: return "Sort";
	case SortedNil: return "SortedNil";
	case SortedCons: return "SortedCons";
	case Merge: return "Merge";
	case MergeIf: return "MergeIf";
	 default: return "NOT_INITIALIZED";
  }
}

struct Term {
    Type type;
    unsigned int t = 0;
	unsigned int arg0; 
	unsigned int arg1; 
	unsigned int arg2; 
    unsigned int parent;
    Term() { type = NOT_INITIALIZED; };
	__host__ __device__ Term(Type type, unsigned int t, unsigned int arg0, unsigned int arg1, unsigned int arg2, unsigned int parent)
        : type(type), t(t), arg0(arg0), arg1(arg1), arg2(arg2), parent(parent){
		
		}
};

__host__ __device__ inline bool is_Zero(Term &t) {
	return t.type == Zero;
}
__host__ __device__ inline bool is_S(Term &t) {
	return t.type == S;
}
__host__ __device__ inline bool is_NaN(Term &t) {
	return t.type == NaN;
}
__host__ __device__ inline bool is_Head(Term &t) {
	return t.type == Head;
}
__host__ __device__ inline bool is_True(Term &t) {
	return t.type == True;
}
__host__ __device__ inline bool is_False(Term &t) {
	return t.type == False;
}
__host__ __device__ inline bool is_Not(Term &t) {
	return t.type == Not;
}
__host__ __device__ inline bool is_Or(Term &t) {
	return t.type == Or;
}
__host__ __device__ inline bool is_And(Term &t) {
	return t.type == And;
}
__host__ __device__ inline bool is_Eq(Term &t) {
	return t.type == Eq;
}
__host__ __device__ inline bool is_Lt(Term &t) {
	return t.type == Lt;
}
__host__ __device__ inline bool is_Gt(Term &t) {
	return t.type == Gt;
}
__host__ __device__ inline bool is_Leq(Term &t) {
	return t.type == Leq;
}
__host__ __device__ inline bool is_Geq(Term &t) {
	return t.type == Geq;
}
__host__ __device__ inline bool is_Nil(Term &t) {
	return t.type == Nil;
}
__host__ __device__ inline bool is_Cons(Term &t) {
	return t.type == Cons;
}
__host__ __device__ inline bool is_Append(Term &t) {
	return t.type == Append;
}
__host__ __device__ inline bool is_Sort(Term &t) {
	return t.type == Sort;
}
__host__ __device__ inline bool is_SortedNil(Term &t) {
	return t.type == SortedNil;
}
__host__ __device__ inline bool is_SortedCons(Term &t) {
	return t.type == SortedCons;
}
__host__ __device__ inline bool is_Merge(Term &t) {
	return t.type == Merge;
}
__host__ __device__ inline bool is_MergeIf(Term &t) {
	return t.type == MergeIf;
}

__global__ void Rewrite(Term *term, bool *done, bool *frontier, bool *nf);

//global variables...
extern __device__ unsigned int next_free;
extern __device__ unsigned int root_idx;

#endif //PEANO_EXPERIMENTS_TERM_CUH