#ifndef PEANO_EXPERIMENTS_TERM_H
#define PEANO_EXPERIMENTS_TERM_H

#include <stack>
#include <iostream>
#include <vector>

enum Head_Symbol { Zero, S, Plus, Mult, InputPlus, InputMult,  Input, NOT_INITIALIZED };

inline const char* HS2String(Head_Symbol t) {
  switch(t) {
	case Zero: return "Zero";
	case S: return "S";
	case Plus: return "Plus";
	case Mult: return "Mult";
	case InputPlus: return "InputPlus";
	case InputMult: return "InputMult";
	
  default: return "NOT_INITIALIZED";
  }
}

struct Term {
    Head_Symbol head_symbol;
    unsigned int index = 0;
    unsigned int arg0; 
    unsigned int arg1; 
    Term() { head_symbol = NOT_INITIALIZED; }
    Term(Head_Symbol head_symbol, unsigned int index, unsigned int arg0, unsigned int arg1)
	  : head_symbol(head_symbol), index(index), arg0(arg0), arg1(arg1){
	}

  std::string toString(std::vector<Term> term) {
	switch(this->head_symbol) {
	case Zero: {
	  return std::string() + "Zero("  + ")";
	}
	case S: {
	  return std::string() + "S("  + term[this->arg0].toString(term)  + ")";
	}
	case Plus: {
	  return std::string() + "Plus("  + term[this->arg0].toString(term)  + ", " + term[this->arg1].toString(term)  + ")";
	}
	case Mult: {
	  return std::string() + "Mult("  + term[this->arg0].toString(term)  + ", " + term[this->arg1].toString(term)  + ")";
	}
	case InputPlus: {
	  return std::string() + "InputPlus("  + term[this->arg0].toString(term)  + ", " + term[this->arg1].toString(term)  + ")";
	}
	case InputMult: {
	  return std::string() + "InputMult("  + term[this->arg0].toString(term)  + ", " + term[this->arg1].toString(term)  + ")";
	}
	

	default:
	  return "Type: " + std::string(HS2String(this->head_symbol));
	}	
  }
  
};

struct Indices {
  std::vector<Term> &term;
  std::vector<int> &refs;
  std::vector<bool> &nf;
  std::stack<unsigned int> free_indices;

  Indices(std::vector<Term> &term, std::vector<int> &refs, std::vector<bool> &nf, std::stack<unsigned int> free_indices)
	: term(term), refs(refs), nf(nf), free_indices(free_indices) {
  }

  void decRef(unsigned int idx) {
	this->refs[idx]--;
	if (this->refs[idx] == 0) {
	  Term &t = this->term[idx];
	  if(t.arg0 != 0) {
		this->decRef(t.arg0);
		}
	  if(t.arg1 != 0) {
		this->decRef(t.arg1);
		}
	  this->free_indices.push(idx);
	}
	else if (this->refs[idx] < 0) {
	  this->refs[idx] = 0;
	}
  }
  
  void incRef(unsigned int idx, int amt) {
	this->refs[idx] += amt;
  }
  
  unsigned int getRef() {
	if(!this->free_indices.empty()) {
	  auto ret = this->free_indices.top();
	  this->free_indices.pop();
	  this->refs[ret]++;
	  return ret;
	} else {
	  this->refs.push_back(1);
	  this->nf.push_back(false);
	  this->term.resize(term.size() + 1);
	  return this->refs.size();
	}
  }
};


inline bool is_Zero(Term &t) {
	return t.head_symbol == Zero;
}
inline bool is_S(Term &t) {
	return t.head_symbol == S;
}
inline bool is_Plus(Term &t) {
	return t.head_symbol == Plus;
}
inline bool is_Mult(Term &t) {
	return t.head_symbol == Mult;
}
inline bool is_InputPlus(Term &t) {
	return t.head_symbol == InputPlus;
}
inline bool is_InputMult(Term &t) {
	return t.head_symbol == InputMult;
}

unsigned int Rewrite(Term &t, std::vector<Term> term, std::vector<bool> nf, Indices &indices);

#endif //PEANO_EXPERIMENTS_TERM_CUH