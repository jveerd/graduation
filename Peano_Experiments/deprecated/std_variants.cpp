//#include <iostream>
//#include <type_traits>
//#include <variant>
//
//class Peano_Zero;
//class Peano_Succ;
//class Peano_Add;
//class Peano_Mult;
//
//typedef std::variant<Peano_Zero, Peano_Succ, Peano_Add, Peano_Mult> Peano_Term;
//
//class Peano_Zero { };
//
//class Peano_Succ
//{
//public:
//	Peano_Succ(Peano_Term& t) : n(t) {}
//	Peano_Term n;
//};
//
//class Peano_Add
//{
//public:
//	Peano_Add(Peano_Term& lhs, Peano_Term& rhs) : lhs(lhs), rhs(rhs) {}
//	Peano_Term lhs;
//	Peano_Term rhs;
//};
//
//class Peano_Mult
//{
//public:
//	Peano_Mult(Peano_Term& lhs, Peano_Term& rhs) : lhs(lhs), rhs(rhs) {}
//	Peano_Term lhs;
//	Peano_Term rhs;
//};
//
//struct Rewrite_Rules {
//	auto operator()(Peano_Zero& z) -> Peano_Term {
//		return z;
//	}
//
//	auto operator()(Peano_Succ& s) -> Peano_Term {
//		return s;
//	}
//
//	auto operator()(Peano_Add& a) -> Peano_Term {
//		if (std::holds_alternative<Peano_Zero>(a.lhs)) //lhs is 0
//			return std::visit(Rewrite_Rules(), a.rhs);
//		else if (std::holds_alternative<Peano_Succ>(a.lhs)) { //lhs is a successor
//			//a.lhs = std::get<Peano_Succ>(a.lhs).n;
//			Peano_Term lhs_ = std::get<Peano_Succ>(a.lhs).n;
//			Peano_Term a_ = Peano_Add(lhs_, a.rhs);
//			Peano_Term a__ = Peano_Succ(a_);
//			return std::visit(Rewrite_Rules(), a__);
//		}
//		// What if lhs is add or mult?
//	}
//
//	auto operator()(Peano_Mult& m) -> Peano_Term {
//		if (std::holds_alternative<Peano_Zero>(m.lhs)) //lhs is 0
//			return m.lhs;
//		else if (std::holds_alternative<Peano_Succ>(m.lhs)) { //S(n)*y
//			Peano_Term lhs_ = std::get<Peano_Succ>(m.lhs).n;
//			Peano_Term m_ = Peano_Mult(lhs_, m.rhs); //(n*y)
//			Peano_Term a = Peano_Add(m_, m.rhs); //(n*y) + y
//			return std::visit(Rewrite_Rules(), a);
//		}
//	}
//};
//
//int main(int argc, const char* argv[])
//{
//	std::cout << "foo" << std::endl;
//}