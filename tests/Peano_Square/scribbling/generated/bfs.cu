#include "../bfs.cuh"
#include <stdio.h>
__global__ void bfs_kernel(bool *frontier, bool *nf, bool *done, unsigned int n_terms) {
    int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id >= n_terms)
	  return;
    bool f = frontier[id];
    __syncthreads();

    if (f)
    {
        Term &my_term = term[id];bool n0 = nf[my_term.arg0];bool n1 = nf[my_term.arg1];
        if(n0 && n1) return;
        if(!n0) {
		  //printf("term[%u].frontier = false, frontier[%u] = true\n", id, my_term.arg0);
            frontier[id] = false;
            frontier[my_term.arg0] = true;
            *done = false;
        }
        if(!n1) {
		  //printf("term[%u].frontier = false, frontier[%u] = true\n", id, my_term.arg1);
            frontier[id] = false;
            frontier[my_term.arg1] = true;
            *done = false;
        }
    }
}