#ifndef PEANO_EXPERIMENTS_TERM_H
#define PEANO_EXPERIMENTS_TERM_H

#include <stack>
#include <iostream>

enum Head_Symbol { {% for f in functions%}{{f.name}}, {% endfor %} Input, NOT_INITIALIZED };

inline const char* HS2String(Head_Symbol t) {
  switch(t) {
	{%for f in functions%}case {{f.name}}: return "{{f.name}}";
	{% endfor %} {%- if input_term %} case Input: return "Input";{%-endif%}
  default: return "NOT_INITIALIZED";
  }
}

struct Term {
    Head_Symbol head_symbol;
    unsigned int index = 0;
	{%- for i in range(maxarity) %}
    unsigned int arg{{loop.index0}}; {% endfor %}
    Term() { head_symbol = NOT_INITIALIZED; }
    Term(Head_Symbol head_symbol, unsigned int index, {% for i in range(maxarity) %}unsigned int arg{{loop.index0}}{{", " if not loop.last}}{% endfor %})
	  : head_symbol(head_symbol), index(index), {% for i in range(maxarity) %}arg{{loop.index0}}(arg{{loop.index0}}){{", " if not loop.last}}{% endfor %}{
	}

  std::string toString(Term *term) {
	switch(this->head_symbol) {
	{% for f in functions%}case {{f.name}}: {
	  return std::string() + "{{f.name}}(" {% for i in range(f.formals|length) %} + term[this->arg{{i}}].toString(term) {{ ' + ", "' if not loop.last}}{%- endfor %} + ")";
	}
	{%endfor%}

	default:
	  return "Type: " + std::string(HS2String(this->head_symbol));
	}	
  }
  
};

struct Indices {
  Term *term;
  int *refs;
  unsigned int next_free;
  std::stack<unsigned int> free_indices;

  Indices(Term *term, int *refs, unsigned int next_free, std::stack<unsigned int> free_indices)
	: term(term), refs(refs), next_free(next_free), free_indices(free_indices) {
  }

  void decRef(unsigned int idx) {
	this->refs[idx]--;
	if (this->refs[idx] == 0) {
	  Term &t = this->term[idx];
	  {%- for i in range(maxarity) %}
	  if(t.arg{{loop.index0}} != 0) {
		this->decRef(t.arg{{loop.index0}});
		}{%-endfor%}
	  this->free_indices.push(idx);
	}
	else if (this->refs[idx] < 0) {
	  this->refs[idx] = 0;
	}
  }
  
  void incRef(unsigned int idx, int amt) {
	this->refs[idx] += amt;
  }
  
  unsigned int getRef() {
	if(!this->free_indices.empty()) {
	  auto ret = this->free_indices.top();
	  this->free_indices.pop();
	  this->refs[ret]++;
	  return ret;
	} else {
	  this->refs[this->next_free] = 1;
	  return this->next_free++;
	}
  }
};

{% for f in functions %}
inline bool is_{{f.name}}(Term &t) {
	return t.head_symbol == {{f.name}};
}{% endfor %}

unsigned int Rewrite(Term &t, Term *term, bool *nf, Indices &indices);

#endif //PEANO_EXPERIMENTS_TERM_CUH
