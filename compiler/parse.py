import pretty_printer as pp
import argparse
import os
import sys
from textx import metamodel_from_file
from jinja2 import Environment, FileSystemLoader, PackageLoader, select_autoescape

parser = argparse.ArgumentParser(description='Generate CUDA code for a given TRS.')
parser.add_argument('-o', '--out', dest='outpath', default=os.path.dirname(os.path.realpath(__file__)),
                    help='Specify output directory for generated files')
parser.add_argument('-f', '--file', dest='trs', default=None, required=True,
                    help='Filename of TRS to generate code for')
parser.add_argument('-C', '--cpp', dest='cpp', action='store_const', const=True, default=False,
                    help='Generate regular C++ instead of CUDA code')
parsed_args = parser.parse_args()
parsed_args.outpath = parsed_args.outpath.rstrip('/')

dsl_meta = metamodel_from_file(os.path.dirname(os.path.realpath(__file__))+'/trs.tx')
model = dsl_meta.model_from_file(parsed_args.trs)

flatten = lambda list: [item for sublist in list for item in sublist]
arity = lambda constructor: len(constructor.formals)

functions = flatten(map(lambda s: s.functions, model.sortDefs))
variables = model.varDefs #map(lambda v: v.name, model.varDefs)

sorted_eqns = {}
for f in functions:
    sorted_eqns[f] = filter(lambda e: e.lhs.hs == f, model.eqnDefs)

class Acc:
    is_acc = True
    is_toplevel = False
    i = None
    term = None
    
    def __init__(self, i, term):
        self.i = i
        self.term = term

class Rec:
    is_rec = True
    hs = None
    term = None

    def __init__(self, hs, term):
        self.hs = hs
        self.term = term

class Term:
    is_term = True
    is_new_term = False
    is_toplevel = False
    hs = None
    args = None

    def __init__(self, hs, args):
        self.hs = hs
        self.args = args

class Arg:
    is_arg = True
    name = None

    def __init__(self, name):
        self.name = name

class If:
    is_if = True
    Preds = []
    VarCounts = {}
    Then = None
    Else = None

    def __init__(self, Preds, VarCounts, Then, Else):
        self.Preds = Preds
        self.VarCounts = VarCounts
        self.Then = Then
        self.Else = Else

def pred(L,l):
    if L.var in variables:
        return [] #empty list as && true would mean nothing anyway
    else: #L is a constructed term
        p = [Rec(L.hs, l)]
        newp = []
        for i in range(arity(L.hs)):
            newp += pred(L.args[i], Acc(i,l))
        p.extend(newp)
        return p

def pred_popfirst(L,l):
    p = pred(L,l)
    p.pop(0)
    return p

def binds(L,l,sigma):
    if L.var in variables:
        sigma[L.var] = l
    elif arity(L.hs) > 0:
        for i in reversed(range(arity(L.hs))):
            binds(L.args[i], Acc(i,l), sigma)
    return

def res(R,sigma):
    if R.var in variables:
        return sigma[R.var]
    else:
        args = []
        if(arity(R.hs)):
            for i in range(arity(R.hs)):
                args.append(res(R.args[i], sigma))
        return Term(R.hs, args)

def rule2term(r):
    if r.var in variables:
        return Arg(r.var.name)
    else:
        args = []
        for i in range(arity(r.hs)):
            args.append(rule2term(r.args[i]))
        return Term(r.hs, args)

def find_vars(t, a, v):
    if(isinstance(t, Arg)):
        v[t.name] = a
    elif(arity(t.hs) > 0):
        for i in range(arity(t.hs)):
            find_vars(t.args[i], Acc(i, a), v)
    return

def count_var(v, R):
    count = 0
    if(isinstance(R, Term)):
        for i in range(arity(R.hs)):
            count += count_var(v, R.args[i])
    else:
        if(R.name == v.name):
            count += 1
    return count

def counts(lhs_vars, rhs_counts, rhs_term):
    if not lhs_vars:
        return
    for v in variables:
        if v.name in lhs_vars:
            c = count_var(v, rhs_term)
            if c != 0:
                rhs_counts[lhs_vars[v.name]] = c
    return rhs_counts
    
    
def combine(T,R,l):
    if len(R) == 0:
        return T
    else:
        r = R.pop(0)
        lhs_term = rule2term(r.lhs)
        lhs_vars = {}
        find_vars(lhs_term, l, lhs_vars)
        rhs_term = rule2term(r.rhs)
        rhs_counts = {}
        counts(lhs_vars, rhs_counts, rhs_term)
        sigma = {}
        binds(r.lhs,l,sigma)
        result = res(r.rhs,sigma)
        result.is_toplevel = True
        
        return combine(
            If(pred_popfirst(r.lhs,l),
               rhs_counts,
               result,
               T),
            R,l)

newSystem = {}

for f, eqns in sorted_eqns.items():
    if(eqns):
        args = []
        for i in range(arity(f)):
            args.append(Arg('X' + str(i)))
        newTerm = Term(f, args)
        newTerm.is_new_term = True
        newSystem[f] = combine(newTerm, eqns, newTerm)

rewritable_terms = {}
for f in functions:
    rewritable_terms[f] = f in newSystem

input_term = res(model.input,{})
input_term.is_toplevel = True

template_dir = '/cpp_templates' if parsed_args.cpp else '/templates';

env = Environment(loader=FileSystemLoader(os.path.dirname(os.path.realpath(__file__))+template_dir))

def generate(termh, rule, inp, bfs=False):
    obj = env.get_template(termh).render(functions=functions, maxarity=max(map(arity, functions)))
    rules = env.get_template(rule).render(system=newSystem, maxarity=max(map(arity, functions)), rewritable_terms=rewritable_terms)
    if(bfs):
        bfs = env.get_template('bfs.cu').render(maxarity=max(map(arity, functions)))
        bfs_f = open(parsed_args.outpath+'/bfs.cu', 'w+')
        bfs_f.write(bfs)
        bfs_f.close()
    testf = env.get_template(inp).render(input_term = input_term, rewritable_terms=rewritable_terms, functions=functions, maxarity=max(map(arity, functions)))

    obj_f = open(parsed_args.outpath+'/'+termh, 'w+')
    obj_f.write(obj)
    obj_f.close()

    termfname = '/term.cpp' if parsed_args.cpp else '/term.cu';
    rules_f = open(parsed_args.outpath+termfname, 'w+')
    rules_f.write(rules)
    obj_f.close()

    testfname = '/test.cpp' if parsed_args.cpp else '/test.cu';
    test_f = open(parsed_args.outpath+testfname, 'w+')
    test_f.write(testf)
    test_f.close()

if(parsed_args.cpp):
    generate('term.h', 'rule.cpp', 'input.cpp')
else:
    generate('term.cuh', 'rule.cu', 'input.cu', 'bfs.cu')


