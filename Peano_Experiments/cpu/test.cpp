#include <iostream>
#include <chrono>
#include "Term.hpp"

#define DEPTH 4

unsigned int next_free;

int make_big_term(int depth, Type bottom, Type rest) {
    term = new Term[1<<(depth+1)];
    in_normalform = new bool[1<<(depth+1)];

    term[0] = Term(Zero, 0, 0, 0);

    for(unsigned int i = 1; i <= (1<<depth); i++) {
        term[i] = Term(bottom, i, 0, 0);
    }

    unsigned int start_index = (1<<depth);

    for(int d = depth-1; d >= 0; d--) {
        unsigned int end_index = start_index + (1<<d);
        unsigned int prev_lvl = start_index - (1<<(d+1));
        for(unsigned int i = start_index + 1; i <= end_index; i++) {
            unsigned int j = i - start_index;
            term[i] = Term(rest, i, prev_lvl + j*2-1, prev_lvl + j*2);
        }
        start_index = end_index;
    }

    next_free = start_index + 1;
    return start_index;
}

int make_big0(int depth) {
    return make_big_term(depth, Add, Add);
}


int make_big1(int depth) {
    return make_big_term(depth, Succ, Mult);
}

int make_2x2x2x2() {
    term = new Term[50];
    in_normalform = new bool[50];

    term[0] = Term(Zero, 0, 0, 0); //zero

    term[1] = Term(Succ, 1, 0, 0); //lone1
    term[2] = Term(Succ, 2, 0, 0); //lone2
    term[3] = Term(Succ, 3, 0, 0); //rone1
    term[4] = Term(Succ, 4, 0, 0); //rone2

    term[5] = Term(Succ, 5, 1, 0); //ltwo1
    term[6] = Term(Succ, 6, 2, 0); //ltwo2
    term[7] = Term(Succ, 7, 3, 0); //rtwo1
    term[8] = Term(Succ, 8, 4, 0); //rtwo2

    term[9] = Term(Mult, 9, 5, 6); //mul1 = ltwo1 * ltwo2
    term[10] = Term(Mult, 10, 7, 8); //mul2 = rtwo1 * rtwo2

    term[11] = Term(Mult, 11, 9, 10); //mul = mul1 * mul2

    next_free = 12;

    return 11;
}

std::ostream& operator<<(std::ostream& os, const Term& pt) {
    os << pt.toString();
    return os;
}

int main(int argc, const char* argv[])
{
    int depth = DEPTH;
    int big = 1;
    if(argc > 2) {
        big = atoi(argv[2]);
        depth = atoi(argv[1]);
    } else if(argc > 1) {
        depth = atoi(argv[1]);
        std::cout << "No term specified, continuing with big1 with depth = " << depth << std::endl;
    } else {
        std::cout << "No depth provided, continuing with big1 with depth = " << DEPTH << std::endl;
    }

    unsigned int root = (big == 0) ? make_big0(depth) : make_big1(depth);

//    unsigned int root = make_2x2x2x2();
    using milli = std::chrono::milliseconds;
    auto start = std::chrono::high_resolution_clock::now();
    term[root].Rewrite();
    auto finish = std::chrono::high_resolution_clock::now();

    std::cout << "Done rewriting after "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

    std::cout << "Rewritten to: " << term[root].toString() << std::endl;
    return 0;
}