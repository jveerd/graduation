{% extends "test.cpp" %}

{%- macro subterm_ctr(term, id) %}
	{%- if term.is_term %}unsigned int idx{{id}} = indices->getRef();
	{%- for a in term.args %}
	{{subterm_ctr(a, id ~ '_' ~ loop.index0)}}
	{%- endfor %}
    Term t{{id}} = Term({{term.hs.name}}, idx{{id}},{% for i in range(maxarity) %}{%- if loop.index0 < term.args|length %} t{{id ~ '_' ~ loop.index0}}.index{%- else %} 0{%-endif%}{{", " if not loop.last}}{%- endfor %});
	term[idx{{id}}] = t{{id}};
        {%- if rewritable_terms[term.hs] %}
    nf[idx{{id}}] = false;
        {%- elif term.args %}
    nf[idx{{id}}] = {%- for a in term.args %} nf[idx{{id ~ '_' ~ loop.index0}}] {{ " && " if not loop.last }}{%- endfor %};
        {%- else %}
	nf[idx{{id}}] = true;
	    {%- endif %}
	{%- endif %}
{%- endmacro %}

{%- macro print_term(term, input) %}
    {%- for a in term.args %}
	{{subterm_ctr(a, loop.index0)}}
	{%- endfor %}

	t.head_symbol = {{term.hs.name}};

    {%- for i in range(maxarity) %}
    {%- if i >= term.args|length %}
    t.arg{{i}} = 0;
	{%- else %}
    t.arg{{i}} = t{{i}}.index;
	{%- endif %}
    {%- endfor %}
    term[t.index] = t;

    {%- if term.args %}
	nf[t.index] = false;
    {%- else %}
    nf[t.index] = true;
	{%-endif%}
{%- endmacro %}

{%- block input %}
void Expand_Input(Term &t) {
  {{print_term(input_term)}}
}
{%- endblock %}
