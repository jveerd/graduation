#include <iostream>
#include "generated/term.cuh"
#include "bfs.cuh"

#define DEPTH 18

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess)
    {
	  fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	  if (abort) exit(code);
    }
}

//bool verbose;
Term *term;
__device__ unsigned int next_free;
__device__ unsigned int root_idx;

__global__ void init(unsigned int d_next_free, unsigned int d_root_idx) {
  next_free = d_next_free;
  root_idx = d_root_idx;
}

unsigned int make_Sort_Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Nil, 0, 0, 0, 0, 0);
  nf_ptr[0] = true;

  term_ptr[1] = Term(Sort, 1, 0, 0, 0, 2);
  nf_ptr[1] = false;
  
  init<<<1,1>>>(2, 1);
  return 1;
}

unsigned int make_Sort_Cons0Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 0, 2);
  nf_ptr[0] = true;

  term_ptr[1] = Term(Nil, 1, 0, 0, 0, 2);
  nf_ptr[1] = true;

  term_ptr[2] = Term(Cons, 2, 0, 1, 0, 3);
  nf_ptr[2] = true;

  term_ptr[3] = Term(Sort, 3, 2, 0, 0, 4);
  nf_ptr[3] = false;
  
  init<<<1,1>>>(4, 3);
  return 3;
}

unsigned int make_Sort_Cons1Cons0Nil(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 0, 1);
  nf_ptr[0] = true;

  term_ptr[1] = Term(S, 1, 0, 0, 0, 3);
  nf_ptr[1] = true;
  
  term_ptr[2] = Term(Nil, 2, 0, 0, 0, 3);
  nf_ptr[2] = true;

  term_ptr[3] = Term(Cons, 3, 0, 2, 0, 4);
  nf_ptr[3] = true;
  
  term_ptr[4] = Term(Cons, 4, 1, 3, 0, 5);
  nf_ptr[4] = true;

  term_ptr[5] = Term(Sort, 5, 4, 0, 0, 100);
  nf_ptr[5] = false;
  
  init<<<1,1>>>(6, 5);
  return 5;
}

__global__ void Result_SortedNil(Term *term, bool *correct) {
  Term temp = term[root_idx];
  *correct = temp.type == Nil;
}

__global__ void Result_SortedCons0Nil(Term *term, bool *correct) {
  Term temp = term[root_idx];
  bool c = temp.type == Cons && term[temp.arg0].type == Zero && term[temp.arg1].type == Nil;
  if(!c) {
	printf("root type: %s\n", Type2String(temp.type));
	printf("arg0 type: %s\n", Type2String(term[temp.arg0].type));
	printf("arg1 type: %s\n", Type2String(term[temp.arg1].type));
  }

  *correct = c;
}

__global__ void Result_SortedCons0SortedCons1Nil(Term *term, bool *correct) {
  Term temp = term[root_idx];
  bool c =
	temp.type == Cons &&
	term[temp.arg0].type == Zero &&
	term[temp.arg1].type == Cons &&
	term[term[temp.arg1].arg0].type == S &&
	term[term[term[temp.arg1].arg0].arg0].type == Zero &&
	term[term[temp.arg1].arg1].type == Nil;
  *correct = c;
}

int countdown(Term *term_ptr, Term &t) {
  return t.type == Zero ? 0 : 1 + countdown(term_ptr, term_ptr[t.arg0]);
}


std::string term2string(Term *term_ptr, Term &t) {
  switch(t.type) {
  case Zero:
	return "0";

  case S:
	return std::to_string(countdown(term_ptr, t));

  case Nil:
	return "[]";
	
  case Cons:
	return "Cons(" + term2string(term_ptr, term_ptr[t.arg0]) + ", " + term2string(term_ptr, term_ptr[t.arg1]) + ")";

  case Sort:
	return "Sort(" + term2string(term_ptr, term_ptr[t.arg0]) + ")";

  case Sort2:
	return "Sort2(" + term2string(term_ptr, term_ptr[t.arg0]) + ", " + term2string(term_ptr, term_ptr[t.arg1]) + ")";

  case If:
	return "If " + term2string(term_ptr, term_ptr[t.arg0]) + "\nThen " + term2string(term_ptr, term_ptr[t.arg1]) + "\nElse " + term2string(term_ptr, term_ptr[t.arg2]);

  case Lt:
	return term2string(term_ptr, term_ptr[t.arg0]) + " < " + term2string(term_ptr, term_ptr[t.arg1]);

  case Gt:
	return term2string(term_ptr, term_ptr[t.arg0]) + " > " + term2string(term_ptr, term_ptr[t.arg1]);

  case Len:
	return "#" + term2string(term_ptr, term_ptr[t.arg0]);

  default:
	return "Type: " + std::string(Type2String(t.type));
  }
}

__global__ void update(unsigned int *current_terms) {
  *current_terms = next_free + 1;
}

int run_test(unsigned int num_terms, Term* term, bool *nf_ptr, unsigned int (*ctr)(Term* t, bool* n), void (*check) (Term *p, bool *c)) {

  // std::cout << "Start of new test run\n";
  unsigned int root = ctr(term, nf_ptr);
  unsigned int *current_terms;
  cudaMallocManaged((void**)&current_terms, sizeof(unsigned int));
  *current_terms = root;

  int device;
  gpuErrchk(cudaGetDevice(&device));
  cudaDeviceProp devProp;
  cudaGetDeviceProperties(&devProp,device);

  // Get the kernel configuration
  int numBlocks = (int)ceil((double)min(num_terms, *current_terms)/(double)devProp.maxThreadsPerBlock);
  int numThreads = min(min(devProp.maxThreadsPerBlock, (int)num_terms), (int)*current_terms)+1;

  // std::cout << "Blocks: " << numBlocks << "\nThreads: " << numThreads << std::endl;
  // std::cout << "Term at highest thread index: " << term2string(term, term[numThreads]) << std::endl;


  // Find first frontier of rewritable terms
  bool *done;
  cudaMallocManaged((void**)&done, sizeof(bool));
  *done = false;
  bool *frontier;
  gpuErrchk(cudaMallocManaged((void**) &frontier, num_terms * sizeof(bool)));

  for(int i = 0; i < num_terms; i++) {
	frontier[i] = false;
  }
  frontier[root] = true;

  int iter = 0;
  float ms_time = 0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  float terms_handled;
  while(!(*done)) {
	terms_handled += numBlocks * numThreads;
	iter++;
	*done = true;

	cudaEventRecord(start);

	bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;
  }

  *done = false;
  auto rewrites = 0;
  while(!(*done)) {
	// for(int i = 0; i < *current_terms; i++) {
	//   if(frontier[i])
	// 	std::cout << "Term " << i << " in frontier: " << term2string(term, term[i]) << std::endl;
	// }
	terms_handled += numBlocks * numThreads;
	iter++;
	rewrites++;
	*done = true;

	//	std::cout << "Term pre rewrite: " << term2string(term, term[root]) << std::endl;
	
	cudaEventRecord(start);

	Rewrite<<<numBlocks, numThreads>>>(term, done, frontier, nf_ptr);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);

	// std::cout << "Term post rewrite: " << term2string(term, term[root]) << std::endl;
	// std::cout << "End of rewrite step " << rewrites << std::endl;
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;

	update<<<1,1>>>(current_terms);
	gpuErrchk(cudaDeviceSynchronize());
	gpuErrchk(cudaPeekAtLastError());
	numBlocks = (int)ceil((double)min(num_terms, *current_terms)/(double)devProp.maxThreadsPerBlock);
	numThreads = min(min(devProp.maxThreadsPerBlock, (int)num_terms), (int)*current_terms);

	if(!(*done)) {
	  while(!(*done)) {
		terms_handled += numBlocks * numThreads;
		iter++;
		*done = true;

		cudaEventRecord(start);

		bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
		gpuErrchk(cudaPeekAtLastError());

		cudaEventRecord(stop);
		cudaEventSynchronize(stop);
		float temp = 0;
		cudaEventElapsedTime(&temp, start, stop);
		ms_time += temp;
	  }
	  *done = false;
	}
  }

#ifdef VERBOSE
  if(terms_handled < 0) {
	std::cout << "negative amount of terms handled... \n\t#blocks = " << numBlocks << "\n\t#threads = " << numThreads << "\n\titerations = " << iter << std::endl;
  }
  float tps = terms_handled / (ms_time / 1000);

  std::cout.precision(0);
  std::cout << std::fixed << "BFSing and rewriting took " << ms_time << " ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific << terms_handled << "\n T/S: " << tps << std::endl;
#endif

  bool *correct;
  cudaMallocManaged((void**)&correct, sizeof(bool));
  *correct = true;

  check<<<1,1>>>(term, correct);

  gpuErrchk(cudaFree(frontier));
  gpuErrchk(cudaFree(done));

  return !*correct;
}

int main(int argc, const char* argv[])
{
  int depth = DEPTH;
  if(argc > 1) {
	depth = atoi(argv[1]);
  } else {
#ifdef VERBOSE
	std::cout << "No depth provided, continuing big0 with DEPTH=" << DEPTH << std::endl;
#endif
  }

  cudaDeviceReset();

  //TODO: allocate room so the root can put its parent in frontier, ugly but fix later in rewrite steps
  size_t num_terms = (1<<(depth+2)) + 1;
  // std::cout << "Max number of terms: " << num_terms << std::endl;

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));

  bool *nf_ptr;
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  auto result = run_test(num_terms, term, nf_ptr, &make_Sort_Nil, &Result_SortedNil);
  if(result) {
	std::cout << "Result was not Nil...\n";
	return result;
  }
  
  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  result = run_test(num_terms, term, nf_ptr, &make_Sort_Cons0Nil, &Result_SortedCons0Nil);
  if(result) {
  	std::cout << "Result was not Cons(0, Nil)...\n";
  	return result;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  result = run_test(num_terms, term, nf_ptr, &make_Sort_Cons1Cons0Nil, &Result_SortedCons0SortedCons1Nil);
  if(result) {
  	std::cout << "Result was not Cons(0, Cons(1, Nil))...\n";
  	return result;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(nf_ptr));
  gpuErrchk(cudaDeviceReset());
  return 0;
}
