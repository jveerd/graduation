#include <iostream>
#include "term.cuh"
#include "bfs.cuh"

#define DEPTH 4

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess)
    {
	  fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	  if (abort) exit(code);
    }
}

//bool verbose;
Term *term;
__device__ unsigned int next_free;
__device__ unsigned int root_idx;

__global__ void init(unsigned int d_next_free, unsigned int d_root_idx) {
  next_free = d_next_free;
  root_idx = d_root_idx;
}

unsigned int make_big_term(Term *term_ptr, bool *nf_ptr, int depth, Type bottom, Type rest) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 0);
  nf_ptr[0] = true;

  for(unsigned int i = 1; i <= (1<<depth); i++) {
	int parent = i + (1<<depth) - (i >> 1);
	term_ptr[i] = Term(bottom, i, 0, 0, parent);
	nf_ptr[i] = (bottom == Succ) || (bottom == Zero);
  }

  unsigned int start_index = (1<<depth);

  for(int d = depth-1; d >= 0; d--) {
	unsigned int end_index = start_index + (1<<d);
	unsigned int prev_lvl = start_index - (1<<(d+1));
	for(unsigned int i = start_index + 1; i <= end_index; i++) {
	  unsigned int j = i - start_index;
	  unsigned int parent = j + end_index - (j>>1);
	  term_ptr[i] = Term(rest, i, prev_lvl + j*2-1, prev_lvl + j*2, parent);
	  nf_ptr[i] = false;
	}
	start_index = end_index;
  }

  init<<<1,1>>>(start_index + 1, start_index);
  return start_index;
}

unsigned int make_big0(Term *term_ptr, bool *nf_ptr, int depth) {
  return make_big_term(term_ptr, nf_ptr, depth, Plus, Plus);
}

unsigned int make_big1(Term *term_ptr, bool *nf_ptr, int depth) {
  return make_big_term(term_ptr, nf_ptr, depth, Succ, Mult);
}

unsigned int make_16x16(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 0);
  nf_ptr[0] = true;

  for(unsigned int i = 1; i <= 16; i++) {
	term_ptr[i] = Term(Succ, i, i-1, 0, i + 1);
	nf_ptr[i] = true;
  }

  term_ptr[17] = Term(Mult, 17, 16, 16, 100);
  nf_ptr[17] = false;

  init<<<1,1>>>(18, 17);
  return 17;
}

__global__ void Report_Result_0(Term *term) {
  int succs = 0;
  Term temp = term[root_idx];
  if(temp.type != Zero) printf("root is not zero...%d\n", temp.type);
  while(temp.type != Zero) {
	succs++;
	temp = term[temp.arg0];
  }
  printf("Result: %d\n", succs);
}

__global__ void Report_Result_1(Term *term) {
  int succs = 0;
  Term temp = term[root_idx];
  while(temp.type != Zero) {
	succs++;
	temp = term[temp.arg0];
  }
  printf("Result: %d\n", succs);
}

__global__ void Assert_Zeroes(Term *term) {
  unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
  Term &temp = term[id];
  if(temp.type != Zero) printf("Term[%u] is not zero, but %d\n", id, temp.type);
}

int main(int argc, const char* argv[])
{
  int depth = DEPTH;
  int big = 1;
  if(argc > 1) {
	depth = atoi(argv[1]);
  } else {
	std::cout << "No depth provided, continuing big1 with DEPTH=" << DEPTH << std::endl;
  }

  if(argc > 2) {
	big = atoi(argv[2]);
  } else {
	std::cout << "No term specified, continuing with big1 with DEPTH=" << depth << std::endl;
  }

  cudaDeviceReset();

  //TODO: allocate room so the root can put its parent in frontier, ugly but fix later in rewrite steps
  size_t num_terms = (1<<(depth+2)) + 1;
  std::cout << "Max number of terms: " << num_terms << std::endl;

  int device;
  gpuErrchk(cudaGetDevice(&device));
  cudaDeviceProp devProp;
  cudaGetDeviceProperties(&devProp,device);

  //    std::cout << "Cuda device: " << device << std::endl;
  //    std::cout << "Name: " << devProp.name << std::endl;
  //    std::cout << "Max threads per block: " << devProp.maxThreadsPerBlock << std::endl;
  //    std::cout << "Streaming Multiprocessor count: " << devProp.multiProcessorCount << std::endl;
  //    std::cout << "Max grid size (in blocks): " << std::to_string(devProp.maxGridSize[0]) << std::endl;

  // Get the kernel configuration
  //TODO: dynamically determine block size based on number of terms. Right now, some invalid memory
  //TODO: accesses are taking place since last block is not completely filled. Correct results, but not nice.
  int numBlocks = (int)ceil((double)num_terms/(double)devProp.maxThreadsPerBlock);
  int numThreads = min(devProp.maxThreadsPerBlock, (int)num_terms);

  //    std::cout << "Number of blocks: " << numBlocks << std::endl;
  //    std::cout << "Number of threads: " << numThreads << std::endl;

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));

  bool *nf_ptr;
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  unsigned int root = make_16x16(term, nf_ptr);
  //unsigned int root = (big == 0) ? make_big0(term, nf_ptr, depth) : make_big1(term, nf_ptr, depth);

  // Find first frontier of rewritable terms
  bool *done;
  cudaMallocManaged((void**)&done, sizeof(bool));
  *done = false;
  bool *frontier;
  gpuErrchk(cudaMallocManaged((void**) &frontier, num_terms * sizeof(bool)));

  for(int i = 0; i < num_terms; i++) {
	frontier[i] = false;
  }
  frontier[root] = true;

  int iter = 0;
  float ms_time = 0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  while(!(*done)) {
	iter++;
	*done = true;

	cudaEventRecord(start);

	bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;
  }

  *done = false;
  while(!(*done)) {
	iter++;
	*done = true;

	cudaEventRecord(start);

	Rewrite<<<numBlocks, numThreads>>>(term, done, frontier, nf_ptr);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;

	if(!(*done)) {
	  while(!(*done)) {
		iter++;
		*done = true;

		cudaEventRecord(start);

		bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
		gpuErrchk(cudaPeekAtLastError());

		cudaEventRecord(stop);
		cudaEventSynchronize(stop);
		float temp = 0;
		cudaEventElapsedTime(&temp, start, stop);
		ms_time += temp;
	  }
	  *done = false;
	}
  }

  float terms_handled = (float)numBlocks * (float)numThreads * (float)iter;
  if(terms_handled < 0) {
	std::cout << "negative amount of terms handled... \n\t#blocks = " << numBlocks << "\n\t#threads = " << numThreads << "\n\titerations = " << iter << std::endl;
  }
  float tps = terms_handled / (ms_time / 1000);

  std::cout.precision(0);
  std::cout << std::fixed << "BFSing and rewriting took " << ms_time << " ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific << terms_handled << "\n T/S: " << tps << std::endl;

  if(big != 0) {
	Report_Result_1<<<1,1>>>(term);
  } else {
	Report_Result_0<<<1,1>>>(term);
	Assert_Zeroes<<<num_terms,1>>>(term);
  }
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(frontier));
  gpuErrchk(cudaFree(done));
  gpuErrchk(cudaFree(nf_ptr));
  cudaDeviceReset();
  return 0;
}
