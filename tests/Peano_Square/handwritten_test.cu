#include <iostream>
#include "generated/term.cuh"
#include "bfs.cuh"

#define DEPTH 8

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess)
    {
	  fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	  if (abort) exit(code);
    }
}

//bool verbose;
Term *term;
__device__ unsigned int next_free;
__device__ unsigned int root_idx;

__global__ void init(unsigned int d_next_free, unsigned int d_root_idx) {
  next_free = d_next_free;
  root_idx = d_root_idx;
}

unsigned int make_16x16(Term *term_ptr, bool *nf_ptr) {
  term_ptr[0] = Term(Zero, 0, 0, 0, 0);
  nf_ptr[0] = true;

  for(unsigned int i = 1; i <= 16; i++) {
	term_ptr[i] = Term(Succ, i, i-1, 0, i + 1);
	nf_ptr[i] = true;
  }

  term_ptr[17] = Term(Square, 17, 16, 0, 100);
  nf_ptr[17] = false;

  init<<<1,1>>>(18, 17);
  return 17;
}

__global__ void Report_Result_256(Term *term, bool *correct) {
  int succs = 0;
  Term temp = term[root_idx];
  while(temp.type == Succ) {
	succs++;
	temp = term[temp.arg0];
  }
  *correct = succs == 256;
#ifdef VERBOSE  
  printf("Result: %d\n", succs);
#endif
}

__global__ void Assert_Zeroes(Term *term) {
  unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
  Term &temp = term[id];
  if(temp.type != Zero) printf("Term[%u] is not zero, but %d\n", id, temp.type);
}

int main(int argc, const char* argv[])
{
  int depth = DEPTH;

  cudaDeviceReset();

  //TODO: allocate room so the root can put its parent in frontier, ugly but fix later in rewrite steps
  size_t num_terms = (1<<(depth+2)) + 1;

  int device;
  gpuErrchk(cudaGetDevice(&device));
  cudaDeviceProp devProp;
  cudaGetDeviceProperties(&devProp,device);

  // Get the kernel configuration
  //TODO: dynamically determine block size based on number of terms. Right now, some invalid memory
  //TODO: accesses are taking place since last block is not completely filled. Correct results, but not nice.
  int numBlocks = (int)ceil((double)num_terms/(double)devProp.maxThreadsPerBlock);
  int numThreads = min(devProp.maxThreadsPerBlock, (int)num_terms);

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms*sizeof(Term)));

  bool *nf_ptr;
  gpuErrchk(cudaMallocManaged((void**)&nf_ptr, num_terms*sizeof(bool)));

  unsigned int root = make_16x16(term, nf_ptr);
  
  // Find first frontier of rewritable terms
  bool *done;
  cudaMallocManaged((void**)&done, sizeof(bool));
  *done = false;
  bool *frontier;
  gpuErrchk(cudaMallocManaged((void**) &frontier, num_terms * sizeof(bool)));

  for(int i = 0; i < num_terms; i++) {
	frontier[i] = false;
  }
  frontier[root] = true;

  int iter = 0;
  float ms_time = 0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  while(!(*done)) {
	iter++;
	*done = true;

	cudaEventRecord(start);

	bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;
  }

  *done = false;
  while(!(*done)) {
	iter++;
	*done = true;

	cudaEventRecord(start);

	Rewrite<<<numBlocks, numThreads>>>(term, done, frontier, nf_ptr);
	gpuErrchk(cudaPeekAtLastError());

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float temp = 0;
	cudaEventElapsedTime(&temp, start, stop);
	ms_time += temp;

	if(!(*done)) {
	  while(!(*done)) {
		iter++;
		*done = true;

		cudaEventRecord(start);

		bfs_kernel<<<numBlocks, numThreads>>>(term, frontier, nf_ptr, done);
		gpuErrchk(cudaPeekAtLastError());

		cudaEventRecord(stop);
		cudaEventSynchronize(stop);
		float temp = 0;
		cudaEventElapsedTime(&temp, start, stop);
		ms_time += temp;
	  }
	  *done = false;
	}
  }

  #ifdef VERBOSE
  float terms_handled = (float)numBlocks * (float)numThreads * (float)iter;
  if(terms_handled < 0) {
	std::cout << "negative amount of terms handled... \n\t#blocks = " << numBlocks << "\n\t#threads = " << numThreads << "\n\titerations = " << iter << std::endl;
  }
  float tps = terms_handled / (ms_time / 1000);

  std::cout.precision(0);
  std::cout << std::fixed << "BFSing and rewriting took " << ms_time << " ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific << terms_handled << "\n T/S: " << tps << std::endl;
#endif


  bool *correct;
  cudaMallocManaged((void**)&correct, sizeof(bool));
  *correct = true;

  Report_Result_256<<<1,1>>>(term, correct);
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  if(!*correct) {
	std::cout << "Result was not 256.\n";
	return 1;
  }

  gpuErrchk(cudaFree(term));
  gpuErrchk(cudaFree(frontier));
  gpuErrchk(cudaFree(done));
  gpuErrchk(cudaFree(nf_ptr));
  cudaDeviceReset();
  return 0;
}
