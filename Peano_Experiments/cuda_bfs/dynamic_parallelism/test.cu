#include <iostream>
#include <chrono>
#include "Term.cuh"

#define DEPTH 20

//bool verbose;
__device__ Term *term;
__device__ bool *in_normalform;
__device__ unsigned int next_free;
__device__ unsigned int root_idx;

__device__ void make_big_term(Term *term_ptr, bool *nf_ptr, int depth, Type bottom, Type rest) {
    term = term_ptr;
    in_normalform = nf_ptr;

    term[0] = Term(Zero, 0, 0, 0);

    for(unsigned int i = 1; i <= (1<<depth); i++) {
        term[i] = Term(bottom, i, 0, 0);
    }

    unsigned int start_index = (1<<depth);

    for(int d = depth-1; d >= 0; d--) {
        unsigned int end_index = start_index + (1<<d);
        unsigned int prev_lvl = start_index - (1<<(d+1));
        for(unsigned int i = start_index + 1; i <= end_index; i++) {
            unsigned int j = i - start_index;
            term[i] = Term(rest, i, prev_lvl + j*2-1, prev_lvl + j*2);
        }
    start_index = end_index;
    }
    next_free = start_index + 1;
    root_idx = start_index;
}

__global__ void make_big0(Term *term_ptr, bool *nf_ptr, int depth) {
    make_big_term(term_ptr, nf_ptr, depth, Add, Add);
}

__global__ void make_big1(Term *term_ptr, bool *nf_ptr, int depth) {
    make_big_term(term_ptr, nf_ptr, depth, Succ, Mult);
}

__global__ void make_2x2x2x2(Term *term_ptr, bool *nf_ptr) {
    term = term_ptr;
    in_normalform = nf_ptr;

    term[0] = Term(Zero, 0, 0, 0); //zero

    term[1] = Term(Succ, 1, 0, 0); //lone1
    term[2] = Term(Succ, 2, 0, 0); //lone2
    term[3] = Term(Succ, 3, 0, 0); //rone1
    term[4] = Term(Succ, 4, 0, 0); //rone2

    term[5] = Term(Succ, 5, 1, 0); //ltwo1
    term[6] = Term(Succ, 6, 2, 0); //ltwo2
    term[7] = Term(Succ, 7, 3, 0); //rtwo1
    term[8] = Term(Succ, 8, 4, 0); //rtwo2

    term[9] = Term(Mult, 9, 5, 6); //mul1 = ltwo1 * ltwo2
    term[10] = Term(Mult, 10, 7, 8); //mul2 = rtwo1 * rtwo2

    term[11] = Term(Mult, 11, 9, 10); //mul = mul1 * mul2
    next_free = 12;
    root_idx = 11;
}

__global__ void Report_Result() {
    int succs = 0;
    Term &temp = term[root_idx];
    while(temp.type != Zero) {
        succs++;
        temp = term[temp.lhs];
    }
    printf("Result: %d\n", succs);
}


int main(int argc, const char* argv[])
{
    using milli = std::chrono::milliseconds;
    int depth = DEPTH;
    int big = 1;
    if(argc > 1) {
        depth = atoi(argv[1]);
    } else {
        std::cout << "No depth provided, continuing big1 with DEPTH=" << DEPTH << std::endl;
    }

    if(argc > 2) {
        big = atoi(argv[2]);
    } else {
        std::cout << "No term specified, continuing with big1 with DEPTH=" << depth << std::endl;
    }

    cudaDeviceReset();

    cudaError_t err = cudaDeviceSetLimit(cudaLimitDevRuntimeSyncDepth, (depth+2));
    if(err != cudaSuccess) {
        std::cout << "Could not set runtimesyncdepth limit: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    err = cudaDeviceSetLimit(cudaLimitDevRuntimePendingLaunchCount, 2048);
    if(err != cudaSuccess) {
        std::cout << "Could not set pendinglaunchcount limit: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }


    size_t req = 1<<(depth+2);
    Term *d_ptr;
    err = cudaMalloc((void**)&d_ptr, req*sizeof(Term));
    if(err != cudaSuccess) {
        std::cout << "Could not cudaMalloc for term array: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    bool *d_nf_ptr;
    err = cudaMalloc((void**)&d_nf_ptr, req*sizeof(bool));
    if(err != cudaSuccess) {
        std::cout << "Could not cudaMalloc for nf array : " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

//    make_2x2x2x2<<<1,1>>>(d_ptr, d_nf_ptr);
    (big == 0) ? make_big0<<<1,1>>>(d_ptr, d_nf_ptr, depth) : make_big1<<<1,1>>>(d_ptr, d_nf_ptr, depth);

    cudaDeviceSynchronize();
    err = cudaGetLastError();
    if(err != cudaSuccess) {
        std::cout << "An error occurred while initializing the term: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    auto start = std::chrono::high_resolution_clock::now();

    rewrite_Root<<<1,1>>>();
    cudaDeviceSynchronize();
    err = cudaGetLastError();
    if(err != cudaSuccess) {
        std::cout << "An error occurred while rewriting the term: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    auto finish = std::chrono::high_resolution_clock::now();

    std::cout << "Done rewriting after "
              << std::chrono::duration_cast<milli>(finish - start).count()
              << " milliseconds\n";

    Report_Result<<<1,1>>>();
    cudaDeviceSynchronize();
    err = cudaGetLastError();
    if(err != cudaSuccess) {
        std::cout << "An error occurred while printing the result: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }

    cudaFree(d_ptr);
    cudaFree(d_nf_ptr);
    cudaDeviceReset();
    return 0;
}
