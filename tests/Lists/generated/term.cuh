#ifndef PEANO_EXPERIMENTS_TERM_CUH
#define PEANO_EXPERIMENTS_TERM_CUH

#include <string>

enum Type { Zero, S, NaN, Head, Nil, Cons, Append, NOT_INITIALIZED };

__host__ __device__ inline const char* Type2String(Type t) {
  switch(t) {
	case Zero: return "Zero";
	case S: return "S";
	case NaN: return "NaN";
	case Head: return "Head";
	case Nil: return "Nil";
	case Cons: return "Cons";
	case Append: return "Append";
	 default: return "NOT_INITIALIZED";
  }
}

struct Term {
    Type type;
    unsigned int t = 0;
	unsigned int arg0; 
	unsigned int arg1; 
    unsigned int parent;
    Term() { type = NOT_INITIALIZED; };
	__host__ __device__ Term(Type type, unsigned int t, unsigned int arg0, unsigned int arg1, unsigned int parent)
        : type(type), t(t), arg0(arg0), arg1(arg1), parent(parent){
		
		}
};

__host__ __device__ inline bool is_Zero(Term &t) {
	return t.type == Zero;
}
__host__ __device__ inline bool is_S(Term &t) {
	return t.type == S;
}
__host__ __device__ inline bool is_NaN(Term &t) {
	return t.type == NaN;
}
__host__ __device__ inline bool is_Head(Term &t) {
	return t.type == Head;
}
__host__ __device__ inline bool is_Nil(Term &t) {
	return t.type == Nil;
}
__host__ __device__ inline bool is_Cons(Term &t) {
	return t.type == Cons;
}
__host__ __device__ inline bool is_Append(Term &t) {
	return t.type == Append;
}

__global__ void Rewrite(Term *term, bool *done, bool *frontier, bool *nf);

//global variables...
extern __device__ unsigned int next_free;
extern __device__ unsigned int root_idx;

#endif //PEANO_EXPERIMENTS_TERM_CUH