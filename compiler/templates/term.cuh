#ifndef PEANO_EXPERIMENTS_TERM_CUH
#define PEANO_EXPERIMENTS_TERM_CUH

enum Head_Symbol { {% for f in functions%}{{f.name}}, {% endfor %} Input, NOT_INITIALIZED };

__host__ __device__ inline const char* HS2String(Head_Symbol t) {
  switch(t) {
	{%for f in functions%}case {{f.name}}: return "{{f.name}}";
	{% endfor %} {%- if input_term %} case Input: return "Input";{%-endif%}
  default: return "NOT_INITIALIZED";
  }
}

struct Term {
    Head_Symbol head_symbol;
    unsigned int index = 0;
	{%- for i in range(maxarity) %}
	unsigned int arg{{loop.index0}}; {% endfor %}
    unsigned int parent;
    Term() { head_symbol = NOT_INITIALIZED; };
	__host__ __device__ Term(Head_Symbol head_symbol, unsigned int index, {% for i in range(maxarity) %}unsigned int arg{{loop.index0}}, {% endfor %}unsigned int parent)
	  : head_symbol(head_symbol), index(index), {% for i in range(maxarity) %}arg{{loop.index0}}(arg{{loop.index0}}), {% endfor %}parent(parent){
		
	  }
};
{% for f in functions %}
__host__ __device__ inline bool is_{{f.name}}(const Term &t) {
	return t.head_symbol == {{f.name}};
}{% endfor %}

__global__ void Rewrite(bool *frontier, bool *nf, const unsigned int * const eligible_terms, const unsigned int * const free_indices, bool *done, const unsigned int wl_size, unsigned int *next_free, int *refs);
__global__ void frontier2wl(unsigned int *nelig, const unsigned int n_terms, const bool * const frontier, unsigned int *eligible_terms);
__global__ void refs2freepos(unsigned int *nfree, const unsigned int n_terms, const int * const refs, unsigned int *free_indices);
__global__ void propagate_refs(const unsigned int n_terms, int *refs);
__global__ void pad_freeindices(const unsigned int offset, const unsigned int required, unsigned int *next_free, unsigned int *free_indices);
//global variables...
extern __managed__ Term *term;

#endif //PEANO_EXPERIMENTS_TERM_CUH
