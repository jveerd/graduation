{% extends "term.cu" %}

{%- macro arg_kind(arg) %}
	{%- if arg.is_acc %}acc
	{%- elif arg.is_term %}idx
	{%- else %} *****ARG_KIND: NOT AN ACCESSOR OR TERM: {{arg.toString()}}***** {%-endif%}
{%- endmacro %}

{%- macro unfold_acc_(acc) %}
	{%- if acc.term.is_acc %}term[{{unfold_acc_(acc.term)}}.arg{{acc.i}}]
	{%- else %}term[t.arg{{acc.i}}] {%- endif %}
{%- endmacro%}

{%- macro unfold_acc(acc) %}
	{%- if acc.term.is_acc %}{{unfold_acc_(acc.term)}}.arg{{acc.i}}
	{%- else %}t.arg{{acc.i}}{%- endif %}
{%- endmacro %}

{%- macro addrefs(varcounts) %}
    {%- for acc, amt in varcounts.items() %}
    {%- if amt > 0 %}atomicAdd(&refs[{{unfold_acc(acc)}}], {{amt}}); {%- endif %}
	{%- endfor %}
{%- endmacro %}

{%- macro make_ctr(term, id, parent, first, varcounts) %}
	{%- if term.is_acc %}unsigned int acc{{id}} = {{unfold_acc(term)}};
	{%- elif term.is_term %}unsigned int idx{{id}} = {%- if first is sameas true %} free_index
													 {%- else %} atomicAggInc(next_free) {%-endif%};
    unsigned int parent{{id}} = {%-if parent %} idx{{parent}} {%else%} t.index {%endif%};
	{%- for a in term.args %}
	{{make_ctr(a, id ~ '_' ~ loop.index0, id)}}
	{%- endfor %}
    Term t{{id}} = Term({{term.hs.name}}, idx{{id}},{% for i in range(maxarity) %}{%- if loop.index0 < term.args|length %} {{arg_kind(term.args[loop.index0])}}{{id ~ '_' ~ loop.index0}}{%- else %} 0{%-endif%}, {%- endfor %} parent{{id}});
	term[idx{{id}}] = t{{id}};
    refs[idx{{id}}] = 1;
    {%- if rewritable_terms[term.hs] %}
    nf[idx{{id}}] = false;
    frontier[idx{{id}}] = true;
    {%- elif not term.args %}
	nf[idx{{id}}] = true;
    frontier[idx{{id}}] = false;
    frontier[parent{{id}}] = true;
    {%- else %}
	nf[idx{{id}}] = false;
    frontier[idx{{id}}] = true;{%- endif %}
    {#printf("{{id}}Term[%u] is now {{term.hs.name}}, {%for i in range(maxarity)%}arg{{i}}:=%u, {%endfor%} parent:=%u\n",
			 idx{{id}}, {%for i in range(maxarity)%}t{{id}}.arg{{i}}, {%endfor%} t{{id}}.parent);#}

	{%- endif %}
{%- endmacro %}

{%- macro print_term(term, varcounts) %}
	{%- if term.is_toplevel %}
	{%- set gc = namespace(first=false) %}{%- for a in term.args %}{%- if a.is_term and not gc.first %}{%- set gc.first = true %}
	{{make_ctr(a, loop.index0, "", true, varcounts)}}{%-else %}{{make_ctr(a, loop.index0, "", false, varcounts)}}{%-endif%}
	{%- endfor %}
	t.head_symbol = {{term.hs.name}};
    {%- for a in term.args %}
    atomicSub(&refs[t.arg{{loop.index0}}], 1);
    t.arg{{loop.index0}} = {{arg_kind(a)}}{{loop.index0}};
    {%- endfor %}
    {%- for i in range(maxarity) %}
    {%- if i >= term.args|length %}
    atomicSub(&refs[t.arg{{loop.index0}}], 1);
    t.arg{{i}} = 0;
    {%- endif %}
    {%- endfor %}
    {%- if not term.args %}
	nf[t.index] = true;
	frontier[t.parent] = true;
    frontier[t.index] = false;
	{%- elif term.args %}
    nf[t.index] = false;
    frontier[t.index] = true;
	{%-endif%}
    {# printf("\tTerm[%u] is now {{term.hs.name}}, {%for i in range(maxarity)%}arg{{i}}:=%u, {%endfor%} parent:=%u\n",
			  t.index, {%for i in range(maxarity)%}t.arg{{i}}, {%endfor%} t.parent); #}

	{%- elif term.is_new_term %}
    printf("Could not rewrite term %u with type {{term.hs.name}} any further, it is now in NF.\n", t.index);
	nf[t.index] = true;
	frontier[t.index] = false;
	frontier[t.parent] = true;
	{%- endif %}
{%- endmacro %}

{%- macro print_acc(acc) %}
  Term &acc = term[{{unfold_acc(acc)}}];
  t.head_symbol = acc.head_symbol;
  {% for i in range(maxarity) %}
  atomicSub(&refs[t.arg{{i}}], 1);
  t.arg{{i}} = acc.arg{{i}};
  {%- endfor %}
  nf[t.index] = true;
  frontier[t.index] = false;
  frontier[t.parent] = true;
{%- endmacro %}

{%- macro print_rec(rec) %}
{%- if rec.term.is_acc %}is_{{rec.hs.name}}({{unfold_acc_(rec.term)}})
{%- elif rec.term.is_term %}is_{{rec.hs.name}}(t)
{%- endif %}
{%- endmacro %}

{%- macro print_if(i) %}
{%- if i.Preds %}if({%- for p in i.Preds%}{{print_rec(p)}}{{ " && " if not loop.last }}{%- endfor %}) {
	{{addrefs(i.VarCounts)}}
    {{print_statement(i.Then, i.VarCounts)}}
  } else {%- if i.Else.is_if %} {{print_statement(i.Else)}}
  {%- else %} {
  	{{print_statement(i.Else)}}
  } {% endif %}
{%- else %}
	{{addrefs(i.VarCounts)}}
    {{print_statement(i.Then)}}
{%- endif %}
{%- endmacro %}

{%- macro print_statement(stmt, varcounts) %}
{%- if stmt.is_acc %}{{print_acc(stmt)}}
{%- elif stmt.is_rec %}{{print_rec(stmt)}}
{%- elif stmt.is_term %}{{print_term(stmt, varcounts)}}
{%- elif stmt.is_if %}{{print_if(stmt)}}
{%- endif %}
{%- endmacro %}

{% block rules %}
{% for function, rule in system.items()%}
__inline__ void __device__ Rewrite_{{function.name}}(Term &t, const unsigned int &free_index, unsigned int *next_free, bool *nf, bool *frontier, int *refs) {
  {{print_statement(rule)}}
}
{% endfor %}
{% endblock %}