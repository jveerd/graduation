#!/usr/bin/env bash
if [ -d build ]; then rm -rf build; fi

keep=false

function do_test {
	if [ -d "$1" ]; then
		if [ ! -d "$1/generated" ]; then mkdir "$1/generated"; fi
		if [ ! -d "$1/build" ]; then mkdir "$1/build"; fi
		python ../compiler/parse.py --out $(pwd)/"$1/generated" --file $(pwd)/"$1/$1.trs"
		mkdir build
		cd build
		cmake $(pwd)/../"$1"/. -DVERBOSE=ON >/dev/null
		make >/dev/null
		./test_gpu
		if [ $? -ne 0 ]; then
			echo "$1 : [FAIL]"
			if [ "$keep" = true ]; then cd ../ && mv build "$1";
			else
				cd ../ && rm -rf build
				rm -rf "$1/generated"
			fi
		else
			echo "$1 : [PASS]"
			cd ../
			if [ "$keep" = false ]; then
				rm -rf build
				rm -rf "$1/generated"
				rm -rf "$1/build"
			else
				mv build/test_gpu "$1"/build
				rm -rf build
			fi
		fi
	fi
}

while getopts ":k" opt; do
  case $opt in
    k)
		keep=true
		;;
    \?)
		echo "Invalid option: -$OPTARG" >&2
		exit
      ;;
  esac
done

shift $(( OPTIND - 1 ))

if [ "$1" != "" ]; then 
	do_test ${1%/}
else
	for test in *; do
		do_test $test
	done
fi
