#include "term.h"

{%- block rules %}{%- endblock%}

unsigned int Rewrite(Term &t, Term *term, bool *nf, Indices &indices) {
  auto rewrites = 0;
  {%- for i in range(maxarity) %}
  if(t.arg{{loop.index0}} > 0 && !nf[t.arg{{loop.index0}}]) {
	rewrites += Rewrite(term[t.arg{{loop.index0}}], term, nf, indices);
  }{%-endfor%}
  rewrites++;
  switch(t.head_symbol) {
	{% for f, rule in system.items()%}case {{ f.name }}:
	  Rewrite_{{f.name}}(t, term, nf, indices);
	  break;
	{% endfor %}default:
	  nf[t.index] = true;
	break;
  }
  if(!nf[t.index]) {
	rewrites += Rewrite(t, term, nf, indices);
  }
  return rewrites;
}
