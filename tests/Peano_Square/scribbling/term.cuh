#ifndef PEANO_EXPERIMENTS_TERM_CUH
#define PEANO_EXPERIMENTS_TERM_CUH

enum Head_Symbol { Zero, Succ, Plus, Mult, Square,  Input, NOT_INITIALIZED };

__host__ __device__ inline const char* HS2String(Head_Symbol t) {
  switch(t) {
	case Zero: return "Zero";
	case Succ: return "Succ";
	case Plus: return "Plus";
	case Mult: return "Mult";
	case Square: return "Square";
	
  default: return "NOT_INITIALIZED";
  }
}

struct Term {
    Head_Symbol head_symbol;
    unsigned int index = 0;
	unsigned int arg0; 
	unsigned int arg1; 
    unsigned int parent;
    Term() { head_symbol = NOT_INITIALIZED; };
	__host__ __device__ Term(Head_Symbol head_symbol, unsigned int index, unsigned int arg0, unsigned int arg1, unsigned int parent)
	  : head_symbol(head_symbol), index(index), arg0(arg0), arg1(arg1), parent(parent){
		
	  }
};

__host__ __device__ inline bool is_Zero(Term &t) {
	return t.head_symbol == Zero;
}
__host__ __device__ inline bool is_Succ(Term &t) {
	return t.head_symbol == Succ;
}
__host__ __device__ inline bool is_Plus(Term &t) {
	return t.head_symbol == Plus;
}
__host__ __device__ inline bool is_Mult(Term &t) {
	return t.head_symbol == Mult;
}
__host__ __device__ inline bool is_Square(Term &t) {
	return t.head_symbol == Square;
}

__global__ void Rewrite(bool *frontier, bool *nf, unsigned int *eligible_terms, unsigned int *free_indices, bool *done, unsigned int wl_size, unsigned int *next_free, int *refs);
__global__ void frontier2wl(unsigned int *nres, unsigned int n_terms, bool *frontier, unsigned int *eligible_terms);
__global__ void countrefs(unsigned int n_terms, int *refs);
__global__ void refs2freepos(unsigned int *nfree, unsigned int n_terms, int *refs, unsigned int *free_indices);
__global__ void pad_freeindices(const unsigned int offset, const unsigned int required, unsigned int *next_free, unsigned int *free_indices);
//global variables...
extern __managed__ Term *term;

#endif //PEANO_EXPERIMENTS_TERM_CUH