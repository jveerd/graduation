#include <iostream>
#include "term.cuh"
#include "../bfs.cuh"
#include <chrono>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess)
    {
	  fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	  if (abort) exit(code);
    }
}

__managed__ Term *term;
__managed__ bool *frontier;
__managed__ bool *nf;
__managed__ unsigned int *eligible_terms;
__managed__ unsigned int *free_indices;
__managed__ bool *refs;

// __global__ void count_refdterms() {
//   auto refd = 0;
//   for(int i = 0; i < next_free; i++) {
// 	if(refs[i] > 0) refd++;
//   }
//   printf("Referenced terms: %u\n", refd);
// }

// __global__ void print_nextfree() {
//   printf("Next free ptr is at %u\n", next_free);
// }

std::string term2string(Term &t) {
  switch(t.head_symbol) {
	{% for f in functions%}case {{f.name}}: {
	  //std::cout << "{{f.name}} " << t.index << {% for i in range(f.formals|length) %} " arg{{i}} " << t.arg{{i}} <<  {%- endfor %} std::endl;
  return std::string() + "{{f.name}}(" {% for i in range(f.formals|length) %} + term2string(term[t.arg{{i}}]) {{ ' + ", "' if not loop.last}}{%- endfor %} + ")";
	}
  {%endfor%}

  default:
	return "Type: " + std::string(HS2String(t.head_symbol));
  }
}

void dump_memory(unsigned int n) {
  for(int i = 1; i < n; i++) {
	bool n = nf[i];
	bool f = frontier[i];
	if(n && f) std::cout << "Term[" << i << "] = " << term2string(term[i]) << " is in NF and is in frontier.\n";
	else if(n) std::cout << "Term[" << i << "] = " << term2string(term[i]) << " is in NF and is NOT in frontier.\n";
	else if(f) std::cout << "Term[" << i << "] = " << term2string(term[i]) << " is NOT in NF and is in frontier.\n";
	else std::cout << "Term[" << i << "] = " << term2string(term[i]) << " is NOT in NF and is NOT in frontier.\n";
  }
}

// __global__ void update(unsigned int *current_terms) {
//   *current_terms = next_free + 1;
// }

int main(int argc, const char* argv[]) {
  size_t free, total;
  gpuErrchk(cudaMemGetInfo(&free, &total));
  cudaStream_t stream1, stream2, stream3, stream4, stream5, stream6;
  cudaStreamCreate(&stream1);
  cudaStreamCreate(&stream2);
  cudaStreamCreate(&stream3);
  cudaStreamCreate(&stream4);
  cudaStreamCreate(&stream5);
  cudaStreamCreate(&stream6);

  unsigned int rewrite_tpb = 32;
  unsigned int other_tpb = 128;

  unsigned int term_size = sizeof(Term);
  unsigned int frontier_size = sizeof(bool);
  unsigned int nf_size = sizeof(bool);
  unsigned int eligitem_size = sizeof(unsigned int);
  unsigned int ref_size = sizeof(bool);
  unsigned int freeidx_size = sizeof(unsigned int);

  unsigned int total_size = term_size + frontier_size + nf_size + eligitem_size + ref_size + freeidx_size;
  unsigned int num_terms = free / total_size - 10;

  gpuErrchk(cudaMallocManaged((void**)&term, num_terms * term_size));
  gpuErrchk(cudaMallocManaged((void**)&frontier, num_terms * frontier_size));
  gpuErrchk(cudaMallocManaged((void**)&nf, num_terms * nf_size));
  gpuErrchk(cudaMallocManaged((void**)&eligible_terms, num_terms * eligitem_size));
  gpuErrchk(cudaMallocManaged((void**)&refs, num_terms * ref_size));
  memset(refs, false, num_terms);
  gpuErrchk(cudaMallocManaged((void**)&free_indices, num_terms * freeidx_size));

  nf[0] = true;
  term[0] = Term(NOT_INITIALIZED, 0, {%- for i in range(maxarity) %} 0, {%- endfor %}  0);
  unsigned int root = 1;
  term[root] = Term(Input, root, {%- for i in range(maxarity) %} 0, {%- endfor %}  0);

  unsigned int end_index = Expand_Input(term[root], 2);
  std::cout << "Input: " << term2string(term[root]) << std::endl;

  int device;
  gpuErrchk(cudaGetDevice(&device));
  cudaDeviceProp devProp;
  cudaGetDeviceProperties(&devProp,device);

  gpuErrchk(cudaMemPrefetchAsync(term, num_terms*term_size, device, stream1));
  gpuErrchk(cudaMemPrefetchAsync(frontier, num_terms*frontier_size, device, stream2));
  gpuErrchk(cudaMemPrefetchAsync(nf, num_terms*nf_size, device, stream3));
  gpuErrchk(cudaMemPrefetchAsync(eligible_terms, num_terms*eligitem_size, device, stream4));
  gpuErrchk(cudaMemPrefetchAsync(refs, num_terms*ref_size, device, stream5));
  gpuErrchk(cudaMemPrefetchAsync(free_indices, num_terms*freeidx_size, device, stream6));
  
  unsigned int *next_free;
  unsigned int n_terms;
  cudaMallocManaged((void**)&next_free, sizeof(unsigned int));
  *next_free = n_terms = end_index;

  unsigned int numBlocks = (unsigned int)ceil((double)n_terms / (double) other_tpb);
  unsigned int numThreads = other_tpb;

  // Find first frontier of rewritable terms
  bool *done;
  cudaMallocManaged((void**)&done, sizeof(bool));
  *done = false;

  auto beginTime = std::chrono::high_resolution_clock::now();
  
  unsigned int terms_handled = 0;
  while(!(*done)) {
	terms_handled += numBlocks * numThreads;
	*done = true;

	bfs_kernel<<<numBlocks, numThreads>>>(done, n_terms);
	gpuErrchk(cudaPeekAtLastError());
  }

  unsigned int *nfree;
  
  cudaMallocManaged((void**)&nfree, sizeof(unsigned int));
  
  countrefs<<<numBlocks, numThreads, 0, stream1>>>(n_terms);
  gpuErrchk(cudaPeekAtLastError());

  *nfree = 0;
  refs2freepos<<<numBlocks, numThreads, 0, stream1>>>(nfree, n_terms);
  gpuErrchk(cudaPeekAtLastError());
  
  unsigned int *wl_size;
  cudaMallocManaged((void**)&wl_size, sizeof(unsigned int));
  *wl_size = 0;
  frontier2wl<<<numBlocks, numThreads, 0, stream2>>>(wl_size, n_terms);  
  gpuErrchk(cudaPeekAtLastError());

  gpuErrchk(cudaStreamSynchronize(stream1));
  gpuErrchk(cudaStreamSynchronize(stream2));
  
  *done = false;
  auto rewrites = 0;
  auto _wl_size = *wl_size;
  auto _nfree = *nfree;

  int leftover = _nfree - _wl_size;
  
  if(leftover < 0) {
	int required = _wl_size - _nfree;
	int offset = _wl_size - required;
	unsigned int threads = 32;
	unsigned int blocks = (unsigned int) ceil( (double)required / (double)threads );
	pad_freeindices<<<blocks, threads>>>(offset, required, next_free);
	gpuErrchk(cudaDeviceSynchronize());
	gpuErrchk(cudaPeekAtLastError());
  }

  while(_wl_size && !(*done)) {
	terms_handled += _wl_size;
	rewrites++;

	*done = true;

	numBlocks = (unsigned int)ceil((double)*next_free / (double) rewrite_tpb);
	numThreads = rewrite_tpb;
	gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), device, stream1));

	Rewrite<<<numBlocks, numThreads, 0, stream2>>>(done, _wl_size, next_free);
	gpuErrchk(cudaStreamSynchronize(stream2));
	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), cudaCpuDeviceId, stream2));
	gpuErrchk(cudaStreamSynchronize(stream2));
	gpuErrchk(cudaPeekAtLastError());
	n_terms = *next_free;

	// update<<<1,1>>>(next_free);
	// gpuErrchk(cudaDeviceSynchronize());
	// gpuErrchk(cudaPeekAtLastError());

	if(!(*done)) {

	  numBlocks = (unsigned int)ceil((double)n_terms / (double) other_tpb);
	  numThreads = other_tpb;

	  gpuErrchk(cudaMemset(refs, false, n_terms * ref_size));
	  countrefs<<<numBlocks, numThreads, 0, stream2>>>(n_terms);
	  gpuErrchk(cudaPeekAtLastError());
	  
	  while(!(*done)) {
		terms_handled += n_terms;
		*done = true;

		bfs_kernel<<<numBlocks, numThreads, 0, stream1>>>(done, n_terms);
		gpuErrchk(cudaStreamSynchronize(stream1));
		gpuErrchk(cudaPeekAtLastError());
	  }
	  gpuErrchk(cudaStreamSynchronize(stream2));
	  *done = false;

	  *wl_size = 0;
	  frontier2wl<<<numBlocks, numThreads, 0, stream1>>>(wl_size, n_terms);
	  gpuErrchk(cudaPeekAtLastError());
	  
	  *nfree = 0;
	  refs2freepos<<<numBlocks, numThreads, 0, stream2>>>(nfree, n_terms);
	  gpuErrchk(cudaPeekAtLastError());
	  
	  gpuErrchk(cudaStreamSynchronize(stream1));
	  gpuErrchk(cudaStreamSynchronize(stream2));
	  
	  _wl_size = *wl_size;
	  _nfree = *nfree;
	  
	  leftover = _nfree - _wl_size;
	  
	  if(leftover < 0) {
		gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), device, stream1));
		int required = _wl_size - _nfree;
		int offset = _wl_size - required;
		unsigned int threads = 32;
		unsigned int blocks = (unsigned int) ceil( (double)required / (double)threads );
		std::cout << "numfree: " << _nfree << " total terms: " << n_terms << std::endl;
		std::cout << "threads 32 blocks " << blocks << " (required " << required << ")\n" << std::endl;
		pad_freeindices<<<blocks, threads>>>(offset, required, next_free);
		gpuErrchk(cudaDeviceSynchronize());
		gpuErrchk(cudaPeekAtLastError());
		gpuErrchk(cudaMemPrefetchAsync(next_free, sizeof(unsigned int), cudaCpuDeviceId, stream2));
		n_terms = *next_free;
	  }
	}
	// std::cout << "Term --" << rewrites << "-->: " + term2string(term[root]) << std::endl;
	// std::cout << "_wl_size " << _wl_size << std::endl;
  }
  //gpuErrchk(cudaMemPrefetchAsync(term, *next_free*sizeof(Term), cudaCpuDeviceId));
  std::cout << "Output: " + term2string(term[root]) << std::endl;
  auto endTime = std::chrono::high_resolution_clock::now();
#ifdef VERBOSE
  long long milliseconds = std::chrono::duration_cast<
	std::chrono::milliseconds>(endTime - beginTime).count();
  double seconds = (double)milliseconds / (double)1000;
  double tps = (double)terms_handled / seconds;

  std::cout.precision(0);
  std::cout << std::fixed << "BFSing and rewriting took " << milliseconds << " ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific << terms_handled << "\n T/S: " << tps << std::endl;
#endif

  std::cout << "Highest pointer to occupied memory: " << *next_free << std::endl;
  gpuErrchk(cudaDeviceSynchronize());
    
  return 0;
}
