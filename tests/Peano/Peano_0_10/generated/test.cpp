#include <iostream>
#include "term.h"
#include <chrono>
#include <vector>

std::vector<Term> term;
Indices *indices;
std::vector<bool> nf;
std::vector<int> refs;
void Expand_Input(Term &t) {
  
	unsigned int idx0 = indices->getRef();
	unsigned int idx0_0 = indices->getRef();
	unsigned int idx0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0_0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0_0_0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0_0_0_0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0_0_0_0_0_0_0 = indices->getRef();
	unsigned int idx0_0_0_0_0_0_0_0_0_0_0 = indices->getRef();
    Term t0_0_0_0_0_0_0_0_0_0_0 = Term(Zero, idx0_0_0_0_0_0_0_0_0_0_0, 0,  0);
	term[idx0_0_0_0_0_0_0_0_0_0_0] = t0_0_0_0_0_0_0_0_0_0_0;
	nf[idx0_0_0_0_0_0_0_0_0_0_0] = true;
    Term t0_0_0_0_0_0_0_0_0_0 = Term(S, idx0_0_0_0_0_0_0_0_0_0, t0_0_0_0_0_0_0_0_0_0_0.index,  0);
	term[idx0_0_0_0_0_0_0_0_0_0] = t0_0_0_0_0_0_0_0_0_0;
    nf[idx0_0_0_0_0_0_0_0_0_0] = nf[idx0_0_0_0_0_0_0_0_0_0_0] ;
    Term t0_0_0_0_0_0_0_0_0 = Term(S, idx0_0_0_0_0_0_0_0_0, t0_0_0_0_0_0_0_0_0_0.index,  0);
	term[idx0_0_0_0_0_0_0_0_0] = t0_0_0_0_0_0_0_0_0;
    nf[idx0_0_0_0_0_0_0_0_0] = nf[idx0_0_0_0_0_0_0_0_0_0] ;
    Term t0_0_0_0_0_0_0_0 = Term(S, idx0_0_0_0_0_0_0_0, t0_0_0_0_0_0_0_0_0.index,  0);
	term[idx0_0_0_0_0_0_0_0] = t0_0_0_0_0_0_0_0;
    nf[idx0_0_0_0_0_0_0_0] = nf[idx0_0_0_0_0_0_0_0_0] ;
    Term t0_0_0_0_0_0_0 = Term(S, idx0_0_0_0_0_0_0, t0_0_0_0_0_0_0_0.index,  0);
	term[idx0_0_0_0_0_0_0] = t0_0_0_0_0_0_0;
    nf[idx0_0_0_0_0_0_0] = nf[idx0_0_0_0_0_0_0_0] ;
    Term t0_0_0_0_0_0 = Term(S, idx0_0_0_0_0_0, t0_0_0_0_0_0_0.index,  0);
	term[idx0_0_0_0_0_0] = t0_0_0_0_0_0;
    nf[idx0_0_0_0_0_0] = nf[idx0_0_0_0_0_0_0] ;
    Term t0_0_0_0_0 = Term(S, idx0_0_0_0_0, t0_0_0_0_0_0.index,  0);
	term[idx0_0_0_0_0] = t0_0_0_0_0;
    nf[idx0_0_0_0_0] = nf[idx0_0_0_0_0_0] ;
    Term t0_0_0_0 = Term(S, idx0_0_0_0, t0_0_0_0_0.index,  0);
	term[idx0_0_0_0] = t0_0_0_0;
    nf[idx0_0_0_0] = nf[idx0_0_0_0_0] ;
    Term t0_0_0 = Term(S, idx0_0_0, t0_0_0_0.index,  0);
	term[idx0_0_0] = t0_0_0;
    nf[idx0_0_0] = nf[idx0_0_0_0] ;
    Term t0_0 = Term(S, idx0_0, t0_0_0.index,  0);
	term[idx0_0] = t0_0;
    nf[idx0_0] = nf[idx0_0_0] ;
    Term t0 = Term(S, idx0, t0_0.index,  0);
	term[idx0] = t0;
    nf[idx0] = nf[idx0_0] ;
	unsigned int idx1 = indices->getRef();
    Term t1 = Term(Zero, idx1, 0,  0);
	term[idx1] = t1;
	nf[idx1] = true;

	t.head_symbol = InputPlus;
    t.arg0 = t0.index;
    t.arg1 = t1.index;
    term[t.index] = t;
	nf[t.index] = false;
}

int main(int argc, const char* argv[]) {
  std::cout << "Foo" << std::endl;
  std::stack<unsigned int> free_indices = std::stack<unsigned int>();
  std::cout << "Foo2" << std::endl;
  indices = new Indices(term, refs, nf, free_indices);
  std::cout << "Foo3" << std::endl;
  
  Term zero(NOT_INITIALIZED, 0, 0,  0);
  term.push_back(zero);
  nf.push_back(false);
  refs.push_back(0);
  std::cout << "Foo4" << term.size() << std::endl;

  Term root(Input, 1, 0,  0);
  std::cout << "Foo4.5\n";
  term.push_back(root);
  std::cout << "Foo5\n";
  refs.push_back(1);
  std::cout << "Foo6\n";
  nf.push_back(false);
  std::cout << "Foo7\n";
  Expand_Input(root);

  std::cout << "Foo8\n";
  //std::cout << "Input: " + root.toString(term) << std::endl;
  
  auto beginTime = std::chrono::high_resolution_clock::now();

  auto handled = Rewrite(root, term, nf, *indices);

  //std::cout << "Output: " + root.toString(term) << std::endl;
  auto endTime = std::chrono::high_resolution_clock::now();

  auto ms = std::chrono::duration<double, std::milli>(endTime - beginTime);
  double seconds = ms.count() / (double)1000;
  double tps = (double)handled / seconds;

  std::cout.precision(0);
  std::cout << std::fixed /*<< "Rewriting took "*/ << ms.count() << " ";//ms. Terms handled: ";
  std::cout.precision(4);
  std::cout << std::scientific /*<< handled << "\n R/S: "*/ << tps << " ";//std::endl;


  std::cout << /*"Highest pointer to occupied memory: " <<*/ term.size() << std::endl;
    
  return 0;
}