#ifndef PEANO_EXPERIMENTS_TERM_CUH
#define PEANO_EXPERIMENTS_TERM_CUH

#include <string>

enum Type { Zero, Succ, Plus, Mult, Square, NOT_INITIALIZED };

__host__ __device__ inline const char* Type2String(Type t) {
  switch(t) {
	case Zero: return "Zero";
	case Succ: return "Succ";
	case Plus: return "Plus";
	case Mult: return "Mult";
	case Square: return "Square";
	 default: return "NOT_INITIALIZED";
  }
}

struct Term {
    Type type;
    unsigned int t = 0;
	unsigned int arg0; 
	unsigned int arg1; 
    unsigned int parent;
    Term() { type = NOT_INITIALIZED; };
	__host__ __device__ Term(Type type, unsigned int t, unsigned int arg0, unsigned int arg1, unsigned int parent)
        : type(type), t(t), arg0(arg0), arg1(arg1), parent(parent){
		
		}
};

__host__ __device__ inline bool is_Zero(Term &t) {
	return t.type == Zero;
}
__host__ __device__ inline bool is_Succ(Term &t) {
	return t.type == Succ;
}
__host__ __device__ inline bool is_Plus(Term &t) {
	return t.type == Plus;
}
__host__ __device__ inline bool is_Mult(Term &t) {
	return t.type == Mult;
}
__host__ __device__ inline bool is_Square(Term &t) {
	return t.type == Square;
}

__global__ void Rewrite(Term *term, bool *done, bool *frontier, bool *nf);

//global variables...
extern __device__ unsigned int next_free;
extern __device__ unsigned int root_idx;

#endif //PEANO_EXPERIMENTS_TERM_CUH