#ifndef PEANO_EXPERIMENTS_TERM_CUH
#define PEANO_EXPERIMENTS_TERM_CUH

#include <string>

enum Type { Zero, Succ, Add, Mult, NOT_INITIALIZED };

struct Term {
    Type type;
    unsigned int t = 0;
    unsigned int lhs = 0;
    unsigned int rhs = 0;
    unsigned int parent;
    Term() { type = NOT_INITIALIZED; };
    Term(Type type, unsigned int t, unsigned int lhs, unsigned int rhs, unsigned int parent)
        : type(type), t(t), lhs(lhs), rhs(rhs), parent(parent) {}
};

__global__ void Rewrite(bool *done, bool *frontier, bool *nf);

//global variables...
extern __device__ Term *term;
extern __device__ unsigned int next_free;
extern __device__ unsigned int root_idx;

#endif //PEANO_EXPERIMENTS_TERM_CUH
