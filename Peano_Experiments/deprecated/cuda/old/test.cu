#include "pointer_poly.cuh"

__device__ Peano_Term *root = NULL;

__global__ void Init_Mult(int lhs, int rhs) {
//    Peano_Zero *l_zero = new Peano_Zero();
//    Peano_Term *lhs_ptr = l_zero;
//    for(int i = lhs; i > 0; i--) {
//        Peano_Term *temp = new Peano_Succ(lhs_ptr);
//        lhs_ptr = temp;
//    }
//
//    Peano_Zero *r_zero = new Peano_Zero();
//    Peano_Term *rhs_ptr = r_zero;
//    for(int i = rhs; i > 0; i--) {
//        Peano_Term *temp = new Peano_Succ(rhs_ptr);
//        rhs_ptr = temp;
//    }
//
//    root = new Peano_Mult(lhs_ptr, rhs_ptr);

    Peano_Zero *l_zero1 = new Peano_Zero();
    Peano_Zero *l_zero2 = new Peano_Zero();
    Peano_Zero *r_zero1 = new Peano_Zero();
    Peano_Zero *r_zero2 = new Peano_Zero();

    Peano_Succ *l_one1 = new Peano_Succ(l_zero1);
    Peano_Succ *l_one2 = new Peano_Succ(l_zero2);
    Peano_Succ *r_one1 = new Peano_Succ(r_zero1);
    Peano_Succ *r_one2 = new Peano_Succ(r_zero2);

    Peano_Succ *l_two1 = new Peano_Succ(l_one1);
    Peano_Succ *l_two2 = new Peano_Succ(l_one2);
    Peano_Succ *r_two1 = new Peano_Succ(r_one1);
    Peano_Succ *r_two2 = new Peano_Succ(r_one2);

    Peano_Mult *mul1 = new Peano_Mult(l_two1, l_two2);
    Peano_Mult *mul2 = new Peano_Mult(r_two1, r_two2);
    root = new Peano_Mult(mul1, mul2);
}

__global__ void Solve_Root() {
    int steps = 0;
    Peano_Term *result = root;
    while(!root->is_normalform()) {
        steps++;
        root = root->Rewrite();
    }
//    root = result;
    printf("Fully rewritten in %d steps!\n", steps);
}

__global__ void Report_Result() {
    int succs = 0;
    Peano_Term* result = root;
    while(!result->is_zero()) {
        succs++;
        Peano_Succ *temp = reinterpret_cast<Peano_Succ*>(result);
        result = temp->n;
    }
    printf("Result: %d\n", succs);
}

int main(int argc, const char* argv[])
{
    cudaError_t error;
    error = cudaGetLastError();

    if(error != cudaSuccess) {
        printf("0 %s\n",cudaGetErrorString(error));
        exit(1);
    }

    Init_Mult<<<1,1>>>(4,4);

    if (error != cudaSuccess)
    {
        printf("1 %s\n",cudaGetErrorString(error));
        exit(1);
    }
    cudaDeviceSynchronize();

    Solve_Root<<<1,1>>>();

    if (error != cudaSuccess)
    {
        printf("2 %s\n",cudaGetErrorString(error));
        exit(1);
    }
    cudaDeviceSynchronize();

    Report_Result<<<1,1>>>();

    if (error != cudaSuccess)
    {
        printf("3 %s\n",cudaGetErrorString(error));
        exit(1);
    }
    cudaDeviceSynchronize();

    return 0;
}