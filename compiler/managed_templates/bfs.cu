#include "../bfs.cuh"
#include <stdio.h>
__global__ void bfs_kernel(bool *done, unsigned int n_terms) {
    int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id >= n_terms)
	  return;
    bool f = frontier[id];
    __syncthreads();

    if (f)
    {
        Term &my_term = term[id];
		{%- for i in range(maxarity) %}bool n{{loop.index0}} = nf[my_term.arg{{loop.index0}}];
		{%-endfor%}
        if({%- for i in range(maxarity)%}n{{loop.index0}}{{ " && " if not loop.last }}{%- endfor %}) return;
		{%- for i in range(maxarity)%}
        if(!n{{loop.index0}}) {
		  //printf("term[%u].frontier = false, frontier[%u] = true\n", id, my_term.arg{{loop.index0}});
            frontier[id] = false;
            frontier[my_term.arg{{loop.index0}}] = true;
            *done = false;
        }{%- endfor %}
    }
}