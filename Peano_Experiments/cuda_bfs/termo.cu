#include "term.cuh"

#include <cuda_runtime.h>
#include <cooperative_groups.h>

inline void __device__ Rewrite_Add(Term &add, bool *frontier, bool *nf) {
    if(term[add.lhs].type == Zero) {
        Term &rhs = term[add.rhs];
        add.type = rhs.type;
        add.lhs = rhs.lhs;
        add.rhs = rhs.rhs;
        term[add.t] = add;

        //this term is now in normalform. Add parent(s) to frontier
        nf[add.t] = true;
        frontier[add.t] = false;
        frontier[add.parent] = true;
    } else if (term[add.lhs].type == Succ) {
        Term &new_succ = add;
        Term &new_add = term[add.lhs];
        new_succ.type = Succ;
        new_succ.lhs = new_add.t;

        new_add.type = Add;
        new_add.rhs = new_succ.rhs;

        nf[new_succ.t] = false;
        nf[new_add.t] = false;
        frontier[new_succ.t] = false;
        frontier[new_add.t] = true;

        term[new_succ.t] = new_succ;
        term[new_add.t] = new_add;
    }
}

inline void __device__ Rewrite_Mult(Term &mul, bool *frontier, bool *nf) {
    if(term[mul.lhs].type == Zero) {
        term[mul.t].type = Zero;
        nf[mul.t] = true;
        frontier[mul.t] = false;
        frontier[mul.parent] = true;
    } else if(term[mul.lhs].type == Succ) {
        Term &new_add = mul;
        Term &new_mul = term[mul.lhs];
        new_add.type = Add;
        new_add.lhs = new_mul.t;

        new_mul.type = Mult;
        new_mul.rhs = new_add.rhs;

        nf[new_add.t] = false;
        nf[new_mul.t] = false;

        frontier[new_add.t] = false;
        frontier[new_mul.t] = true;

        term[new_add.t] = new_add;
        term[new_mul.t] = new_mul;
    }
}

__global__ void Rewrite(bool *done, bool *frontier, bool *nf) {
    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    bool do_work = frontier[id] && !nf[id];
    __syncthreads();
    if(do_work) {
        Term &my_term = term[id];

        switch(my_term.type) {
            case Zero:
                break;
            case Succ:
                //don't think these should happen anymore, as this term should never be in frontier
                break;
            case Add:
                Rewrite_Add(my_term, frontier, nf);
                *done = false;
                break;
            case Mult:
                Rewrite_Mult(my_term, frontier, nf);
                *done = false;
                break;
        }
    }
    else if(nf[id]) frontier[id] = false;
}